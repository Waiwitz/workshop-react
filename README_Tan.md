
## Device Manage : เมื่อเลือก BORROWED ให้แสดง Select ห้องประชุม และ เมื่อเลือก STANDARD ให้แสดง Select ที่นั่ง
        **pages**

              import ManageDevice from "@/components/ManageDevice";
              import hasAuthRedirect from "@/services/Auth/has-auth-redirect"; //ซึ่งเป็นฟังก์ชันที่อาจถูกใช้ในการตรวจสอบสิทธิ์การเข้าถึง (authorization) และการเปลี่ยนเส้นทางการเข้าถึงหน้า. ฟังก์ชัน 
              import React from "react";

              export async function getServerSideProps(context) {
                let redirect;
                if ((redirect = hasAuthRedirect(context))) { //และตรวจสอบว่ามีการต้องการการเปลี่ยนเส้นทางหรือไม่. หากมีการต้องการการเปลี่ยนเส้นทาง (redirect), ฟังก์ชันจะ return ออกไปทันทีพร้อมกับข้อมูลการเปลี่ยนเส้นทาง.
                  return {
                    redirect,
                  };
                }

                return {
                  props: {},  //ฟังก์ชันจะ return ออกไปพร้อมกับ props ที่เป็นวัตถุที่ไม่มีข้อมูล. ในที่นี้จะ return { props: {} } ซึ่งหมายถึงไม่มีข้อมูลพิเศษที่จะส่งไปยังหน้า ManageDevice.


                }; //getServerSideProps ในที่นี้ถูกใช้เพื่อทำการตรวจสอบสิทธิ์การเข้าถึงและการเปลี่ยนเส้นทางก่อนที่หน้า ManageDevice จะถูกสร้างขึ้นและส่งไปยังไคลเอ็นต์.
              }

const ManageDevicePage = () => {
  return <ManageDevice />;
};

export default ManageDevicePage;

## นี้เป็นหน้าเพจของ  ManageDevicePage ที่มีการเรียกใช้ components ของ  ManageDevice 
##  components ManageDevice มีหน้าที่เกี่ยวกับการ จัดการอุปกรณ์ จะแสดงรายการอุปกรต่างๆ ที่ User ใช้งาน เช่น ประเภทอุปกรณ์	ชื่ออุปกรณ์	รายละเอียด	ไอคอน	อุปกรณ์สำหรับ	วันที่แก้ไขล่าสุด	การดำเนินการ




 ในส่วนที่ได้ทำ คือส่วนของ AddDeviceDialog  เป็นส่วนของการเพิ่มข้อมูล 

      const AddDeviceDialog = ({
        dialogOpen: open,
        onSubmit,
        closeDialog,
        systemList,
      }) =>  //เป็นการ ตัวกำหนดค่าว่าจะเก็บไว้ในตัวแปรไหน 
        //   
        const [open, setOpen] = useState(false);
        const [deviceType, setDeviceType] = useState("");
        const [systemId, setSystemId] = useState("");
        const [deviceNameEn, setDeviceNameEn] = useState("");
        const [deviceNameTh, setDeviceNameTh] = useState("");
        const [details, setDetails] = useState("");
        const [iconName, setIconName] = useState("");
        const [seatChecked, setSeatChecked] = useState(false);
        const [meetingRoomChecked, setMeetingRoomChecked] = useState(false);
        const [errors, setErrors] = useState({});
        const intl = useIntl();


            const handleDeviceTypeChange = (event) => {  //เมื่อมีการเปลี่ยนแปลงค่าใน input ที่เกี่ยวข้องกับประเภทอุปกรณ์ (device type) ฟังก์ชันนี้จะถูกเรียก ซึ่งจะทำการอัปเดต state deviceType ด้วยค่าที่เปลี่ยนแปลง.
              setDeviceType(event.target.value);
            };
            const handleSystemIdChange = (event) => { // เมื่อมีการเปลี่ยนแปลงค่าใน input ที่เกี่ยวข้องกับรหัสระบบ (system ID) ฟังก์ชันนี้จะถูกเรียก ซึ่งจะทำการอัปเดต state systemId
              setSystemId(event.target.value);
            };

            const handleIconChange = (e) => { //เมื่อมีการเปลี่ยนแปลงค่าใน input ที่เกี่ยวข้องกับชื่อไอคอน (icon name) ฟังก์ชันนี้จะถูกเรียก ซึ่งจะทำการอัปเดต state iconName ด้วยค่าที่เปลี่ยนแปลง
              setIconName(e.target.value); 
            };

            const handleSeatCheckboxChange = (event) => { //เมื่อมีการเปลี่ยนแปลงค่าใน checkbox ที่เกี่ยวข้องกับที่นั่ง (seat) ฟังก์ชันนี้จะถูกเรียก ซึ่งจะทำการอัปเดต state seatChecked ด้วยค่าที่เปลี่ยนแปลง.
              setSeatChecked(event.target.checked);
            };

            const handleMeetingRoomCheckboxChange = (event) => { //เมื่อมีการเปลี่ยนแปลงค่าใน checkbox ที่เกี่ยวข้องกับห้องประชุม (meeting room) ฟังก์ชันนี้จะถูกเรียก ซึ่งจะทำการอัปเดต state meetingRoomChecked ด้วยค่าที่เปลี่ยนแปลง. 
              setMeetingRoomChecked(event.target.checked);
            };

            const handleSubmit = (event) => { //เมื่อ form ถูกส่ง (submit) ฟังก์ชันนี้จะถูกเรียก โดยมีการใช้
              event.preventDefault();

        **//event.preventDefault() เพื่อป้องกันการโหลดหน้าใหม่ (refresh) ทำให้หน้าไม่ถูกโหลดใหม่. ฟังก์ชันนี้สามารถใช้เพื่อดำเนินการต่อไป เช่น ส่งข้อมูลที่ผู้ใช้ป้อนใน form ไปยังเซิร์ฟเวอร์หรือทำการประมวลผลข้อมูลเพิ่มเติมได้.



  ## จะเป็นการเพิ่มข้อมูลของ รายการอุปกรต่างๆ 
  
          <DialogContent>
                <form onSubmit={handleSubmit}>
                  <div
                    style={{
                      display: "flex",
                      justifyContent: "space-between",
                      mb: 2,
                      marginTop: "-20px",
                    }}
                  >
                    <div style={{ width: "49%" }}>
                      <FormControl fullWidth>
                        <FormLabel
                          htmlFor="device-type"
                          sx={{ fontSize: "16px !important" }}
                        >
                          {intl.formatMessage({
                            id: "device.type",
                            defaultMessage: "Device Type",
                          })}
                        </FormLabel>
                        <Select
                          value={deviceType} //คุณสมบัตินี้ใช้กำหนดค่าใน input element โดยให้มีค่าเท่ากับ deviceType ซึ่งอาจจะเป็น state ที่ถูกเก็บไว้ใน React component. 
                          onChange={handleDeviceTypeChange} //: คุณสมบัตินี้ใช้กำหนดฟังก์ชันที่จะถูกเรียกเมื่อมีการเปลี่ยนแปลงค่าใน input element นี้ ในที่นี้คือ handleDeviceTypeChange ซึ่งอาจจะเป็นฟังก์ชันที่ทำการอัปเดต state deviceType ของ React component.
                          error={!!errors.deviceType}  //การตรวจสอบว่ามีข้อผิดพลาดที่เกี่ยวข้องกับ input element นี้หรือไม่ โดย errors.deviceType อาจถูกใช้ในลักษณะนี้ ถ้ามี 
                          inputProps={{ id: "device-type" }} //ใช้กำหนดค่าใน input element โดยในที่นี้คือ id ที่ถูกกำหนดเป็น "device-type". ค่าที่ถูกกำหนดจะถูกนำไปใช้ใน HTML markup ของ input element.
                          displayEmpty //ใช้ในการกำหนดว่า dropdown (หรือ select) ควรแสดงตัวเลือกว่าง (empty option) หรือไม่.
                          placeholder={intl.formatMessage({ //ใช้กำหนดข้อความที่จะแสดงใน dropdown (หรือ select) เมื่อยังไม่ได้ทำการเลือกตัวเลือกใด ๆ ในที่นี้ใช้ intl.formatMessage เพื่อรับข้อความจากไลบรารีการจัดการข้อความ (internationalization) และให้ข้อความเริ่มต้นเป็น "Select type". 
                            id: "select.type",
                            defaultMessage: "Select type",
                          })}
                        >
                        
                        <MenuItem value="STANDARD">
                          {intl.formatMessage({
                            id: "standard.type",
                            defaultMessage:
                              intl.locale === "th" ? "มาตรฐาน" : "Standard", 
                              // เป็นการเชคแปลภาษาจาก Api ที่ดึงมาว่าเป็น th ไหม
                          })} 
                        </MenuItem>
                        <MenuItem value="BORROWED">
                          {intl.formatMessage({
                            id: "borrowed.type",
                            defaultMessage: intl.locale === "th" ? "ยืม" : "Borrowed",
                          })}
                        </MenuItem>
                      </Select>
                      {errors.deviceType && (
                        <ErrorText>{errors.deviceType}</ErrorText>
                      )}
                    </FormControl>
                  </div>
  
  ## เงือนไขการทำงานคือ  เมื่อเลือก BORROWED ให้แสดง Select ห้องประชุม และ เมื่อเลือก STANDARD ให้แสดง Select ที่นั่ง/ห้องประชุม 

  ## 1 โครงสร้างฟอร์ม:

โค้ดนี้เริ่มต้นด้วย element form ซึ่งจะทำการ submit ผ่านฟังก์ชัน handleSubmit ที่ถูกกำหนดไว้.
## 2 การเลือกประเภทอุปกรณ์ (Device Type):

ใน div ที่ถูกกำหนดความกว้างเป็น มี FormControl ซึ่งเป็นคอมโพเนนต์ที่ให้การควบคุมการป้อนข้อมูล.
ภายใน FormControl มี FormLabel สำหรับ "ประเภทอุปกรณ์".
## 3 ตัวเลือกประเภทอุปกรณ์:

มี MenuItem สองตัวที่แทนประเภทอุปกรณ์ต่าง ๆ: "STANDARD" และ "BORROWED".
ข้อความของตัวเลือกนี้ถูกแปลโดยใช้ intl.formatMessage, ซึ่งดูเหมือนจะมีการตรวจสอบภาษา locale เพื่อแปลเป็น "มาตรฐาน" หรือ "Standard" สำหรับ locale เป็น "th" (ไทย) และ "ยืม" หรือ "Borrowed" สำหรับ locale อื่น ๆ.
## 4 การจัดการข้อผิดพลาด:

หากมีข้อผิดพลาดที่เกี่ยวกับฟิลด์ deviceType, จะแสดง ErrorText เพื่อแสดงข้อความข้อผิดพลาด
## 5 Internationalization ():

ใช้ intl object  ซึ่งให้ข้อความที่ถูกแปลตาม locale ของผู้ใช้.
formatMessage function ใช้เพื่อดึงข้อความที่ถูกแปลสำหรับ label และ placeholder.
## 6 การจัดรูปแบบ (Styling):

มีการใช้ inline styles เพื่อปรับแต่งเลยเท่งและลักษณะขององค์ประกอบบางประการ เช่น ระยะห่างและขนาดตัวอักษร.



# Device Manage : แปลภาษาประเภทอุปกรณ์ ทั้งแก้ไข ทั้งเพิ่มรายการ

## ในส่วนของ DeviceListTable 

        import CustomTableCell, { StyledTableCell } from "@/components/CustomTableCell"; //ป็นคอมโพเนนต์ที่ถูกสร้างขึ้นเพื่อแสดงข้อมูลในรูปแบบของเซลล์ในตาราง (table cell). StyledTableCell อาจเป็น Styled-component ที่ถูกนำเข้าและใช้ใน CustomTableCell.  //ไอคอนที่ถูกนำเข้าและใช้สำหรับแสดงไอคอนการแก้ไข (edit) และลบ (delete). 
        import EditOutlinedIcon from "@mui/icons-material/EditOutlined";
        import DeleteIcon from "@mui/icons-material/Delete";
        import {
          Paper,
          Table,
          TableBody,
          TableContainer,
          TableHead,
          TableRow,
          styled,
        } from "@mui/material";
        import { useEffect } from "react"; //ฟังก์ชัน Hook ที่ถูกนำเข้ามาจาก React ใช้ในการทำผลกระทบ (side effects) หลังจากที่ component ถูก render. อาจจะใช้เพื่อดึงข้อมูลหรือทำงานต่าง ๆ หลังจาก component ถูก mount. 
        import { useIntl } from "react-intl"; //ที่ถูกนำเข้ามาจาก react-intl เพื่อให้สามารถใช้งานการจัดการข้อความและการแปลภาษาใน component. 
        import getUserFromCookie from "@/services/Auth/getUserFromCookie"; //ฟังก์ชันที่ถูกนำเข้ามาเพื่อดึงข้อมูลผู้ใช้จากคุกกี้ ซึ่งอาจจะเป็นการตรวจสอบสิทธิ์การเข้าถึงหรือข้อมูลผู้ใช้ที่ต้องการใน component.

        const DeviceListTable = ({
          deviceList,
          openUpdate,
          handleDeleteDialogOpen,
        }) => {
          const intl = useIntl();
          console.log(intl.locale);
          useEffect(() => {
            console.log("deviceList", deviceList);
          }, []);

          // const handleEdit = (id) => {
          //   console.log('edit');  //
          //   openUpdate(id);
          // };

          const handleDelete = () => {
            console.log("delete");
          };

        **useEffect คือ Hook ที่ใช้ใน React สำหรับการทำผลกระทบหลังจากที่ component ถูก render.
        ในที่นี้ useEffect ถูกใช้เพื่อล็อก (console.log) ค่าของ deviceList ทุกครั้งที่ component ถูก render หรือถูก mount.
        useEffect ถูกกำหนด dependency array [] ซึ่งหมายถึงไม่มี dependency ใด ๆ ทำให้ useEffect จะทำงานเพียงครั้งเดียวเมื่อ component ถูก mount เท่านั้น.

        **ฟังก์ชัน handleEdit ถูกคัดลอก (commented out) ด้วยการใส่ // ที่เริ่มต้นของฟังก์ชัน.
          นั่นหมายความว่าฟังก์ชันนี้ไม่ได้ถูกใช้งานในขณะนี้ (commented out) และอาจจะถูกใช้ในภายหลังหรือถูกทิ้งไว้เป็นการอ้างอิงในอนาคต.

        **ฟังก์ชัน handleDelete ถูกสร้างขึ้นเพื่อทำงานเมื่อมีการเรียกใช้.
        ในที่นี้, มีการล็อก (console.log) ข้อความ "delete" เพื่อแสดงใน console ของเบราว์เซอร์เมื่อ handleDelete ถูกเรียก.  
        ##  การใช้ useEffect เพื่อล็อกค่า deviceList แสดงว่ามีการตรวจสอบค่าหรือทำผลกระทบหลังจากการทำงานบางอย่างที่ทำให้ deviceList เปลี่ยนแปลง หรือทำงานตามที่ต้องการ. ส่วน handleEdit และ handleDelete จะเป็นฟังก์ชันที่ถูกสร้างขึ้นเพื่อให้สามารถจัดการกับการแก้ไขหรือลบข้อมูลได้ในภายหลัง.

## เป็นส่วนหนึ่งของ component ที่ถูกนำมาใช้ในการแสดงรายการอุปกรณ์ (device) ในรูปแบบตาราง (table) และมีการใช้ useIntl  รวมถึง useEffect ที่ใช้สำหรับการดูข้อมูลเริ่มต้น. และมีบางส่วนที่อาจจะยังไม่ได้รับการใช้หรือเป็นตัวอย่าง.

## Prop และการใช้ useIntl:

  DeviceListTable รับ prop 3 ตัวคือ deviceList, openUpdate, และ handleDeleteDialogOpen.
ในภายใน DeviceListTable มีการใช้ useIntl hook จากที่คุณเรียกว่า intl เพื่อให้การสนับสนุนและจัดการข้อความแบบระหว่างประเทศ (Internationalization or i18n) ใน component นี้.
##การใช้ useEffect:

  มี code ที่ถูก comment-out ซึ่งคือฟังก์ชัน handleEdit ที่ยังไม่ได้ให้การใช้งาน.
น่าเป็นส่วนหนึ่งของการพัฒนาที่อาจจะถูกใช้ในภายหลังหรืออาจจะเป็น code ที่ยังไม่ได้ลบออก.
ฟังก์ชัน handleDelete:

## มีฟังก์ชัน handleDelete 
  ที่ console log ข้อความ "delete" เมื่อถูกเรียก. อาจจะเป็นตัวอย่างที่ยังไม่ได้รับการประยุกต์ใช้หรืออาจจะเป็น placeholder สำหรับการลบข้อมูลในรุ่นต่อไป.
  ## ในส่วนที่ทำคือ
      
              {deviceList?.map((row, index) => (
                <StyledTableRow key={row.id}>

                  <CustomTableCell 
                      value={
                      intl.locale == "en" 
                       ? row.deviceType : row.deviceType == "BORROWED"
                       ? "ยืม" : "มาตรฐาน"
                    } />

                  <CustomTableCell
                    value={
                      intl.locale == "en" ? row.deviceNameEn : row.deviceNameTh
                    }
                  />
                  
                  <CustomTableCell value={row.details || "-"} />
                  <CustomTableCell
                    value={
                      row.iconName ? (
                        <i className={row.iconName}></i>
                      ) : (
                        <>-</>
                      )
                    }
                  />
                <CustomTableCell value={row.equipmentFor} />
                <CustomTableCell value={row.updatedAt} />
                <StyledTableCell>
                  <EditOutlinedIcon
                    onClick={() => openUpdate(row.id)}
                    color="green"
                    sx={{ cursor: "pointer" }}
                  />
                  <DeleteIcon
                    onClick={() => handleDeleteDialogOpen(row)}
                    sx={{ cursor: "pointer" }}
                    color="warning"
                  />
                </StyledTableCell>
              </StyledTableRow>
            ))}
          </TableBody>

## การแสดงข้อมูลแต่ละแถว:
  ในส่วนนี้, มีการใช้ .map บน deviceList เพื่อสร้าง StyledTableRow สำหรับแต่ละอุปกรณ์ (device).
StyledTableRow มี prop key ที่ถูกกำหนดตาม row.id เพื่อระบุความแตกต่างของแต่ละแถว.
## การแสดงข้อมูลในแต่ละคอลัมน์ (CustomTableCell):

สำหรับแต่ละคอลัมน์, มี CustomTableCell component ที่มี prop value ที่ถูกกำหนดตามข้อมูลที่ต้องการแสดง.
ตัวอย่างเช่น, ในคอลัมน์แรก (row.deviceType), มีการตรวจสอบ locale (intl.locale) เพื่อแสดงข้อมูลที่ถูกแปลถูกต้อง ในที่นี้ถ้า locale เป็น "en" ก็จะแสดง row.deviceType ตามปกติ แต่ถ้า locale เป็น "th" ก็จะแสดง "ยืม" หรือ "มาตรฐาน" ตามค่า row.deviceType.
คอลัมน์ที่สอง (row.deviceNameEn หรือ row.deviceNameTh) จะแสดงชื่ออุปกรณ์ภาษาอังกฤษหรือไทยตาม locale.

## การใช้ StyledTableRow และ CustomTableCell:
ในกรณีนี้, StyledTableRow และ CustomTableCell น่าจะเป็น components ที่ถูกกำหนดไว้สำหรับปรับแต่งสไตล์ของแถวและคอลัมน์ของตารางตามต้องการ.
## การใช้(intl):
intl.locale ถูกใช้ในการตรวจสอบภาษาปัจจุบันของผู้ใช้.
ตามตัวอย่าง, ในการแสดงประเภทอุปกรณ์ (row.deviceType), ถ้า locale เป็น "en" จะแสดงค่าเป็น row.deviceType โดยตรง, แต่ถ้า locale เป็น "th" จะแสดง "ยืม" หรือ "มาตรฐาน" ตามค่า row.deviceType.

## การบริหารพื้นที่ : แก้ไขการจัดวาง "คอลัม" ในตาราง ให้ระยะห่างพอดี (ตามความเหมาะสม)

## นี้จะเป็นหน้า Page ของ  การบริหารพื้นที่ ที่มีการเรียก  components มาจาก  Booking/AreaAdministration


    import AreaAdministration from "@/components/Booking/AreaAdministration";
    import hasAuthRedirect from "@/services/Auth/has-auth-redirect";

    export async function getServerSideProps(context) {
      let redirect;
      if ((redirect = hasAuthRedirect(context))) {
        return {
          redirect,
        };
      }

      return {
        props: {},
      };
    }

    const AreaAdministrationPage = () => {
      return <AreaAdministration />;
    };

    export default AreaAdministrationPage;


## ในส่วน components ที่ได้รับหมอบหมาย ก็คือ  แก้ไขการจัดวาง "คอลัม" ในตาราง ให้ระยะห่างพอดี (ตามความเหมาะสม) ทำในส่วนของ Booking/Reserve/SeatAreaTypeTable.js

      const SeatAreaTypeTable = ({
        records,
        onSubmit,
        hookForm,
        bookingSummaryData,
        zoneAreaType,
        handleCancelableBookingOpen,
        cancelableBookingOpen,
        handleCancelableBookingClose,
        cancelableBookingList,
        selectedBookingIdList,
        setSelectedBookingIdList,
        seats,
        mutateSeats,
        reserveSeatDialogOpen,
        setReserveSeatDialogOpen,

## ส่วนของ components ย่อยนี้จะเป็นการจองพื้นที่ของผู้ที่ได้รับสิทธืจองในพื้นที่นี้  ก็จะแสดงตารางข้อมูล เช่น โซน ,ประเภทพื้นที่ ,ประเภท ,รหัสที่นั่ง ,สถานะ,และก็การจอง

        <Grid item xs={12}>
          <TableContainer //**ปรับขนาดตาราง**
            sx={{
              marginRight: "1.3vh",
              overflowX: "auto !important",
              width: "100%",
              position: "relative",
              marginLeft: "-1vh",
              boxSizing: "border-box",
            }}
            component={Paper}
          >
            <Grid item xs={12}>
              <Table
                sx={{ width: "100%", maxWidth: "100%", maxHeight: "100%" }}
              >
                <TableHead sx={{ whiteSpace: "nowrap" }}>
                  <TableRow>
                    <CustomTableCell
                      value={
                        <Typography sx={{ marginLeft: "5vh" }}>
                          {intl.formatMessage({
                            id: "zone",
                            defaultMessage: "Zone",
                          })}
                        </Typography>
                      }
                    />
                    <CustomTableCell
                      value={
                        <Typography sx={{ marginLeft: "-1.5vh" }}>
                          {intl.formatMessage({
                            id: "area.type",
                            defaultMessage: "Area Type",
                          })}
                        </Typography>
                      }
                    />
                    <CustomTableCell
                      value={
                        <Typography sx={{ marginLeft: "3vh" }}>
                          {intl.formatMessage({
                            id: "type",
                            defaultMessage: "Type",
                          })}
                        </Typography>
                      }
                    />
                    <CustomTableCell
                      value={
                        <Typography sx={{ marginLeft: "2vh" }}>
                          {intl.formatMessage({
                            id: "seat.code",
                            defaultMessage: "Seat Code",
                          })}
                        </Typography>
                      }
                    />

                    <CustomTableCell
                      value={
                        <Typography sx={{ marginLeft: "2vh" }}>
                          {intl.formatMessage({
                            id: "status",
                            defaultMessage: "Status",
                          })}
                        </Typography>
                      }
                    />

                    <CustomTableCell value="" />
                  </TableRow>
                </TableHead> 

                <TableBody>
                  {seats?.data?.map((row, index) => {
                    return (
                      <StyledTableRow key={index}>
                        <CustomTableCell
                          value={
                            <Typography sx={{ marginLeft: "6vh" }}>
                              {row.zoneName}
                            </Typography>
                          }
                        />
                        <CustomTableCell
                          value={
                            <Typography sx={{ marginLeft: "-1vh" }}>
                              {row.areaTypeDesc}
                            </Typography>
                          }
                        />
                        <CustomTableCell
                          value={
                            <Typography sx={{ marginLeft: "1vh" }}>
                              {row.seatStatus == 2 ? "Fixed" : "Bookable"}
                            </Typography>
                          }
                        />
                        <CustomTableCell
                          value={
                            <Typography sx={{ marginLeft: "5vh" }}>
                              {row.seatLabel}
                            </Typography>
                          }
                        />

                        <CustomTableCell
                          value={
                            <Typography
                              sx={{
                                fontSize: "16px",
                                marginRight: "2vh",
                                color:
                                  row.availableStatus == 1
                                    ? "#07A982"
                                    : "#C11515",
                              }}
                            >
                              {row.availableStatus == 1
                                ? intl.formatMessage({
                                    id: "available",
                                    defaultMessage: "Available",
                                  })
                                : intl.formatMessage({
                                    id: "unavailable",
                                    defaultMessage: "Unavailable",
                                  })}
                            </Typography>
                          }
                        />
                        <CustomTableCell
                          value={
                            <Typography
                              sx={{
                                fontSize: "16px",
                                marginRight: "-1.8vh",
                                cursor: "pointer",
                                color:
                                  row.availableStatus == 1
                                    ? "#07A982"
                                    : "#C11515",
                              }}
                              onClick={() =>
                                row.availableStatus == 1
                                  ? handleReserveSeat(row)
                                  : handleCancelableBookingOpen(
                                      null,
                                      row.seatId
                                    )
                              }
                            >
                              {row.availableStatus == 1
                                ? intl.formatMessage({
                                    id: "reserve",
                                    defaultMessage: "Reserve",
                                  })
                                : intl.formatMessage({
                                    id: "cancel.booking",
                                    defaultMessage: "Cancel Booking",
                                  })}
                            </Typography>
                          }
                        />
                      </StyledTableRow>
                    );
                  })}
                </TableBody>
              </Table>
            </Grid>

โค้ดนี้เป็นส่วนหนึ่งของ React component ที่ใช้ในการแสดงข้อมูลที่ได้รับมาจาก seats?.data ในรูปแบบของตาราง (table) โดยใช้ Material-UI components เช่น Grid, TableContainer, Table, TableHead, TableBody, StyledTableRow, CustomTableCell, และ Typography.

## การปรับขนาดของตาราง:

มีการใช้ sx prop ใน TableContainer เพื่อปรับขนาดและสไตล์ของตาราง.
ได้กำหนด maxWidth, maxHeight, marginRight, marginLeft, และ position เพื่อให้ตารางมีขนาดและตำแหน่งที่ต้องการ.
## การแสดงหัวข้อของตาราง (TableHead):

มี TableHead ที่มี TableRow ภายใน.
ในแต่ละคอลัมน์ของหัวข้อ (CustomTableCell), ใช้ Typography และ intl.formatMessage เพื่อแสดงข้อความที่ถูกแปลตาม locale.
## การแสดงข้อมูลในแต่ละแถว (TableBody):

ใน TableBody, มีการใช้ .map บน seats?.data เพื่อสร้าง StyledTableRow สำหรับแต่ละที่นั่ง (seat).
แต่ละคอลัมน์ในแต่ละแถว (CustomTableCell) มีการใช้ Typography เพื่อแสดงข้อมูล.
มีการใช้ตัวจัดรูปแบบของข้อมูล (เช่น sx prop) เพื่อปรับแต่งรูปแบบและขนาดของข้อมูลแต่ละช่อง.
## การใช้งาน i18n (intl):

ในการแสดงข้อความที่ต้องการแปล, ใช้ intl.formatMessage โดยระบุ id และ defaultMessage เพื่อแสดงข้อความตาม locale ที่กำหนด.
## การทำงานเมื่อคลิกบนปุ่ม:

ในปุ่ม "Reserve" และ "Cancel Booking", มีการใช้ onClick เพื่อเรียกฟังก์ชันที่เกี่ยวข้อง เช่น handleReserveSeat หรือ handleCancelableBookingOpen.
การใช้งาน onClick นี้ถูกเชื่อมโยงกับสถานะ row.availableStatus เพื่อกำหนดว่าปุ่มนั้น ๆ ควรทำงานอะไร.




## ดึง API Manage Permission : Last Update

  **page

      import LicensesOfCompany from "@/components/user/Licenses/Company";
      import hasAuthRedirect from "@/services/Auth/has-auth-redirect";
      import { useRouter } from "next/router";

      export async function getServerSideProps(context) {
        let redirect;
        if ((redirect = hasAuthRedirect(context))) {
          return {
            redirect,
          };
        }

        return {
          props: {},
        };
      }

      const LicenseOfCompanyPage = () => {
        const {
          query: { slug },
        } = useRouter();

        return <LicensesOfCompany companyId={slug} />;
      };

      export default LicenseOfCompanyPage;

       

  ## Page LicenseOfCompanyPage เป็นดึง components จาก  LicensesOfCompany 
  มีหน้าที่ จัดการสิทธิ์การใช้งาน ของ User ดึงจาก /components/user/Licenses/Company

  ในส่วนที่ได้ทำก็คือ จะเป็นการ ดึงข้อมูล Lastupdate มาแสดงจาก api Last Update มาแสดง
    
    **components

      import LicenseLeftMenu from "./LicenseLeftMenu";

        const LicenseCard = ({ data, mutateRoleList, handleEdit, clearFormData }) => {
        const intl = useIntl();
  
        ..//
      <LicenseLeftMenu
            data={data}
            handleEdit={handleEdit}
            mutateRoleList={mutateRoleList}
            clearFormData={clearFormData}
          />
        </Stack>
        <Stack
          flexDirection={"row"}
          gap="4px"
          justifyContent={"start"}
          pl={"8px"}
        >
          <Typography sx={{ fontSize: "16px !important" , marginTop: "-4px" }}>
            {intl.formatMessage({
              id: "last.update",
              defaultMessage: "Last Update",
            })}
           {" : "} 
          </Typography>
 
      
  LicenseCard เป็น components ที่่ไว้แสดงข้อมูลเกี่ยวกับ จัดการสิทธิ์การใช้งาน

  ในส่วนที่ทำจะเป็นส่วนที่เป็น components ย่อยของ LicenseCard

    import CompanyLeftMenu from "./CompanyLeftMenu";

              const CompanyCard = ({
            data,
            mutateCompany,
            handleCompanyDialogClose,
            noMenu = false,
            handleCompanyDialogOpen,
            setValue,
            handleClick,
            companyId,
            page,
          }) => {
            const intl = useIntl();
            const router = useRouter();


จะเป็นการเอาข้อมูล APi มาแสดง แล้วสามารถ แก้ไข หรือ ลบข้อมูลได้

 
            
             **ดึงข้อมูล Lastupdate มาแสดงจาก api Last Update มาแสดง**

             import CompanyLeftMenu from "./CompanyLeftMenu";
                  <Stack
                    flexDirection={"row"}
                    justifyContent={"space-between"}
                    alignItems={"center"}
                  >
                    <Typography
                      sx={{
                        fontSize: "20px !important",
                        fontWeight: "600",
                        color: "#373a59",
                        marginLeft: 1,
                      }}
                    >
                      {data.name}
                    </Typography>
                    {noMenu && (
                      <CompanyLeftMenu
                        id={data.id}
                        system_Id={data.System_Id}
                        mutateCompany={mutateCompany}
                        handleCompanyDialogClose={handleCompanyDialogClose}
                        handleCompanyDialogOpen={handleCompanyDialogOpen}
                        setValue={setValue}
                        name={data.name}
                      />
                    )}
                  </Stack>
                  <Stack
                    flexDirection={"row"}
                    gap="4px"
                    justifyContent={"start"}
                    pl={"8px"}
                  >
                    <Typography sx={{ fontSize: "16px !important", marginTop: "-4px" }}>
                      {intl.formatMessage({
                        id: "last.update",
                        defaultMessage: "Last Update",
                      })}
                      {" : "}
                    </Typography> 
                    <Typography sx={{ fontSize: "16px !important", marginTop: "-4px" }}> 
                      {page === "manage-employee" && data?.lastUpdatedEmployeeManage  
                      //เชค Api เเต่ละ page  **
                        ? data.lastUpdatedEmployeeManage
                        : page === "company-in-charge" && data?.lastUpdatedCompanyInCharge
                        ? data.lastUpdatedCompanyInCharge
                        : page === "location-in-charge" &&
                          data?.lastUpdatedLocationInCharge
                        ? data.lastUpdatedLocationInCharge
                        : page === "space-usage" && data?.lastUpdatedSpaceUsage
                        ? data.lastUpdatedSpaceUsage
                        : page === "license" && data?.lastUpdatedLicense
                        ? data.lastUpdatedLicense
                        : data.updatedDateTime || data.lastUpdatedDate
                        ? data.updatedDateTime || data.lastUpdatedDate
                        : "-"}
                    </Typography>
                  </Stack>
                </Stack>


        
จะดึงเอา Api มาจาก components ย่อยของ CompanyCard คือ CompanyLeftMenu

### {noMenu && (...)}:
เป็นการใช้เงื่อนไขทาง logic โดยจะแสดง <CompanyLeftMenu> เฉพาะเมื่อ noMenu เป็น true. นั่นหมายถึงถ้าไม่มีเมนู (noMenu เป็น true) ก็จะแสดง <CompanyLeftMenu> โดยส่ง props ต่าง ๆ ไปยัง component.

### <Stack>: 
เป็นคอมโพเนนต์ Stack อีกครั้งที่ใช้ในการจัดวางคอมโพเนนต์ลูก (child components) ในลักษณะของ Stack หรือ Container โดยมี style ต่าง ๆ เช่น flexDirection, gap, justifyContent, และ paddingLeft.

### <Typography>: 
คอมโพเนนต์ที่ใช้ในการแสดงข้อความ โดยมีการใช้ intl.formatMessage เพื่อแปลงข้อความตามภาษาที่กำหนด (internationalization). มีการแสดงข้อความ "Last Update" และตามด้วยข้อมูลเกี่ยวกับการอัปเดตล่าสุดของข้อมูล.

### ส่วน {page === "..." && data?.lastUpdated...}: 
เป็นการใช้เงื่อนไขทาง logic โดยแสดงข้อมูลเกี่ยวกับการอัปเดตล่าสุดขึ้นอยู่กับหน้า (page) ที่กำลังแสดงอยู่. จะตรวจสอบหน้าปัจจุบันว่าเป็นหน้าไหน และแสดงข้อมูลการอัปเดตล่าสุดที่เกี่ยวข้องกับหน้านั้น ๆ.

## **สรุปได้ว่า, โค้ดนี้ใช้ในการแสดง UI ที่มีการตรวจสอบเงื่อนไขก่อนการแสดงบางส่วน และมีการแสดงข้อมูลเกี่ยวกับการอัปเดตล่าสุดของข้อมูลซึ่งขึ้นอยู่กับหน้าที่กำลังแสดงอยู่ในแอปพลิเคชัน React นั้น ๆ.


## จัดการการใช้พื้นที่ : แก้ไข pop up รายละเอียดการขออนุมัติ
 ในส่วนนี้จะเป็น pages การจัดการ  การใช้พื้นที่ของบริษัท 

      import ManageCompanySpace from "@/components/user/ManageComapnySpace"; 
      //เป็น component ที่ใช้ในการแสดงหน้าจัดการพื้นที่ของบริษัท.
      import hasAuthRedirect from "@/services/Auth/has-auth-redirect";
      //เป็นฟังก์ชันที่ใช้ในการตรวจสอบสิทธิ์การเข้าถึงและการ redirect.
      import { useRouter } from "next/router";
      //  useRouter จาก Next.js ซึ่งให้การเข้าถึง object ของ router ที่ใช้ในการดึงข้อมูลจาก URL
      import React from "react";

      export async function getServerSideProps(context) { 
        //ส่วนนี้ใช้สำหรับการทำ Server-side Rendering ด้วย Next.js ซึ่งเป็นฟังก์ชันที่รันทุกรอบที่มีการเข้าถึงหน้านี้
        let redirect;
        if ((redirect = hasAuthRedirect(context))) {
          return {
            redirect,
          };
        }

        return {
          props: {},
        };
      }

      const ManageCompanySpacePage = () => {
        const {
          query: { slug },
        } = useRouter();
        return <>{slug && <ManageCompanySpace slug={slug} />}</>;
      };
      //สร้าง functional component เพื่อดึงข้อมูล slug จาก URL parameters และแสดง 
      ManageCompanySpace component ถ้า slug มีค่า.

      export default ManageCompanySpacePage;



ในส่วนของ component ที่เรียกใช้ก็คือ  ManageCompanySpace ที่ใช้แสดงข้อมูลเกี่ยวกับ รายการพนักงาน

      const ManageCompanySpace = ({ slug }) => {
      const [isButtonHidden, setIsButtonHidden] = useState(true)
      const [open, setOpen] = useState(false)
      const [selected, setSelected] = useState(null)
      // const [companyEmployeeList, setCompanyEmployeeList] = useState([]);
      const [pageNo, setPageNo] = useState(1)
      const [searchKey, setSearchKey] = useState('')
      const [employeeName, setEmployeeName] = useState('')
      const [employeeId, setEmployeeId] = useState(null)
      const [companyName, setCompanyName] = useState('')
      // const [meeting]
      const intl = useIntl()
      const filterRef = useRef()

## ในส่วนที่ได้ทำคือ 
### จะเป็นการ เรียก component  LocationApprovalDetailDialog ที่ไว้แสดง popup รายละเอียดการขออนุมัติ เช่น ชื่อผู้ขอ ,รายการที่ขอ , ประเภทพื้นที่ ,วันที่เริ่มต้น, วันที่สิ้นสุด 


      <LocationApprovalDetailDialog
            open={open}
            handleClose={handleClose}
            locationList={locationList?.data}
            areaList={areaList?.data}
            hookform={hookform}
            employeeName={employeeName}
            meetingRoom={meetingRoom}
            specialMeetingRoom={specialMeetingRoom}
            employeeId={employeeId}
            selected={selected}
            setSelected={setSelected}
            locationRequest={locationRequest}
            companyId={slug}
            isButtonHidden={isButtonHidden}
          />


## ในส่วนที่แก้ไข

  ## สิ่งที่ต้องการที่จะทำคือ 
  เมื่อเราเลือก รายการสถานที่ที่แสดง ต้องแสดงสถานที่ทั้งหมดเมื่อเลือกสถานที่ที่ต้องการ พื้นที่ใน list ที่แสดง ต้องเป็นพื้นที่ที่ผูกไว้กับสถานที่เท่านั้น
  


          <Grid
                  item
                  xs={6}
                  sx={{
                    marginTop: "-5px",
                  }}
                >
                  <Controller
                  //เป็นคอมโพเนนต์ที่ใช้กับ React Hook Form (control และ name prop) เพื่อจัดการ state ของฟอร์ม.
                    sx={{ width: "100% !important" }}
                    control={control}
                    name="location"
                    render={({ field, fieldState: { error } }) => {
                      const fieldWithoutRef = {
                        ...field,
                        ref: undefined,
                      };
                      return (
                        console.log("locationList", locationList),
                         */ทำการ log ค่าของ locationList และ areaList ลงใน console. น่าจะใช้เพื่อ debug หรือตรวจสอบค่าของตัวแปรในระหว่างการพัฒนา.
                        (
                          <CustomSelect
                          //เป็นคอมโพเนนต์ที่ถูกสร้างขึ้นเองหรือทำการ customize จาก Material-UI Select component
                            disable={status == 1 && true} //ใช้ prop disable เพื่อปิดใช้งาน Select ในกรณีที่ status เท่ากับ 1 (true).
                            {...fieldWithoutRef}
                            fullWidth={true} //  ใช้ spread operator ({...}) เพื่อนำข้อมูลทั้งหมดใน fieldWithoutRef มาใส่เป็น prop ใน CustomSelect component. fieldWithoutRef
                            inputRef={field.ref}
                            label={intl.formatMessage({ 
                              //ใช้ prop label เพื่อกำหนดข้อความ label ของ Select โดยใช้ internationalization (i18n) จาก intl.formatMessage
                              id: "location",
                              defaultMessage: "Location",
                            })}

                            options={locationList}
                            optionLabel={
                              locale == "th"
                                ? "name"
                                : "englishName"
                            }
                            // ใช้ prop optionLabel เพื่อกำหนด field ที่จะใช้เป็น label ของตัวเลือก โดยขึ้นอยู่กับ locale ที่กำหนดค่าในโค้ด. เป็นการแปลภาษา En กับ Th
                            optionValue="id"
                            error={error}
                            sx={{ backgroundColor: "#fff" }}
                             onChange={(e) => {
                              const selectedValue = e.target.value; //นี้คือการดึงค่าที่ถูกเลือกมาจากตัวเลือกที่ผู้ใช้เลือกใน CustomSelect ผ่านอีเวนต์ onChange. e.target.value คือค่าที่ถูกเลือก.
                             console.log('Selected Location Value:', selectedValue); // ทำการ log ค่าที่ถูกเลือกลงใน console
                             setSelectedLocation(selectedValue); // ฟังก์ชันที่กำหนดค่า state สำหรับ selectedLocation) เพื่ออัปเดตค่า state ด้วยค่าที่ถูกเลือก.
                             const correspondingArea = getFilteredAreas(selectedValue); //เป็นการเรียกใช้ฟังก์ชัน getFilteredAreas โดยส่งค่าที่ถูกเลือกมาเป็นอาร์กิวเมนต์ เพื่อรับข้อมูลที่เกี่ยวข้องหรือกรองตามค่าที่ถูกเลือก 
                             console.log('Corresponding Area:', correspondingArea);
                             setSelectedArea(correspondingArea); // เป็นฟังก์ชันที่กำหนดค่า state สำหรับ selectedArea) เพื่ออัปเดตค่า state ด้วยข้อมูลที่ได้หลัง
                             }}
                          />
                        )
                      );
                    }}
                  />
                </Grid>

               ** นี้คือกระบวนการที่ทำงานเมื่อมีการเลือกตัวเลือกใน CustomSelect: นำค่าที่ถูกเลือกไปใช้ในการอัปเดต state 
                และปรับข้อมูลตามต้องการ (เช่นการกรองข้อมูลที่เกี่ยวข้อง). การ log ค่าที่ถูกเลือกและข้อมูลที่ได้ใน console
                 ช่วยในกระบวนการ Debug และตรวจสอบการทำงานของโค้ด

                <Grid
                  item
                  xs={6}
                  sx={{
                    marginTop: "-5px",
                  }}
                >
                  <Controller
                    sx={{ width: "100% !important" }}
                    control={control}
                    name="area"
                    render={({ field, fieldState: { error } }) => {
                      const fieldWithoutRef = {
                        ...field,
                        ref: undefined,
                      };
                      return (
                        console.log("areaList", areaList),
                        (
                          // The rest of your code...
                          <CustomSelect
                            disable={status == 1 && true}
                            {...fieldWithoutRef}
                            fullWidth={true}
                            inputRef={field.ref}
                            label={intl.formatMessage({
                              id: "area",
                              defaultMessage: "Area",
                            })}
                            options={areaList ? areaList : areaListOne} //: นี่คือการกำหนด options ใน CustomSelect โดยตรวจสอบว่า areaList มีค่าหรือไม่ ถ้ามีก็ใช้ 
                            optionLabel={
                              areaListOne.length !== 0 //จะเป็นการเชคการแปลภาษาจาก Apiที่่ดึงมาสำหรับรายการพนักงาน 
                                ? locale === "th" // ตรวจสอบภาษาปัจจุบันที่กำลังใช้งาน, ถ้าเป็น "th" จะใช้เงื่อนไขในส่วนถัดไป.
                                  ? areaListOne?.nameTh !== "" &&  //ถ้า nameTh ใน areaListOne มีค่าและไม่เป็นช่องว่าง, ให้ใช้ "nameTh", ไม่然และ name ไม่เป็นช่องว่างให้ใช้ "name". นั่นคือ, ใช้ nameTh ถ้ามีข้อมูลภาษาไทย, ไม่มีให้ใช้ name 
                                    areaListOne?.nameTh
                                    ? "nameTh"
                                    : "name"
                                  : areaListOne?.engName !== ""
                                  ? "engName"
                                  : "englishName"
                                : locale === "th"
                                ? "name"
                                : "engName"
                            }
                            optionValue="id"
                            error={error}
                            sx={{ backgroundColor: "#fff" }}
                            **ตัว optionLabel นี้ถูกใช้เพื่อกำหนด label ของตัวเลือกใน CustomSelect โดยมีการตรวจสอบเงื่อนไขในหลาย ๆ ระดับเพื่อเลือก label ที่ถูกต้องตามเงื่อนไขที่กำหนด.
                          />
                        )
                      );
                    }}
                  />
                </Grid>
              </Grid>
        


## Area Administration : pagination 
แสดงตำแหน่ง pagination ใต้ข้อมูล เวลาเพิ่มข้อมูลให้แสดงในหน้าใหม่

    **pages

    import Reserve from "@/components/Booking/Reserve";
    export async function getServerSideProps(context) {
      let redirect;
      if ((redirect = hasAuthRedirect(context))) {
        return {
          redirect,
        };
      }

      return {
        props: {},
      };
    }

    const ReservePage = () => {
      return <Reserve />;
    };

    export default ReservePage;


จะเป็นการเรียก component จาก <Reserve />

** component 

     const Reserve = ({ bookingData, submitFunction = createBooking }) => {
                const router = useRouter();
                const intl = useIntl();
                const { showLoading } = useLoading();
                const { enqueueSnackbar } = useSnackbar();
                const [viewType, setViewType] = useState(2);
                const [cancelableBookingOpen, setCancelableBookingOpen] = useState(false);
                const [cancelableBookingList, setCancelableBookingList] = useState([]);
                const [selectedBookingIdList, setSelectedBookingIdList] = useState([]);
                const [reserveSeatDialogOpen, setReserveSeatDialogOpen] = useState(false);
                const [attendanceOpen, setAttendanceOpen] = useState(false);
                const { user } = useUser();
                const [pageNo, setPageNo] = useState(1);

ในส่วนนี้จะเป็นการแสดง การบริหารพื้นที่ เป็นการค้นหาข้อมูลที่มีอยู่ Data 

## เป็นการแสดงข้อมูลในรูปแปป ตาราง เช่น แสดง โซน,ประเภทพื้นที่,ประเภท,รหัสที่นั่ง,สถานะ
## ในส่วนที่ทำก็คือ CustomPagination ใต้ข้อมูล ที่จะแสดงหน้าถัดไป (หน้าที่) ในกรณีที่มีจำนวนรายการมากและต้องแบ่งหน้า. นี้คืออธิบายเกี่ยวกับโค้ด:


           {Math.ceil(seats?.totalCount / 10) > 1 || //(: นี้คือการตรวจสอบว่าจำนวนหน้าทั้งหมดที่จะต้องแสดง (Math.ceil(seats?.totalCount / 10) และ Math.ceil(availableRooms?.totalCount / 10)) มีมากกว่า 1 หรือไม่. ถ้าใช่, ก็ทำการแสดง Pagination component.
            Math.ceil(availableRooms?.totalCount / 10) > 1 ? (
              <Grid item xs={12}>
                <Box
                  sx={{ display: "flex", justifyContent: "flex-end", mt: 2 }}
                >
                  <CustomPagination //นี้คือการนำเข้าและใช้ CustomPagination component ที่ถูกสร้างขึ้นเอง. Props ที่ถูกส่งไปประกอบด้วย 
                    page={pageNo} //ระบุหน้าปัจจุบันที่ต้องการแสดง 
                    handleChange={handlePaginationChange} ระบุฟังก์ชันที่จะถูกเรียกเมื่อเปลี่ยนหน้า.
                    totalPageCount={Math.ceil(
                      seats
                        ? seats?.totalCount / 10
                        : availableRooms?.totalCount / 10 //ระบุจำนวนหน้าทั้งหมดของ Pagination, คำนวณจากจำนวนรายการทั้งหมดที่ให้มา และแบ่งด้วยจำนวนรายการที่แสดงในแต่ละหน้
                    )}
                  />
                </Box>
              </Grid>
            ) : null}
            **โค้ดนี้ใช้เงื่อนไขเพื่อตรวจสอบว่าจำนวนหน้าทั้งหมดที่จะแสดงใน Pagination มีมากกว่า 1 หรือไม่ ถ้าใช่ก็จะแสดง Pagination component ที่ถูกสร้างขึ้นเพื่อให้ผู้ใช้สามารถเลือกหน้าที่ต้องการได้.

## ในส่วน component CustomPagination 
  โค้ดนี้เป็นการสร้าง Custom Pagination component โดยใช้ Material-UI library ซึ่งเป็นส่วนหนึ่งของ React functional component ที่ชื่อ CustomPagination. นี่คืออธิบายเกี่ยวกับโค้ด:
          
                import * as React from "react";
              import Typography from "@mui/material/Typography";
              import Pagination from "@mui/material/Pagination"; // นำเข้า Pagination component จาก Material-UI library.
              import Stack from "@mui/material/Stack";

              export default function CustomPagination({ //และรับ prop ทั้งหมดสามตัว:
                page, //ระบุหน้าปัจจุบันที่ถูกเลือก.
                handleChange, // ฟังก์ชันที่จะถูกเรียกเมื่อมีการเปลี่ยนหน้า. 
                totalPageCount, //ระบุจำนวนหน้าทั้งหมดของ Pagination. 
              }) {
                console.log(page, "page");

                // React.useEffect(() => {
                //   handlePaginationChange(page);
                //   console.log(page);
                // }, [page]);

                // React.useEffect(() => {
                //   setPage(1);
                // }, [pageStatus]);

                return ( /นี้คือส่วนที่สร้าง JSX สำหรับการแสดง Pagination component ที่มีการกำหนด prop ต่าง ๆ:
                  <Pagination count={totalPageCount} //ระบุจำนวนหน้าทั้งหมดของ Pagination.
                   page={page} //ระบุหน้าปัจจุบันที่ถูกเลือก.
                   onChange={handleChange} //ระบุฟังก์ชันที่จะถูกเรียกเมื่อมีการเปลี่ยนหน้า.
                    />
                );
              }

  ** CustomPagination component นี้ถูกสร้างขึ้นเพื่อให้การใช้งาน Pagination ของ Material-UI เป็นไปอย่างยืดหยุ่น, และสามารถส่ง prop ต่าง ๆ เข้าไปเพื่อกำหนดค่าใน Pagination ตามความต้องการ.
          
## ทำกาด  :booking Bug : เมื่อ Login ด้วย UserName อื่นแล้วเกิดบัค

    **page
      import React from "react";
      import Waitinglogin from "@/components/WaitingLogin";

      const WaitingLoginPage  = () => {
          return <Waitinglogin />;
        };
        
        export default WaitingLoginPage ;

เป็นการเรียก components <Waitinglogin /> มาใช้งาน มีหน้าที่เมื่อ User login หรือ admin จะไปยังหน้าที่ User มีสิทธิ์ เท่านั้น


        
    **component

              import { Container, Grid, IconButton, Stack, Typography } from "@mui/material"; //นำเข้าคอมโพเนนต์ UI จาก Material-UI เพื่อใช้ในการสร้างเลย์เอาต์.
              import NextImage from "next/image";
              import Link from "next/link"; // Import Link from Next.js //นำเข้าคอมโพเนนต์ Next.js สำหรับการจัดการรูปภาพ. 
              import { useRouter } from "next/router"; //นำเข้าฟังก์ชัน Hook เพื่อให้เข้าถึงข้อมูลเกี่ยวกับเส้นทาง (route) ใน Next.js. 


              import React, { useRef } from "react";

              import Search from "@/components/Search"; //เป็นคอมโพเนนต์ที่ได้มีการสร้างขึ้นเพื่อการค้นหา ซึ่งน่าจะถูกนำเข้ามาแล้วในโค้ดที่ไม่ได้แสดง.

              const Waitinglogin = () => {
                const filterRef = useRef(null);

                const router = useRouter();

                const handleLocationManagementClick = () => {
                  // Use the appropriate route constant from your application //เมื่อคลิกที่ปุ่ม จะนำทางไปยังเส้นทาง "/location/place-management".
                  const ROUTE_PLACE_MANAGEMENT = "/location/place-management";
                  // Navigate to the specified route
                  router.push(ROUTE_PLACE_MANAGEMENT);
                };

                const handleLocationCompanyClick = () => {
                  const  ROUTE_MANAGE_COMPANY  = "/location/manage-company" //เมื่อคลิกที่ปุ่ม จะนำทางไปยังเส้นทาง "/location/manage-company".
                  router.push(ROUTE_MANAGE_COMPANY);
                };

                const handleLocationAreaClick = () => {
                  const  ROUTE_MANAGE_AREA = "/location/manage-space" //เมื่อคลิกที่ปุ่ม จะนำทางไปยังเส้นทาง "/location/manage-space". 
                  router.push(ROUTE_MANAGE_AREA);
                };

          /*ค้ดด้านบนน่าจะเป็นส่วนของหน้า Waitinglogin ซึ่งประกอบไปด้วยการนำเข้าคอมโพเนนต์ต่าง ๆ และฟังก์ชันที่จัดการเหตุการณ์เมื่อมีการคลิกที่ปุ่ม เพื่อนำทางไปยังเส้นทางที่กำหนด


                        <MainLayout>
                          <Container maxWidth={"xl"}>
                            <Grid container mt={2} spacing={2}>
                              <Grid item xs={12}>
                                <Stack
                                  flexDirection={"row"}
                                  justifyContent={"space-between"}
                                  alignItems={"center"}
                                >
                                  <Stack flexDirection={"row"} alignItems={"center"}>
                                    {/* Link to Location Management */}
                                    <Link href="/location/place-management" passHref>
                                      <Typography sx={{ fontSize: "38px !important", fontWeight: 500 }}
                                      onClick={handleLocationManagementClick}
                                      >
                                        
                                        {"Location Management "}
                                      </Typography>
                                    </Link>
                                    </Stack>
                                    {/* Link to Manage Company */}
                                    <Stack flexDirection={"row"} alignItems={"center"}>
                                    <Link href="/location/manage-company" passHref>
                                      <Typography sx={{ fontSize: "38px !important", fontWeight: 500 }}
                                      onClick={handleLocationCompanyClick}
                                      >
                                      
                                        {"Manage Company"}
                                      </Typography>
                                    </Link>
                                    
                                  </Stack>
                                  <Stack flexDirection={"row"} alignItems={"center"}></Stack>
                                  <Link href="/location/manage-space" passHref>
                                      <Typography sx={{ fontSize: "38px !important", fontWeight: 500 }}
                                      onClick={handleLocationAreaClick}
                                      >
                                        
                                        {"Manage Area"}
                                      </Typography>
                                    </Link>
                                    
                                  <Search filterRef={filterRef} />
                                </Stack>
                                {/* No need for Company List and Pagination */}
                              </Grid>
                            </Grid>
                          </Container>
                        </MainLayout>
  


  ## <MainLayout>: เป็นคอมโพเนนต์ที่ใช้เป็นเลย์เอาต์หลักของหน้า Waitinglogin. นั่นหมายความว่ามีการใส่เนื้อหาหน้านี้ในเลย์เอาต์ที่กำหนดใน MainLayout.

## <Container>: คอมโพเนนต์ที่ใช้สร้างคอนเทนเนอร์ (container) ภายในหน้า Waitinglogin โดยกำหนด maxWidth เป็น "xl" เพื่อให้มีขนาดคอนเทนเนอร์ใหญ่มาก.

## <Grid>: ใช้เพื่อสร้างกริด (grid) ของคอนเทนเนอร์ภายใน Container. มี mt={2} และ spacing={2} ในการกำหนดระยะห่างและการจัดวาง.

## <Stack>: นำเข้าคอมโพเนนต์ Stack จาก Material-UI เพื่อจัดวางองค์ประกอบในลักษณะของ Stack. มีการกำหนด flexDirection, justifyContent, และ alignItems เพื่อจัดวางองค์ประกอบในลักษณะที่ต้องการ.

# ลิงก์ไปยังหน้าที่เกี่ยวข้อง:

## ใน Stack มีลิงก์ไปยัง "/location/place-management" และกำหนด Typography ด้วยขนาดและน้ำหนักตัวอักษรที่กำหนด.
 มีการใส่ onClick เพื่อเรียกใช้ฟังก์ชัน handleLocationManagementClick เมื่อ Typography ถูกคลิก.
## ใน Stack ยังมีลิงก์ไปยัง "/location/manage-company" และกำหนด Typography ด้วยขนาดและน้ำหนักตัวอักษรที่กำหนด
. มีการใส่ onClick เพื่อเรียกใช้ handleLocationCompanyClick เมื่อ Typography ถูกคลิก.
มีลิงก์ไปยัง "/location/manage-space" และกำหนด Typography ด้วยขนาดและน้ำหนักตัวอักษรที่กำหนด. 

มีการใส่ onClick เพื่อเรียกใช้ handleLocationAreaClick เมื่อ Typography ถูกคลิก.

## <Search>: นำเข้าคอมโพเนนต์ Search ที่อาจจะเป็นกล่องค้นหาหรือฟอร์มค้นหา และใส่ filterRef เพื่อให้เชื่อมต่อกับ ref ที่ถูกสร้างไว้ในคอมโพเนนต์ Waitinglogin.