import { Box, Button, TextField, Typography } from "@mui/material";
import { useState } from "react";
import NavBarEcom from "src/components/waiwitz/workshopSix/navBar";

const shopLogin = () => {
    const [username, setUsername] = useState('mor_2314');
    const [password, setPassword] = useState('83r5^_');
    const [validUsername, setValidUsername] = useState(false);
    const [validPassword, setvalidPassword] = useState(false);
    const [helperTextUsername, setHelperTextUsername] = useState();
    const [helperTextPassword, setHelperTextPassword] = useState();

    if (localStorage.getItem('Token')) {
        window.location.href = '/workShop/waiwitz/workshopSix'
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        fetch(`https://fakestoreapi.com/auth/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                username, password
            })
        }).then(async (res) => {
            if (res.ok) {
                const data = await res.json();
                console.log(data);
                localStorage.setItem('Token', data.token);
                console.info('เข้าสู่ระบบสำเร็จ');
                console.log(`Token : ${localStorage.getItem('Token')}`)
                alert(`เข้าสู่ระบบสำเร็จ`)
                window.location.href = '/workShop/waiwitz/workshopSix'
            } else {
                console.error(`status : ${res.status}`);
                console.info(`info : ${res.message}`);
                !username ? setValidUsername(true) & setHelperTextUsername('กรุณากรอกชื่อผู้ใช้') : ''
                !password ? setvalidPassword(true) & setHelperTextPassword('กรุณากรอกรหัสผ่าน') : ''
                if (!username || !password) {
                    console.info(`ช่องชื่อผู้ใช้หรือรหัสผ่านว่าง `);
                    console.info(`username : '${username}', password : '${password}'`);
                } else if (username != res.username || password != res.password) {
                    console.info(res.message)
                    alert('ชื่อผู้ใช้หรือรหัสผ่านไม่ตรง')
                }
            }
        })
    }

    return (
        <>
            <NavBarEcom />
            <Box margin={'auto'} padding={5} >
                <Typography variant="h3" gutterBottom>Login</Typography>
                <form noValidate autoComplete="off" onSubmit={handleSubmit}>
                    <div>
                        <TextField error={validUsername} helperText={helperTextUsername} type="text"
                            label="Username"
                            defaultValue={'mor_2314'}
                            sx={{ width: '100%', marginBottom: '1em' }}
                            onChange={(e) => setUsername(e.target.value)} />
                        <TextField error={validPassword} helperText={helperTextPassword} label="Password" sx={{ width: '100%' }}
                            defaultValue={'83r5^_'} type="password"
                            onChange={(e) => setPassword(e.target.value)} />
                    </div>
                    <Button type="submit" variant="outlined" color="secondary" sx={{ marginTop: '10px', width: '100%' }} >เข้าสู่ระบบ</Button>
                </form>
            </Box>
        </>
    )
}

export default shopLogin;