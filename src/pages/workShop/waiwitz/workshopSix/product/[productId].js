import { Box, Button, CardMedia, Chip, Grid, Rating, Skeleton, Tab, Tabs, Typography } from "@mui/material";
import { Container } from "@mui/system";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import NavBarEcom from "src/components/waiwitz/workshopSix/navBar";
import PropTypes from 'prop-types';
import Image from "next/image";
import styles from "src/styles/waiwitzStyles.module.css";

const product = () => {
    const [product, setProduct] = useState([]);
    const [cartsItem, setCartsItem] = useState(JSON.parse(localStorage.getItem('carts')) || []);
    const [rateValue, setRateValue] = useState();
    const [count, setCount] = useState();
    const [loading, setLoading] = useState(true);

    const router = useRouter();
    const { productId } = router.query;
    useEffect(() => {
        const getProductDetail = async () => {
            const res = await fetch(`https://fakestoreapi.com/products/${productId}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json'
                },
            });
            try {
                if (productId) {
                    if (res.ok) {
                        const data = await res.json();
                        setProduct(data)
                        const rating = data['rating'];
                        console.log(rating);
                        setRateValue(rating.rate)
                        setCount(rating.count)
                        setLoading(false);
                    } else {
                        return (
                            <>
                                มะเจอสินค้า
                            </>
                        )
                    }
                }
            } catch (error) {
                console.log(error);
                return (
                    <>
                        มุแง้
                    </>
                )
            }
        };
        getProductDetail();
    }, [productId]);

    const addCart = (item) => {
        const updatedCart = [...cartsItem, item];
        setCartsItem(updatedCart);
        localStorage.setItem('carts', JSON.stringify(updatedCart));
        alert('เพิ่มลงตะกร้าเรียบร้อยแล้ว');
      };
    
    return (
        <>
            <NavBarEcom carts={cartsItem}/>
            <Container maxWidth='xl' sx={{ mt: 20 }}>
                {loading ?
                    <Grid container spacing={4} justifyContent={'center'}>
                        <Grid item xs={5}>
                            <Skeleton variant="rounded" width={500} height={500} />
                        </Grid>
                        <Grid item xs={6}>
                            <Skeleton animation="wave" />
                        </Grid>
                    </Grid>

                    :
                    <Grid container spacing={4} justifyContent={'center'}>
                        <Grid item xs={5}>
                            <Box sx={{ width: 500, height: 500, border: 'solid 1px #ddd', overflow: 'hidden', borderRadius: 2 }}>
                                <img src={product.image} style={{ objectFit: 'cover', height: '60%', margin: '5em auto' }} />
                            </Box>
                        </Grid>
                        <Grid item xs={6}>
                            <Chip label={product.category} sx={{ mb: 2 }} />
                            <Typography variant="h4">{product.title}</Typography>
                            <Box display={'flex'} mt={2}>
                                <Rating name="read-only" value={rateValue} readOnly sx={{ mr: 2 }} />
                                <Typography variant="body2" alignSelf={'center'}>{count} Reviews</Typography>
                            </Box>
                            <Typography fontSize={'26px'} my={2}>${product.price}</Typography>
                            <Typography variant='body2'>{product.description}</Typography>
                            {localStorage.getItem('Token') ?
                             <Button sx={{ width: '100%', mt: 3 }} variant="outlined" onClick={() => addCart(product)} className={styles.addToCart}>เพิ่มลงตะกร้า</Button> 
                            :  <Button sx={{ width: '100%', mt: 3 }} variant="outlined" disabled={true}>กรุณาเข้าสู่ระบบก่อน</Button> }
                        </Grid>
                    </Grid>
                }
            </Container>
        </>
    );
}


export default product;