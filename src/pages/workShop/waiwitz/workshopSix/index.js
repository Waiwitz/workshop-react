import { Box, Button, Card, CardContent, CardMedia, Container, Grid, ListItem, ListItemButton, Skeleton, Typography } from "@mui/material";
import Link from "next/link";
import { useEffect, useState } from "react";
import NavBarEcom from "src/components/waiwitz/workshopSix/navBar";
import SearchFilter from "../../../../components/waiwitz/workshopSix/serachFilter";


const workshop6 = () => {
    const [allProduct, setAllProduct] = useState([]);
    const [allCate, setAllCate] = useState([]);
    const [loading, setLoading] = useState(true);
    const [searchInput, setSearchInput] = useState('');
    const [searchCate, setSearchCate] = useState([]);
    const [searchPrice, setSearchPrice] = useState('');

    const getAPI = (api, setArr) => {
        fetch(`${api}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json'
            },
        }).then(async (res) => {
            if (res.ok) {
                const data = await res.json();
                console.log(data);
                setLoading(false)
                return setArr(data);
            } else {
                return false;
            }
        })
    };
    useEffect(() => {
        const getAllProduct = () => getAPI('https://fakestoreapi.com/products', setAllProduct);
        const getAllCate = () => getAPI('https://fakestoreapi.com/products/categories', setAllCate);
        getAllProduct();
        getAllCate();
    }, [])

    const filterName = allProduct.filter((product) => {
        const inputFilter = product.title.toLowerCase().includes(searchInput.trim().toLowerCase())
        const cateMatch = searchCate.length === 0 || searchCate.includes(product.category);
        return inputFilter && cateMatch
    }).sort((a, b) => {
        if (searchPrice == 'lowToHigh') {
            return a.price - b.price;
        } else if (searchPrice == 'highToLow') {
            return b.price - a.price;
        } else {
            return 0;
        }
    })

    const SkeletonLoading = () => {
        return (
            <Box mb={5}>
                <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }} sx={{ justifyContent: { xs: 'center', md: 'start' } }}>
                    <Grid item xs={2.4}>
                        <Skeleton variant="rounded" width={'inherit'} height={450} />
                    </Grid>
                    <Grid item xs={2.4}>
                        <Skeleton variant="rounded" width={'inherit'} height={450} />
                    </Grid>
                    <Grid item xs={2.4}>
                        <Skeleton variant="rounded" width={'inherit'} height={450} />
                    </Grid>
                    <Grid item xs={2.4}>
                        <Skeleton variant="rounded" width={'inherit'} height={450} />
                    </Grid>
                    <Grid item xs={2.4}>
                        <Skeleton variant="rounded" width={'inherit'} height={450} />
                    </Grid>
                </Grid>
            </Box>
        )
    }

    return (
        <>
            <NavBarEcom />
            <Container maxWidth='xl' sx={{ mt: 10 }}>
                {loading ?
                    <SkeletonLoading />
                    :
                    <Box my={5}>
                        <SearchFilter categories={allCate}
                            inputSetState={setSearchInput}
                            selectCate={searchCate}
                            cateSetState={setSearchCate}
                            selectPrice={searchPrice}
                            priceSetState={setSearchPrice} />
                        <Grid container spacing={{ xs: 2, md: 3 }} columns={{ xs: 4, sm: 8, md: 12 }} sx={{justifyContent: { xs: 'center', md: 'start' } }}>
                            {filterName.map((item, index) => (
                                <Grid item xs={3} key={index}>
                                    <Card sx={{ height: 450, position: 'relative', borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }}>
                                        <Box sx={{ width: 'inherit', height: 250, margin: '1em', border: 'solid 1px #ddd', borderRadius: '10px' }}>
                                            <CardMedia
                                                component="img"
                                                sx={{ width: 'inherit', height: 150, margin: '2em auto' }}
                                                image={item.image}
                                                alt={item.name}
                                            />
                                        </Box>
                                        <Box mx={3} component={'div'} textOverflow={'ellipsis'} overflow={'hidden'}>
                                            <Typography variant="subtitle1" gutterBottom sx={{ whiteSpace: 'nowrap', overflow: 'hidden', textOverflow: 'ellipsis' }}>{item.title}</Typography>
                                            <Typography variant="caption" gutterBottom sx={{ whiteSpace: 'nowrap', textOverflow: 'ellipsis' }}>{item.description}</Typography>
                                            <Typography variant="body2" fontWeight={'bold'} textAlign={'center'} mt={2}>${item.price}</Typography>
                                        </Box>
                                        <Link href={`/workShop/waiwitz/workshopSix/product/${item.id}`}>
                                            <Button sx={{ width: '87%', position: 'absolute', bottom: 15, mx: 2 }} variant="outlined">watch</Button>
                                        </Link>
                                    </Card>
                                </Grid>
                            ))}
                        </Grid>
                    </Box>
                }
            </Container>
        </>
    )
}

export default workshop6;