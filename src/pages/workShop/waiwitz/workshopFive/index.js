import { Card, Container, CardContent, Grid, Button, Box, CardHeader, Modal, Typography, CardActions } from "@mui/material";
import { useState } from "react";
import KeyboardArrowRightRoundedIcon from '@mui/icons-material/KeyboardArrowRightRounded';
import UserList from "src/components/waiwitz/workshop5/userList";
import UserForm from "src/components/waiwitz/workshop5/userForm";
import Head from "next/head";
import BackButton from "src/components/waiwitz/backButton";


const workshopFive = () => {
    const [users, setUsers] = useState([])
    const [open, setOpen] = useState(false);
    const [openEdit, setOpenEdit] = useState(false)
    const [openDelete, setOpenDelete] = useState(false)
    const [api, setApi] = useState(null);
    const [form, setForm] = useState('');

    const [userId, setUserId] = useState();
    const [fname, setFname] = useState();
    const [lname, setLname] = useState();
    const [username, setUsername] = useState();
    const [password, setPassword] = useState();
    const [email, setEmail] = useState();
    const [avatar, setAvatar] = useState('https://www.melivecode.com/users/cat.png');

    const handleOpen = (i) => {
        let config = i.config;
        let crud = i.crud;
        setOpenEdit(false);
        setOpen(true);

        if (config) setApi(config);
        if (crud) setForm(crud)

        setUserId('')
        setUsername('');
        setPassword('');
        setFname('');
        setLname('');
        setEmail('');
        // if (crud == 'update_delete_user') {
        //     CallUserlist();
        // }
    }

    const handleOpenEdit = (user) => {
        setOpen(false);
        setOpenEdit(true);

        setUserId(user.id)
        setUsername(user.username);
        setPassword(user.password);
        setFname(user.fname);
        setLname(user.lname);
        setEmail(user.email);
    }

    const handleOpenDelete = (user) => {
        setOpen(false);
        setOpenEdit(false);
        setOpenDelete(true);
        setUserId(user.id)
        setUsername(user.username);
    }

    const handleClose = () => {
        setOpen(false);
        setOpenEdit(false);
        setOpenDelete(false);
    }
    const userField = {
        "id": userId,
        "fname": fname,
        "lname": lname,
        "username": username,
        "password": password,
        "email": email,
        "avatar": avatar
    };


    const callGetApi = async (api, apiName, apiMethod, reqBody) => {
        fetch(`${api}`, {
            method: `${apiMethod}`,
            headers: {
                'Content-Type': 'application/json'
            },
            ...(apiMethod == 'POST' || apiMethod == 'PUT' || apiMethod == 'DELETE' ? { body: JSON.stringify(reqBody) } : {})
        }).then(async (res) => {
            try {
                const data = await res.json();
                console.log(res)
                if (res.ok) {
                    if (data.message) {
                        console.info(`เรียก api สำเร็จ แต่ ${data.message}`);
                        alert(data.message);
                    } else {
                        console.info(`เรียก api ${apiName} สำเร็จ ✅`);
                        alert(`เรียก api ${apiName} สำเร็จ`);
                    }
                    console.log('รายละเอียด :', data);
                    return data;
                } else {
                    alert(`เรียก api ${apiName} ไม่สำเร็จ ${data.message}`);
                    console.error(`เรียก api ${apiName} ไม่สำเร็จ ❌`)
                    if (data.message) console.info(data.message);
                    console.error('Status : ', res.statusText);
                }
            } catch (error) {
                alert('มีข้อผิดพลาดเกิดขึ้น ! 🥲');
                console.error('Error : ', error);
                console.info(res)
            }
        });
    }

    const userListApi = [
        {
            apiName: 'USER LIST: api/users',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_USERLIST_APIUSERS, 'USER LIST', 'GET'),
        },
        {
            apiName: 'USER LIST (SEARCH): api/users?search={search}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_USERLIST_SEARCH, 'USER LIST (SEARCH)', 'GET'),
        },
        {
            apiName: 'USER LIST (PAGINATION): api/users?page={page}&per_page={per_page}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_USERLIST_PAGINATION, 'USER LIST (PAGINATION)', 'GET'),
        },
        {
            apiName: 'USER LIST (SORT): api/users?sort_column={sort_column}&sort_order={sort_order}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_USERLIST_SORTORDER, 'USER LIST (SORT)', 'GET'),
        },
        {
            apiName: 'USER LIST (SEARCH + PAGINATION + SORT): api/users?search={search}&page={page}&per_page={per_page}&sort_column={sort_column}&sort_order={sort_order}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_USERLIST_SEARCH_PAGI_SORT, 'USER LIST (SEARCH + PAGINATION + SORT)', 'GET'),
        },
        {
            apiName: 'USER DETAIL: api/users/{id}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_USERLIST_USERSID, 'USER DETAIL: api/users/{id}', 'GET',),
        },
        {
            apiName: 'USER CREATE',
            config: {
                url: process.env.NEXT_PUBLIC_USERLIST_CREATE,
                apiName: 'USER CREATE: api/users/create',
                method: 'POST',
            },
            crud: 'createUser'
        },
        {
            apiName: 'USER UPDATE AND DELETE',
            crud: 'update_delete_user'
        },
        // {
        //     apiName: 'USER DELETE: api/users/delete',
        //     callApi: () => callGetApi(process.env.NEXT_PUBLIC_USERLIST_DELETE, 'USER DELETE: api/users/delete', 'DELETE', userDelete),
        //     crud: 'deleteUser'
        // },
    ];

    const configUpdate = {
        url: process.env.NEXT_PUBLIC_USERLIST_UPDATE,
        apiName: 'USER UPDATE: api/users/update',
        method: 'PUT',
    }
    const configDelete = {
        url: process.env.NEXT_PUBLIC_USERLIST_DELETE,
        apiName: 'USER DELETE: api/users/delete',
        method: 'DELETE',
    }

    const attractionList = [
        { 
            apiName: 'ATTRACTION LIST: api/attractions',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_ATTRACTIONSLIST_APIATTRACTIONS, 'ATTRACTION LIST', 'GET'),
        },
        {
            apiName: 'ATTRACTION LIST (SEARCH): api/attractions?search={search}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_ATTRACTIONSLIST_SEARCH, 'ATTRACTION LIST (SEARCH)', 'GET'),
        },
        {
            apiName: 'ATTRACTION LIST (PAGINATION): api/attractions?page={page}&per_page={per_page}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_ATTRACTIONSLIST_PAGINATION, 'ATTRACTION LIST (PAGINATION)', 'GET'),
        },
        {
            apiName: 'ATTRACTION LIST (SORT): api/attractions?sort_column={sort_column}&sort_order={sort_order}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_ATTRACTIONSLIST_SORTORDER, 'ATTRACTION LIST (SORT)', 'GET'),
        },
        {
            apiName: 'ATTRACTION LIST (SEARCH + PAGINATION + SORT): api/attractions?search={search}&page={page}&per_page={per_page}&sort_column={sort_column}&sort_order={sort_order}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_ATTRACTIONSLIST_SEARCH_PAGI_SORT, 'ATTRACTION LIST (SEARCH + PAGINATION + SORT)', 'GET'),
        },
        {
            apiName: 'ATTRACTION LIST (LANGUAGE): api/{language}/attractions',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_ATTRACTIONSLIST_LANGUAGE, 'ATTRACTION LIST (LANGUAGE): api/{language}/attractions', 'GET'),
        },
        {
            apiName: 'ATTRACTION DETAIL: api/attractions/{id}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_ATTRACTIONSLIST_DETAIL, 'ATTRACTION DETAIL: api/attractions/{id}', 'GET'),
        },
        {
            apiName: 'ATTRACTION DETAIL: api/{language}/attractions/{id}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_ATTRACTIONSLIST_DETAILLANG, 'ATTRACTION DETAIL: api/{language}/attractions/{id}', 'GET'),
        },
        {
            apiName: 'ATTRACTION STATIC PATHS: api/attractions/static_paths',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_ATTRACTIONSLIST_STATICPATH, 'ATTRACTION STATIC PATHS: api/attractions/static_paths', 'GET'),
        },
    ]

    const petSaleList = [
        {
            apiName: 'PET SALES SUMMARY (7 DAYS): api/pets/7days/{date}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_PETSALES_SUM7DAYS, 'PET SALES SUMMARY (7 DAYS)', 'GET'),
        },
        {
            apiName: 'PET SALES (DAILY): /api/pets/{date}',
            callApi: () => callGetApi(process.env.NEXT_PUBLIC_PETSALES_DAILY, 'PET SALES (DAILY)', 'GET'),
        },
    ]
    return (
        <>
            <Head>
                <title>Workshop 5 API</title>
            </Head>
            <BackButton />
            <Box bgcolor={'#f7f7f7'} height={'100vh'}>
                <Container maxWidth='xl' sx={{ margin: '4em auto' }}>
                    <Grid container spacing={5} columns={{ xs: 4, sm: 8.5, md: 12 }} >
                        <Grid item xs={4}>
                            <Card sx={{ height: '80vh', overflow: 'scroll' }}>
                                <CardHeader title='USERS' />
                                <CardContent>
                                    <Box>
                                        {userListApi.map((item, index) => (
                                            <Button key={index} onClick={!item.crud ? item.callApi : () => handleOpen(item)}
                                                style={{
                                                    backgroundColor: '#eee',
                                                    color: '#001',
                                                    width: '100%',
                                                    margin: '5px 0'
                                                }}>
                                                {item.apiName}
                                                {item.crud ? <KeyboardArrowRightRoundedIcon
                                                    sx={{ bgcolor: '#d4d4d4', marginLeft: '10px', borderRadius: '100%' }} /> : ''}
                                            </Button>
                                        ))}
                                    </Box>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={4}>
                            <Card sx={{ height: '80vh', overflow: 'scroll' }}>
                                <CardHeader title='ATTRACTIONS' />
                                <CardContent sx={{ overflow: 'scroll' }}>
                                    <Box>
                                        {attractionList.map((item, index) => (
                                            <Button key={index} onClick={!item.crud ? item.callApi : () => handleOpen(item.crud)} style={{
                                                backgroundColor: '#eee',
                                                color: '#001',
                                                width: '100%',
                                                margin: '5px 0'
                                            }}>
                                                {item.apiName}
                                                {item.crud ? <KeyboardArrowRightRoundedIcon
                                                    sx={{ bgcolor: '#d4d4d4', marginLeft: '10px', borderRadius: '100%' }} /> : ''}
                                            </Button>
                                        ))}
                                    </Box>
                                </CardContent>
                            </Card>
                        </Grid>
                        <Grid item xs={4} >
                            <Card sx={{ height: '80vh', overflow: 'scroll' }}>
                                <CardHeader title='PET SALES' />
                                <CardContent sx={{ overflow: 'scroll' }}>
                                    <Box>
                                        {petSaleList.map((item, index) => (
                                            <Button key={index} onClick={item.callApi} style={{
                                                backgroundColor: '#eee',
                                                color: '#001',
                                                width: '100%',
                                                margin: '5px 0'
                                            }}>
                                                {item.apiName}
                                            </Button>
                                        ))}
                                    </Box>
                                </CardContent>
                            </Card>
                        </Grid>
                    </Grid>
                </Container>
            </Box>

            <Modal
                open={open}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Container maxWidth="md">
                    <Card sx={{ marginTop: '5em' }}>
                        <CardContent>
                            {form == 'createUser' ?
                                <>
                                    <Typography gutterBottom variant="h4">
                                        Create User
                                    </Typography>
                                    <UserForm
                                        form={form}
                                        setUsername={setUsername}
                                        setFname={setFname}
                                        setLname={setLname}
                                        setPassword={setPassword}
                                        setEmail={setEmail}
                                        username={username}
                                        fname={fname}
                                        lname={lname}
                                        password={password}
                                        email={email}
                                        getApi={() => { callGetApi(api.url, api.apiName, api.method, userField); handleClose(); }}
                                    />
                                </>
                                :
                                <></>
                            }

                            {form == 'update_delete_user' ?
                                <UserList users={users} openEdit={handleOpenEdit} openDelete={handleOpenDelete} setUsers={setUsers} />
                                :
                                <></>
                            }
                        </CardContent>
                    </Card>
                </Container>
            </Modal>

            <Modal
                open={openEdit}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Container maxWidth="md">
                    <Card sx={{ marginTop: '5em' }}>
                        <Box display={'flex'} margin={'3em'}>
                            <Button variant="contained" onClick={handleOpen}>กลับ</Button>
                            <Typography variant="h5" sx={{alignItems: 'center', marginLeft:'10px'}}>Update User {username}</Typography>
                        </Box>
                        <CardContent>
                            <UserForm setUsername={setUsername}
                                setFname={setFname}
                                setLname={setLname}
                                setPassword={setPassword}
                                setEmail={setEmail}
                                username={username}
                                fname={fname}
                                lname={lname}
                                password={password}
                                email={email}
                                getApi={() => { callGetApi(configUpdate.url, configUpdate.apiName, configUpdate.method, userField); handleClose(); }}
                            />
                        </CardContent>
                    </Card>
                </Container>
            </Modal>
            <Modal
                open={openDelete}
                onClose={handleClose}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Container maxWidth="sm">
                    <Card sx={{ marginTop: '5em' }}>
                        <CardHeader title={`Delete User ${username}`} />
                        <CardContent>
                            Delete user {username} ????
                        </CardContent>
                        <CardActions sx={{justifyContent: 'end'}}>
                            <Button variant="contained" onClick={handleOpen}>NoNoNo</Button>
                            <Button variant="contained" onClick={() => { callGetApi(configDelete.url, configDelete.apiName, configDelete.method, userField); handleClose() }}>Delete</Button>
                        </CardActions>
                    </Card>
                </Container>
            </Modal>
        </>
    )
}

export default workshopFive;