
import Head from "next/head";
import { Layout } from "src/layouts/dashboard/layout";
import styles from "src/styles/waiwitzStyles.module.css";
import { Typography, Button, Link, Breadcrumbs } from "@mui/material";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import { useState } from "react";
const calculator = () => {
    const [input, setInput] = useState('');

    const operators = ['/', '*', '-', '+'];
    const numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", '.'];
    const errMsg = 'โปรดใส่เลขให้ถูกต้อง'
    const undefindMsg = 'ไม่พบค่า'
    const handleClick = (value) => {
        try {
            const lastchar = input.slice(-1)
            if (operators.includes(lastchar) && operators.includes(value.target.textContent)) {
                setInput(input.slice(0, -1).concat(value.target.textContent));
            } else if (input === errMsg || input === undefindMsg) {
                setInput(input.replace(undefindMsg,value.target.textContent));
            } else {
                setInput(input.concat(value.target.textContent));
            }
        } catch (error) {
            setInput(errMsg)
        }
        console.log(input);
    }

    // เคลียร์ช่อง
    const clear = () => {
        setInput('');
    }

    // ปุ่ม = คำนวณ
    const startCalculate = () => {
        try {
            setInput(eval(input).toString());
        } catch (error) {
            setInput('');
        }
    }

    //  ปุ่มหาเปอร์เซ็น %
    const percent = () => {
        try {
            if (!eval(input / 100)) {
                setInput(undefindMsg);
            } else {
                setInput(eval(input / 100).toString());
            }

            console.log(input);
            // setInput(eval(input / 100).toString());
            // if (input == undefined || input == NaN || input == null) setInput('ไม่พบค่า');
        } catch (error) {
            setInput('');
        }
    }

    const invertInt = () => {
        // console.log('ตัวแรก ' + input.slice(0, 1));
        if (input.slice(0, 1) == '-') {
            setInput(input.substring(1));
        } else {
            setInput('-' + input);
        }
    }

    return (
        <>
            <Head>
                <title>Workshop || เครื่องคิดเลข</title>
            </Head>
            <div>
                <div style={{ margin: "1em 2em", display: 'flex' }}>
                    <Link href='/workShop/waiwitz' style={{ marginRight: '10px' }}>
                        <Button>
                            <ArrowBackIosIcon></ArrowBackIosIcon>
                        </Button>
                    </Link>
                    <div>
                        <Typography variant="h4">Workshop 2 || เครื่องคิดเลข 🧮</Typography>
                        <Breadcrumbs maxItems={3} aria-label="breadcrumb" style={{ marginTop: '10px' }}>
                            <Link underline="hover" color="inherit" href="/workShop">
                                WorkShop
                            </Link>
                            <Link underline="hover" color="inherit" href="/workShop/waiwitz">
                                การ์ด
                            </Link>
                            <Typography color="text.primary">Workshop 2 </Typography>
                        </Breadcrumbs>
                    </div>
                </div>
            </div>

            <div className={styles.calculator}>
                {/* จอแสดงผล */}
                <div className={styles.calculatorInput}>
                    <input type="text" value={input} />
                </div>

                <div className={styles.rowPad}>
                    <div className={styles.numberRow}> {/* ปุ่มฝั่งซ้าย */}
                        <div className={styles.topRow}>
                            <Button onClick={clear}>c</Button>
                            <Button onClick={invertInt}>+/-</Button>
                            <Button onClick={percent}>%</Button>
                        </div>
                        <div className={styles.keyPad}>
                            {numbers.map((number) => (
                                <Button key={number} onClick={handleClick}>{number}</Button>
                            ))}
                        </div>
                    </div>

                    <div className={styles.operatorsRow}> {/* ปุ่มฝั่งขวา */}
                        {operators.map((symbol) => (
                            <Button key={symbol} onClick={handleClick}>{symbol}</Button>
                        ))}
                        <Button onClick={startCalculate}>=</Button>
                    </div>
                </div>
            </div>
        </>
    )
}
calculator.getLayout = (calculator) => <Layout>{calculator}</Layout>;

export default calculator;