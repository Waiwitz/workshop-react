import { Box, Button, Typography } from "@mui/material";
import Navbar from "/src/components/waiwitz/workshop2/navBar";
import Footer from "src/components/waiwitz/workshop2/footer";
import BackButton from "src/components/waiwitz/backButton";

const error = () => {
    return (
        <>
        <BackButton/>
            <Navbar />
            <Box bgcolor={'#989494'}>
                <Box
                    padding={'20em 0 0em 0'}
                    textAlign={'center'}
                    color={'white'}>
                    <Typography variant="h1" fontWeight={1000} gutterBottom>ERROR 404</Typography>
                    <Typography variant="body1" fontWeight={'bold'} gutterBottom>PAGE NOT FOUND, PLEASE GO BACK</Typography>
                    <Button sx={{
                        borderRadius: '0',
                        marginTop: '1em', backgroundColor: '#5f5f5f',
                        width: 'fit-content', color: '#FFFFFF', padding: '1.5em 4em'
                    }}>
                        ORDER NOW
                    </Button>
                </Box>
                <Footer />
            </Box>
        </>
    )
};

export default error;