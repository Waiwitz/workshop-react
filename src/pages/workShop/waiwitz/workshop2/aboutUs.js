import { Box, Button, Container, Grid, Paper, Typography } from "@mui/material";
import Navbar from "/src/components/waiwitz/workshop2/navBar";
import Banner from "src/components/waiwitz/workshop2/banner";
import Introduce from "src/components/waiwitz/workshop2/introduce1";
import styles from "src/styles/waiwitzStyles.module.css";
import Team from "src/components/waiwitz/workshop2/teamCard";
import CallIcon from '@mui/icons-material/Call';
import Footer from "src/components/waiwitz/workshop2/footer";
import Introduce1 from "src/components/waiwitz/workshop2/introduce1";
import Introduce3 from "src/components/waiwitz/workshop2/introduce3";
import Introduce2 from "src/components/waiwitz/workshop2/introduce2";
import BackButton from "src/components/waiwitz/backButton";
const teamList = [
    {
        name: 'John',
        position: 'Founder'
    },
    {
        name: 'Alice',
        position: 'Founder'
    },
    {
        name: 'James',
        position: 'Founder'
    },
    {
        name: 'Betty',
        position: 'Founder'
    },
    {
        name: 'August',
        position: 'Founder'
    },
    {
        name: 'Inez',
        position: 'Founder'
    },
]

const aboutUs = () => {
    return (
        <>
        <BackButton/>
            <Navbar />
            <Banner title={'ABOUT US'} />
            <Container maxWidth='xl' sx={{ marginTop: '5em' }}>
                <Introduce1 head1={'WHO WE ARE'} head2={'WE ARE PERFECT TEAM FOR HOME INTERIOR DECORATION'} body={'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo'} />
                <Box sx={{ marginTop: '5em' }}>
                    <Typography gutterBottom>OUR TEAM</Typography>
                    <Typography variant="h3" sx={{ fontWeight: 'bold', marginBottom: '1.5em' }} >WE WORK WITH TEAM</Typography>
                    <Grid container columns={{ xs: 4, sm: 8, md: 12 }} spacing={4}>
                        {teamList.map((team, index) => (
                            <Grid item xs={4} key={team.name}>
                                <Team name={team.name} position={team.position} index={index} />
                            </Grid>
                        ))}
                    </Grid>
                </Box>
            </Container>
            <Box bgcolor={'#757575'} height={440} margin={'8em 0'}>
                <Container maxWidth='xl' sx={{ marginTop: '5em' }}>
                    <Grid container justifyContent={'space-between'} columns={{ xs: 4, sm: 8, md: 12 }} >
                        <Grid xs={4}>
                            <Box bgcolor={'#C4C4C4'} height={300} sx={{ transform: 'translateY(-170px)' }} />
                            <Box bgcolor={'#fff'} height={200} padding={5} sx={{ transform: { xs: 'translateY(-150px) translateX(0px)', md: 'translateY(-265px) translateX(280px)' } }}>
                                <CallIcon sx={{ marginBottom: '0.5em', fontSize: '50px', color: '#757575' }} />
                                <Typography variant="h5" gutterBottom>CALL US NOW</Typography>
                                <Typography>( +62 ) 123 456 789</Typography>
                            </Box>
                        </Grid>
                        <Grid xs={5} sx={{ display: { xs: 'none', md: 'inherit' } }}>
                            <Typography variant="h1" color={'white'}>"WORK HARD & GREAT QUALITY IS MY PRIORITY"</Typography>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
            <Container maxWidth='xl'>
                <Introduce2 head1={'PERFECT PARTNER'} head2={'WE HAVE PRIORITY FOR CAN CREATE DREAM HOME DESIGN'} body={`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.`} />
                <Introduce3 head1={'TRUST US NOW'} head2={'WHY CHOICE OUR HOME DESIGN INTERIOR SERVICES'} body={`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.`} />
            </Container>
            <Footer />
        </>
    )
};

export default aboutUs;