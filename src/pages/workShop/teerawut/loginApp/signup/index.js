import { Alert, Snackbar } from "@mui/material";
import { useEffect, useState } from "react";
import { LayoutLogin } from "src/components/freshy/loginApp/loginPage2";
import SignupPage from "src/components/freshy/loginApp/signupPage";
import { useRouter } from "next/router";


const baseUrl = '/workShop/teerawut/loginApp';
export default function index() {
    const router = useRouter();
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState({});

    const handleCloseSnackbar = () => {
        setOpenSnackbar(false);
    };

    useEffect(() => {
        setTimeout(() => {
            setOpenSnackbar(false);
        }, 5000);
        if (snackbarMessage.status === 'success') {
            setTimeout(() => router.push(baseUrl), 3000);
        }
    }, [openSnackbar, snackbarMessage])
    return (
        <>
            <LayoutLogin>
                <SignupPage setOpenSnackbar={setOpenSnackbar} setSnackbarMessage={setSnackbarMessage} />
            </LayoutLogin>
            <Snackbar
                open={openSnackbar}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar}
            >
                <Alert
                    onClose={handleCloseSnackbar}
                    severity={snackbarMessage.status === 'success' ? 'success' : 'warning'}
                >
                    {snackbarMessage.title}
                </Alert>
            </Snackbar>
        </>
    );
}
