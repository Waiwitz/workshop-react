// index.js
import { Alert, Snackbar } from '@mui/material';
import React, { useState } from 'react';
import LoginPage from 'src/components/freshy/loginApp/loginPage';
import LoginPage2, { LayoutLogin } from 'src/components/freshy/loginApp/loginPage2';
import MainSection from 'src/components/freshy/loginApp/mainSection';

const Index = () => {
    const [user, setUser] = useState(null);
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [snackbarMessage, setSnackbarMessage] = useState({});
    const [loading, setLoading] = useState(false);

    const handleUser = (userData) => {
        setUser(userData);
        console.log('setUserData', userData);
    };

    const handleLogout = () => {
        setLoading(true);
        setTimeout(() => {
            try {
                setUser(null);
                setSnackbarMessage({ status: 'success', title: 'Logout successful' });
                localStorage.clear();
                setOpenSnackbar(true);
                setLoading(false);

                console.info("[INFO] Logout Successful");
            } catch (error) {
                console.error("[ERROR] Error during logout: " + error)
            }
        }, 750)
    };

    const handleCloseSnackbar = () => {
        setOpenSnackbar(false);
    };

    return (
        <>
            {user ? (
                <MainSection
                    user={user}
                    handleLogout={handleLogout}
                    loading={loading}
                    setOpenSnackbar={setOpenSnackbar}
                    setSnackbarMessage={setSnackbarMessage}
                />
            ) : (
                <LayoutLogin>
                    <LoginPage2
                        handleUser={handleUser}
                        setOpenSnackbar={setOpenSnackbar}
                        setSnackbarMessage={setSnackbarMessage}
                        snackbarMessage={snackbarMessage} />
                </LayoutLogin>
            )
            }
            <Snackbar
                open={openSnackbar}
                autoHideDuration={5000}
                onClose={handleCloseSnackbar}
            >
                <Alert
                    onClose={handleCloseSnackbar}
                    severity={snackbarMessage.status === 'success' ? 'success' : 'warning'}
                >
                    {snackbarMessage.title}
                </Alert>
            </Snackbar>
        </>
    );
};

export default Index;
