import { Layout } from "src/layouts/dashboard/layout";
import { Inter } from 'next/font/google'
import { Container } from "@mui/material";
import Introduction from "src/components/freshy/introduction";
import Workshop from "src/components/freshy/workshop";
import Playground from "src/components/freshy/playground";


const inter = Inter({ subsets: ['latin'] })

const teerawutProfile = {
    nickName: "Freshy",
    fullName: "Teerawut Sungkagaro",
    avatar: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/avatar-freshy.png?alt=media&token=55cc979e-a0db-4017-816f-a4ae58010725',
    biography: `Bachelor of Information Technology, College of Computing - Khon Kaen University, 
    Thailand. Interesting to know more about programming and explore experiences.`,
    contacts: {
        github: "https://github.com/trwfs00",
        linkedin: "https://www.linkedin.com/in/trwfs00/",
        dribbble: "https://dribbble.com/trwfs0"
    }
}

// Array of workshop data with title and subtitle
const workshops = [
    {
        tabName: 'Pre Workshop',
        title: 'Resume Website',
        subtitle: 'Design your own resume website with beautiful and comment code at least 2 pages.',
        date: '2023-11-15',
        dueDate: '2023-11-15',
        reposLink: 'https://github.com/trwfs00/resume-website',
        reposHTTPS: 'https://github.com/trwfs00/resume-website.git',
        thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-16%20131231.png?alt=media&token=b652a651-e1fe-42d5-b6cd-f507d9a70530'
    },
    {
        tabName: 'Workshop 0',
        title: 'Workshop Collection',
        subtitle: 'Fork base repos then design your workshop collection page and make a merge request.',
        date: '2023-11-16',
        dueDate: '2023-11-16',
        reposLink: 'https://gitlab.com/trwfs00/workshop-react',
        reposHTTPS: 'https://gitlab.com/trwfs00/workshop-react.git',
        thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-16%20112229.png?alt=media&token=af339714-90bf-420d-b35c-11816717031a'
    },
    {
        tabName: 'Workshop 1',
        title: 'Calculator',
        subtitle: "Create a UI page for a calculator. Divide the Workshop's data collection as well.",
        date: '2023-11-17',
        dueDate: '2023-11-17',
        reposLink: 'https://gitlab.com/trwfs00/workshop-react',
        reposHTTPS: 'https://gitlab.com/trwfs00/workshop-react.git',
        thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-20%20084828.png?alt=media&token=51f5b92d-5d9c-467e-b069-6929714376bd',
        demoHref: '/calculatorApp'
    },
    {
        tabName: 'Workshop 2',
        title: 'Design UI',
        subtitle: 'Design a beautiful UI from figma wireframe, Divide the components and make a wonderful decoration.',
        date: '2023-11-20',
        dueDate: '2023-11-20',
        reposLink: 'https://gitlab.com/trwfs00/workshop-react',
        reposHTTPS: 'https://gitlab.com/trwfs00/workshop-react.git',
        thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-21%20232424.png?alt=media&token=1fee7162-ef2e-40a8-ac0d-3830970be84c',
        demoHref: '/designUI'
    },
    {
        tabName: 'Workshop 3 | 4 | 5',
        title: 'Login Page',
        subtitle: 'Make a Login Page using Melivecode API, Logging and collect JWT to localstorage.',
        date: '2023-11-21',
        dueDate: 'undefined',
        reposLink: 'https://gitlab.com/trwfs00/workshop-react',
        reposHTTPS: 'https://gitlab.com/trwfs00/workshop-react.git',
        thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-21%20233101.png?alt=media&token=1bcdcbf9-331a-4da1-9ed2-33699561764c',
        demoHref: '/loginApp'
    },
    {
        tabName: 'Workshop 6',
        title: 'ระบบสั่งสินค้ามีรายการสินค้าให้เลือกเก็บลงตะกร้า',
        subtitle: 'ระบบสั่งสินค้ามีรายการสินค้าให้เลือกเก็บลงตะกร้ากดยืนยันแล้วแสดง popupรายการสินค้าที่เลือกบอกจำนวนเงิน และจำนวนสินค้า listเป็นรายงาน',
        date: '2023-11-27',
        dueDate: '2023-11-28',
        reposLink: 'https://gitlab.com/trwfs00/workshop-react',
        reposHTTPS: 'https://gitlab.com/trwfs00/workshop-react.git',
        thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-21%20233101.png?alt=media&token=1bcdcbf9-331a-4da1-9ed2-33699561764c',
        demoHref: '/samsung'
    }
];

 // Array of workshop data with title and subtitle
 const playgrounds = [
    {
        tabName: "Playground 1",
        title: 'Weather Web Application',
        subtitle: 'Web Application check weather, temperature and air quality.',
        date: '2023-11-17',
        reposLink: 'https://gitlab.com/trwfs00/workshop-react',
        reposHTTPS: 'https://gitlab.com/trwfs00/workshop-react.git',
        thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-17%20144041.png?alt=media&token=dfb4352b-bd14-4b47-aa29-39aee55a04d9',
        demoHref: '/weatherApp'
    },
    {
        tabName: "Playground 2",
        title: 'Melivecode JWT Authentication',
        subtitle: 'Playing with Melivecode Authentication API',
        date: '2023-11-22',
        reposLink: 'https://gitlab.com/trwfs00/workshop-react',
        reposHTTPS: 'https://gitlab.com/trwfs00/workshop-react.git',
        thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-17%20144041.png?alt=media&token=dfb4352b-bd14-4b47-aa29-39aee55a04d9',
        demoHref: '/loginApp/playground'
    },
];

const teerawut = () => {
    return (
        <>
            <main className={`flex min-h-screen flex-col items-center justify-between p-12 md:p-24 ${inter.className}`}>
                <Container maxWidth="xl">
                    <Introduction profile={teerawutProfile}/>
                    <Workshop workshops={workshops} />
                    <Playground playgrounds={playgrounds} />
                </Container>
            </main>
        </>
    )
}
teerawut.getLayout = (teerawut) => <Layout>{teerawut}</Layout>;
export default teerawut;