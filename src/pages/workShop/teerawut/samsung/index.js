import Head from 'next/head';
import React from 'react'
import SamsungAppBar from 'src/components/freshy/samsung/Layouts/navbar';
import { ProductGrid } from 'src/components/freshy/samsung/Layouts/productGrid';
import { SamsungHero } from 'src/components/freshy/samsung/hero';


const Samsung = () => {
    return (
        <>
            <Head>
                <title>Samsung Thailand | มือถือ สมาร์ทโฟน ทีวี และเครื่องใช้ไฟฟ้าต่างๆ</title>
            </Head>
            <SamsungAppBar />
            <SamsungHero/>
            <ProductGrid/>
        </>
    )
}

export default Samsung;