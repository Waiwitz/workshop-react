import { Layout } from "src/layouts/dashboard/layout";
import * as React from 'react';
import { Inter } from 'next/font/google'
import aerioLogo from 'public/assets/freshy/aerioLogo.png'
import WeatherSense from "src/components/freshy/weatherApp/weatherSense";
import WeatherForm from "src/components/freshy/weatherApp/weatherForm";
import { Box } from "@mui/system";
import { Button, Typography } from "@mui/material";
import Image from "next/image";
import { useState } from "react";
const inter = Inter({ subsets: ['latin'] })

const WeatherApp = (props) => {
    const [location, setLocation] = useState(null);
    const handleLocation = (country, state, city, isSearch) => {
        setLocation({
            country: country,
            state: state,
            city: city,
            isSearch: isSearch
        });
    }
    return (
        <main className={`flex min-h-screen flex-col items-center justify-between p-12 md:p-24 ${inter.className}`}>
            <Box className='flex flex-col md:flex-row justify-center items-center gap-6'>
                <Image src={aerioLogo} className='h-24 w-auto' alt='Logo' />
                <Box className='text-center md:text-left md:w-1/2'>
                    <Typography variant="h4" sx={{ mb: 1 }}>
                        WeatherSense
                    </Typography>
                    <Typography variant="body2">
                        Introducing WeatherSense, a cutting-edge web application designed to elevate your weather-checking experience.
                    </Typography>
                </Box>
            </Box>
            <WeatherSense locationSetting={location}/>
            <WeatherForm handleLocation={handleLocation} />
        </main>
    )
}
WeatherApp.getLayout = (WeatherApp) => <Layout>{WeatherApp}</Layout>;
export default WeatherApp;