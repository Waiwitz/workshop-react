import { Layout } from "src/layouts/dashboard/layout";
import * as React from "react";
import Content from "src/components/Tong/Content";
import MyLayout from "./MyLayout";
const tong = () => {
  return (
    <>
      <MyLayout>
        <Content />
      </MyLayout>
    </>
  );
};

tong.getLayout = (tong) => <Layout>{tong}</Layout>;
export default tong;