import * as React from "react";
import Users from "src/components/Tong/Work5/Users";
import Attractions from "src/components/Tong/Work5/Attractions";
import Pet from "src/components/Tong/Work5/Pet";
import MyLayout2 from "./MyLayout2";
const ApiCall = ({ children }) => {
  return (
    <>
      <MyLayout2>
        <br/>
        <Users/>
        <br/>
        <Attractions/>
        <br/>
        <Pet/>
      </MyLayout2>
    </>
  );
};

// tong.getLayout = (tong) => <Layout>{tong}</Layout>;
export default ApiCall;
