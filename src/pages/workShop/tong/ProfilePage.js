import * as React from "react";
import Profile from "src/components/Tong/Work4/Profile";
import MyLayout2 from "./MyLayout2";
const ProfilePage = ({ children }) => {
  return (
    <>
      <MyLayout2>
        <br/>
        <Profile/>
      </MyLayout2>
    </>
  );
};

// tong.getLayout = (tong) => <Layout>{tong}</Layout>;
export default ProfilePage;
