import React from "react";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Navui from "src/components/miw/Navui";
import Footui from "src/components/miw/Footui";
import Bodyui0 from "src/components/miw/Bodyui0";
import Bodyui1 from "src/components/miw/Bodyui1";
import Bodyui2 from "src/components/miw/Bodyui2";

const ui = () => {
  return (
    <>
      <Navui />
      <div style={{ backgroundColor: "#f2eddd", padding: "100px" }}>
        <Typography
          variant="h1"
          sx={{
            color: "#455a64",
            margin: "10px",
            display: "block",
            textAlign: "left",
            fontWeight: "bold",
            fontSize: "80px",
          }}
          className="text-9xl "
        >
          BLOG
          <Typography variant="body1">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, lutus
            <br />
            nec ullamcorper mattis, pulvinar dapibus leo.
          </Typography>
        </Typography>
      </div>
      <div className="pb-40">
        <Bodyui0 />
      </div>
      <Box
        sx={{
          position: "relative",
          width: "100%",
          height: "200px",
          backgroundColor: "#f2eddd",
        }}
      >
        <Box
          sx={{
            marginX: "5rem",
            marginBottom: "5rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,-5em)",
            zIndex: 2,
          }}
        >
          <Bodyui1 />
        </Box>

        <Box
          sx={{
            marginX: "12rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,-8em)",
            zIndex: 30,
          }}
        >
          {" "}
          <Bodyui2 />
        </Box>
      </Box>

      <Footui />
    </>
  );
};

export default ui;
