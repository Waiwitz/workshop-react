import Totalapi from "src/components/pang/workshop5/totalapi";
import Create from "src/components/pang/workshop5/Create";
import Update from "src/components/pang/workshop5/Update";
import Delete from "src/components/pang/workshop5/Delete";
import Totalattractions from "src/components/pang/workshop5/attractions/totalattractions";
import Petsale from "src/components/pang/workshop5/petsale/petsale";


const Api = () => {
  const handleCreate = () => {};

  const handleUpdate = () => {};

  const handleDelete = () => {};


  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        mx:"100px",
        my:"auto",
      }}
    >
      <Totalapi />
      <Totalattractions />
      <Petsale />
      <div
        style={{
          display: "flex",
          gap: "16px",
          marginBottom: "20px",
          marginTop: "20px",
        }}
      >
        <Create onCreate={handleCreate} />
        <Update onUpdate={handleUpdate} />
        <Delete onDelete={handleDelete} />
      </div>

    </div>
  );
};

export default Api;
