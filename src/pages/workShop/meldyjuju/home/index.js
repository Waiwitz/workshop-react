// index.js
import Head from "next/head";
import Login from "src/components/pang/Login";
import UserInfo from "src/components/pang/Userinfo";
import { useRouter } from "next/router";
import React, { useState } from "react";
import Button from "@mui/material/Button";


export default function Home() {
  const [jwt, setJwt] = useState(localStorage.getItem("jwt") || "");
  const router = useRouter();

  const handleLogin = async (jwt) => {
    setJwt(jwt);
  };


  const handleLogout = () => {
    localStorage.removeItem("jwt");
    localStorage.removeItem("userData");
    setJwt("");
  };

  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        justifyContent: "center",
        height: "100vh",
        color: "#000000",
      }}
    >
      <Head>
        <title>Login Workshop</title>
        <meta name="description" content="Login Workshop" />
      </Head>

      <main>
        {jwt ? (
          
          <div>
            <UserInfo jwt={jwt} />
            <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
            <Button
              variant="contained"
              color="primary"
              onClick={handleLogout}
              
              style={{
                backgroundColor: "#26a69a", 
                borderRadius: "8px", 
                
              }}
            >
              Logout
            </Button>
            </div>
          </div>
        ) : (
          <Login onLogin={handleLogin} />
        )}
      </main>
    </div>
  );
}
