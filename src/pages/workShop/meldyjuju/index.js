import { Layout} from "src/layouts/dashboard/layout";
import Navbar from "src/components/pang/navbar";
import Mycard from "src/components/pang/mycard";
import Jujustore from "src/components/pang/jujustore";
import Workshop1 from "src/components/pang/workshop1";
import Link from "next/link";

const meldyjuju = () => {
    return(
        <div>
        <Navbar />
        <Mycard />
        <Jujustore/>
        <div style={{ display: "flex", justifyContent: "center", gap: "20px", padding: "20px" }}>

        <div
        style={{
          width: "475px",
          height: "75px",
          fontWeight: "bold",
          backgroundColor: "#F2E9D3",
          padding: "20px",
          borderRadius: "8px",
          boxSizing: "border-box",
          position: "relative",
          textAlign: "center",
          
        }}
      >
        <div>
        DOCUMENTATION (src/pages/workShop/meldyjuju/README/readme.md)..

        </div>
        <p style={{ color: "#000000" }}></p>
      </div>  
        </div>
        <Workshop1/>
        </div>

    )
}
meldyjuju.getLayout = (meldyjuju) => <Layout>{meldyjuju}</Layout>;
export default meldyjuju;