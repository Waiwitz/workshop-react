import React, { useState } from "react";
import Testproduct from "src/components/pang/workshop6/testproduct";
import Navbar from "src/components/pang/workshop6/navbar";
import Productbody from 'src/components/pang/workshop6/productbody'; 

const Home = () => {
  const [cartItems, setCartItems] = useState([]); 
  return (
    <div>
      <Navbar cartItems={cartItems} />
      <Productbody />
      <div style={{ margin: "60px" }}>
      <Testproduct setCartItems={setCartItems} />
      </div>
    </div>
  );
};

export default Home;
