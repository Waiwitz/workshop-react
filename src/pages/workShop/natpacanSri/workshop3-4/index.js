import React, { useState, useEffect } from 'react'
import { redirect } from 'next/navigation'
import Router from 'next/router'
import {
  Button,
  TextField,
  FormControlLabel,
  Checkbox,
  Link,
  Grid,
  Box,
  Typography,
  Container
} from "@mui/material"
import { Email, Message } from '@mui/icons-material'

export default function index() {

  const [email, setEmail] = useState()
  const [password, setPassword] = useState()
  const [userData, setUserData] = useState(null);
  const [message,setMessage] = useState()

  const handleBlur = (field) => {
    console.log(`${field} input:`, field === 'email' ? email : "********");
  };

  const handleSubmit = async (event) => {
    event.preventDefault();

    if (!Email) {
      console.warn("email didn't input")
      setMessage('email and password are requirement')
    } else if (!password) {
      console.warn("password didn't input")
      setMessage('email and password are requirement')
    } else {
      try {

        const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}${process.env.NEXT_PUBLIC_LOGIN_ENDPOINT}`, {
          // const response = await fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            username: email,
            password: password,
          }),
        });

        console.log("input Data:",JSON.stringify({
          username: email,
          password: "********",
        }))

        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }

        const responseData = await response.json();

        // Log information
        console.info('Login success:', responseData);

        // Save JWT to local storage
        // localStorage.setItem('jwt', responseData.jwt);
        localStorage.setItem('jwt', responseData.accessToken);
        // redirect('./home')
        Router.push('./workshop3-4/home')
      } catch (error) {
        // Log error
        console.error('Login error:', error);
        setMessage('Invalid email or password')
        // Handle error in your UI
      }
    }

  };


  return (
    <Container component="main" maxWidth="xs">
      <Box
        sx={{
          marginTop: 8,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
          <TextField
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={(e) => setEmail(e.target.value)}
            onBlur={() => handleBlur('email')}
          />
          <TextField
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(e) => setPassword(e.target.value)}
            onBlur={() => handleBlur('password')}
          />
          {/* <FormControlLabel
          control={<Checkbox value="remember" color="primary" />}
          label="Remember me"
        /> */}
          <Button
            type="submit"
            fullWidth
            variant="contained"
            sx={{ mt: 3, mb: 2 }}
          >
            Sign In
          </Button>
          {!message ?
          '' 
          : <Typography sx={{color:'red'}}>{message}</Typography>}
        </Box>
      </Box>

    </Container>
  );
}
