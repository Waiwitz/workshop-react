import React, { useState, useEffect } from "react";
import { TextField, Button, Grid, Card, CardContent, CardMedia, Typography } from "@mui/material";
import styled from "@emotion/styled";
import Navbar from "src/components/Por/Navbar";

// Styled components for styling
const SearchContainer = styled.div`
  padding: 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  & > div {
    display: flex;
    gap: 10px;
    align-items: center;
  }
`;

const UserCard = styled(Card)`
  margin: 10px;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const NavigationButtons = styled.div`
  display: flex;
  gap: 10px;
`;

const UserListPage = () => {
  const [users, setUsers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [searchTerm, setSearchTerm] = useState("");

  // ดึงข้อมูลผู้ใช้เมื่อคอมโพเนนต์ถูกโหลด
  useEffect(() => {
    fetchUsers(currentPage, searchTerm);
    console.log("ดึงข้อมูลผู้ใช้เมื่อคอมโพเนนต์ถูกโหลด");
  }, [currentPage, searchTerm]);

  // ฟังก์ชันในการดึงข้อมูลผู้ใช้จาก API
  const fetchUsers = async (page, search = "") => {
    try {
      const apiUrl = `${process.env.NEXT_PUBLIC_API_URL}/users?page=${page}&per_page=10&search=${search}`;
      const response = await fetch(apiUrl);
      const data = await response.json();

      if (data && Array.isArray(data.data)) {
        setUsers(data.data);
        setTotalPages(data.total_pages);
        console.log("ดึงข้อมูลผู้ใช้สำเร็จ:", data);
      }
    } catch (error) {
      console.error("ผิดพลาดในการดึงข้อมูลผู้ใช้:", error);
    }
  };

  // ฟังก์ชันในการเปลี่ยนหน้า
  const handleNextPage = () => {
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
      fetchUsers(currentPage + 1, searchTerm);
      console.log("เปลี่ยนไปที่หน้าถัดไป");
    }
  };

  const handlePrevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
      fetchUsers(currentPage - 1, searchTerm);
      console.log("เปลี่ยนไปที่หน้าก่อนหน้า");
    }
  };

  // ฟังก์ชันในการค้นหา
  const handleSearch = () => {
    fetchUsers(1, searchTerm);
    setCurrentPage(1);
    console.log("เริ่มค้นหาผู้ใช้ด้วยคำค้น:", searchTerm);
  };

  // ฟังก์ชันที่เรียกเมื่อคลิกที่การ์ดผู้ใช้
  const handleCardClick = (userId) => {
    window.location.href = `/user/${userId}`;
  };

  return (
    <div>
      <Navbar />
      <SearchContainer>
        <div>
          <TextField
            label="ค้นหาผู้ใช้"
            variant="outlined"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
          <Button
            variant="contained"
            onClick={handleSearch}
            sx={{ backgroundColor: "#2196F3", color: "white" }}
          >
            ค้นหา
          </Button>
        </div>
        <NavigationButtons>
          <Button
            variant="contained"
            onClick={handlePrevPage}
            disabled={currentPage === 1}
            sx={{ backgroundColor: "#2196F3", color: "white" }}
          >
            หน้าก่อนหน้า
          </Button>
          <Button
            variant="contained"
            onClick={handleNextPage}
            disabled={currentPage === totalPages}
            sx={{ backgroundColor: "#2196F3", color: "white" }}
          >
            หน้าถัดไป
          </Button>
        </NavigationButtons>
      </SearchContainer>

      <Grid container spacing={2}>
        {users.map((user) => (
          <Grid item key={user.id} xs={12} sm={6} md={4} lg={3}>
            <UserCard onClick={() => handleCardClick(user.id)}>
              <CardMedia
                component="img"
                style={{ width: "50%", height: "50%", marginTop: '1rem'}}
                image={user.avatar}
                alt={`${user.fname} ${user.lname}`}
              />

              <CardContent>
                <Typography variant="h6">{`${user.fname} ${user.lname}`}</Typography>
                <Typography variant="body2">{user.username}</Typography>
              </CardContent>
            </UserCard>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default UserListPage;
