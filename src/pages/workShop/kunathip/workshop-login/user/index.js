// นำเข้า Hook และ Component ที่ต้องใช้
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { Container, Typography, Button, Avatar, CircularProgress, Paper } from '@mui/material';
import { styled } from '@mui/system';
import Navbar from 'src/components/Por/Navbar';

// Styled Components สำหรับกำหนดสไตล์
const StyledContainer = styled(Container)({
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  marginTop: '2em',
});

const UserInfoContainer = styled(Paper)({
  textAlign: 'center',
  marginTop: '1em',
  padding: '2em',
});

const AvatarImage = styled(Avatar)({
  width: '150px',
  height: '150px',
  margin: '0 auto 1em',
});

const LogoutButton = styled(Button)({
  marginTop: '1em',
});

const LoadingSpinner = styled(CircularProgress)({
  margin: '2em',
});
// ส่วนหลักของ Component User
const User = () => {
  // ใช้ Hook สำหรับการนำทาง
  const router = useRouter();
   // สถานะ state สำหรับเก็บข้อมูลผู้ใช้
  const [user, setUser] = useState(null);
// Effect นี้ทำหน้าที่ดึงข้อมูลผู้ใช้เมื่อ Component ถูกโหลด
  useEffect(() => {
    const fetchUserData = async () => {
      try {
        // ดึง Access Token จาก Local Storage
        const accessToken = localStorage.getItem('accessToken');
        // ถ้าไม่พบ Access Token จะแสดงข้อผิดพลาด
        if (!accessToken) {
          console.error('No access token found');
          return;
        }
        // ทำการส่งคำขอไปยัง API เพื่อขอข้อมูลผู้ใช้
        const response = await axios.get(`${process.env.NEXT_PUBLIC_API_URL}/auth/user`, {
          headers: {
            Authorization: `Bearer ${accessToken}`,
          },
        });
        // ถ้าคำขอสำเร็จ จะเซ็ตข้อมูลผู้ใช้ลงใน state
        if (response.data.status === 'ok') {
          setUser(response.data.user);
          localStorage.setItem('userToken', JSON.stringify(response.data.user));
          JSON.parse(localStorage.getItem('userToken'));
          console.log('User data:', response.data.user);
        } else {
          // ถ้ามีข้อผิดพลาดจะแสดงข้อความผิดพลาด
          console.error('Error fetching user data:', response.data.message);
        }
      } catch (error) {
        // แสดงข้อผิดพลาดที่เกิดขึ้นในการทำคำขอ
        console.error('API request error:', error);
      }
    };
    // เรียกใช้งานฟังก์ชันดึงข้อมูลผู้ใช้
    fetchUserData();// บอกให้ Effect นี้ทำงานเฉพาะเมื่อ Component ถูกโหลด
  }, []);
  // ฟังก์ชันสำหรับการออกจากระบบ
  const handleLogout = () => {
    // ลบข้อมูลทั้งหมดใน Local Storage
    localStorage.clear();
    // นำทางไปยังหน้า Login
    router.push('/workShop/kunathip/workshop-login/login');
  };

  return (
    <>
    <Navbar/>
    <StyledContainer>
      <Typography variant="h4" gutterBottom>
        User Information
      </Typography>
      {user ? (
        <UserInfoContainer elevation={3}>
          <AvatarImage alt="User Avatar" src={user.avatar} />
          <Typography variant="h6" gutterBottom>
            ID: {user.id}
          </Typography>
          <Typography variant="h6" gutterBottom>
            Name: {user.fname} {user.lname}
          </Typography>
          <Typography variant="h6" gutterBottom>
            Email: {user.email}
          </Typography>
          <LogoutButton variant="contained" color="primary" onClick={handleLogout}>
            Logout
          </LogoutButton>
        </UserInfoContainer>
      ) : (
        <LoadingSpinner />
      )}
    </StyledContainer>
    </>
  );
};

export default User;
