import React, { useEffect, useState } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Grid from "@mui/material/Grid";
import Navbar from "src/components/Por/Navbar";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";

const Attractions = () => {
  const [attractions, setAttractions] = useState([]);
  const [search, setSearch] = useState("");

  useEffect(() => {
    fetchAttractions();
  }, []);
  const handleSearch = () => {
    console.log("Searching for:", search);
    fetchAttractions();
  };
  
  const fetchAttractions = () => {
    console.log("Fetching attractions with search term:", search);
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/attractions?search=${search}`)
      .then((response) => {
        console.log("Response status:", response.status);
        return response.json();
      })
      .then((data) => {
        console.log("Attractions data received:", data);
        setAttractions(data);
      })
      .catch((error) => {
        console.error("Error fetching attractions:", error);
      });
  };

  return (
    <React.Fragment>
      <Navbar />
      <CssBaseline />
      <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Typography variant="h4" component="div" gutterBottom>
          Attractions
        </Typography>
        <div style={{ display: "flex", alignItems: "center" }}>
          <TextField
            label="Search"
            variant="outlined"
            fullWidth
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            sx={{ marginRight: 1, width: "20%" }}
          />
          <Button
            variant="contained"
            onClick={handleSearch}
            sx={{ backgroundColor: "#19a7ce", color: "white" }}
          >
            Search
          </Button>
        </div>

        <Grid marginTop={1} container spacing={3}>
          {attractions.map((attraction) => (
            <Grid item key={attraction.id} xs={12} sm={6} md={4}>
              <Card>
                <CardMedia
                  component="img"
                  alt={attraction.name}
                  height="140"
                  image={attraction.coverimage}
                />
                <CardContent>
                  <Typography variant="h6" component="div">
                    {attraction.name}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    {attraction.detail}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>
      </Container>
    </React.Fragment>
  );
};

export default Attractions;
