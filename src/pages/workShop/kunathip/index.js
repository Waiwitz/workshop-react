import { Layout } from "src/layouts/dashboard/layout";
import { styled } from "@mui/system";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Link from "next/link";
import Button from "@mui/material/Button";
import TableContainer from "@mui/material/TableContainer";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableRow from "@mui/material/TableRow";
import TableCell from "@mui/material/TableCell";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));
const TableGrid = ({ children }) => {
  return (
    <TableContainer component={Paper} style={{ border: "2px solid lightgray", padding: "1em" }}>
      <Table>
        <TableBody>{children}</TableBody>
      </Table>
    </TableContainer>
  );
};
const kunathip = () => {
  const fontSize1 = {
    color: "#FFFFFF",
    textAlign: "left",
    fontSize: "25px",
    fontWeight: "bold",
  };

  const fontSize2 = {
    color: "#FFFFFF",
    textAlign: "left",
    fontSize: "40px",
    fontWeight: "bold",
  };

  const fontSize3 = {
    color: "#FFFFFF",
    textAlign: "left",
    fontSize: "15px",
  };

  const textWorkshop = {
    color: "#FFFFFF",
    textAlign: "left",
    fontSize: "30px",
    marginTop: "4rem",
    fontWeight: "bold",
    textAlign: "left",
    marginBottom:'0.5rem'
  };

  const buttonNext = {
    marginTop: "1em",
    cursor: "pointer",
    whiteSpace: "nowrap",
    margin: "0.5em",
    padding: "1em",
  };

  const imageProfile = {
    margin: "10px",
    borderRadius: "50%",
    boxShadow: "5px 10px 450px #6366F1",
    overflow: "hidden",
    width: "auto",
    height: "auto",
    maxWidth: "350px",
  };
  

  return (
    <>
     <Box sx={{ flexGrow: 1, backgroundColor: "#303841", padding: "2em" }}>
  <Grid container spacing={2}>
          <Grid item xs={12} sm={5} sx={{ color: "white", marginTop: "10em", marginLeft: "2em" }}>
            <p style={fontSize1}>Hello, I am</p>
            <p style={fontSize2}>Kunathip</p>
            <p style={fontSize3}>
              Hello! I'm Por, a seasoned programmer with expertise in software and web application
              development. With experience in various businesses and projects, I am dedicated to
              creating efficient and user-centric applications.
            </p>

            <div
              style={{
                marginTop: "1em",
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
              }}
            >
              <h1 style={textWorkshop}>My workshop</h1>
              <TableGrid>
                {[
                  { href: "/workShop/kunathip/workshop-resume", label: "Resume" },
                  { href: "/workShop/kunathip/workshop-calculator", label: "Workshop 1" },
                  { href: "/workShop/kunathip/workshop-homco", label: "Workshop 2" },
                  { href: "/workShop/kunathip/workshop-login/login", label: "Workshop 3/4" },
                  { href: "/workShop/kunathip/workshop-userapi/MediaCard", label: "Workshop 5 user" },
                  { href: "/workShop/kunathip/workshop-userapi/UserListPage", label: "Workshop 5 search" },
                  { href: "/workShop/kunathip/workshop-attapi/searchAttrac", label: "Workshop 5 attractions" },
                  { href: "/workShop/kunathip/workshop-shopping/product2", label: "Workshop 6 shopping" },
                ].map((item, index) => (
                  <TableRow key={index}>
                    <TableCell>
                      <Link href={item.href}>
                        
                          {item.label}
                        
                      </Link>
                    </TableCell>
                  </TableRow>
                ))}
              </TableGrid>
            </div>
          </Grid>
          <Grid
            item
            xs={12}
            sm={5}
            sx={{ marginTop: { xs: "2em", sm: "200px" }, alignItems: "right", marginLeft: "auto" }}
          >
            <img src="/imageknt/Profile.jpg" alt="Profile" style={imageProfile} />
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

kunathip.getLayout = (kunathip) => <Layout>{kunathip}</Layout>;
export default kunathip;
