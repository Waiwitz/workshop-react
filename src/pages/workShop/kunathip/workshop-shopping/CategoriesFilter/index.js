
import React from "react";
import { ButtonGroup, Button } from "@mui/material";

const CategoriesFilter = ({ categories = [], selectedCategory, setSelectedCategory }) => {
  return (
    <ButtonGroup color="primary" aria-label="outlined primary button group" style={{ marginBottom: "1rem" }}>
      <Button
        onClick={() => setSelectedCategory("")}
        variant={selectedCategory === "" ? "contained" : "outlined"}
      >
        All
      </Button>
      {categories.map((category) => (
        <Button
          key={category}
          onClick={() => setSelectedCategory(category)}
          variant={selectedCategory === category ? "contained" : "outlined"}
        >
          {category}
        </Button>
      ))}
    </ButtonGroup>
  );
};

export default CategoriesFilter;
