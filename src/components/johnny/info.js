import React from 'react'
import { Fade, Bounce, Flip, Slide } from "react-awesome-reveal";

import PersonalInfo from './info/personalInfo'
import Contact from './info/contact';

function info() {
    return (

        <div id='aboutme' className="py-36"> 
            {/* title */}
            <Slide triggerOnce direction='up' duration={1800}  className='my-8 text-2xl font-bold text-gray-600'>
                <h2  >
                    About me
                </h2>
            </Slide>
            <div className="grid grid-cols-3 gap-8">
                <Fade cascade damping={0.1} className='col-span-2'>
                    <PersonalInfo />
                </Fade>
                <Fade>
                    <Contact/>
                </Fade>
                
            </div>
            
        </div>
    )
}

export default info