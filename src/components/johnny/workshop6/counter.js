// Counter.js
import React, { useState } from 'react';
import {
    Button,
    Box,
    Typography
} from '@mui/material';

const Counter = ({ quantity, onUpdateQuantity }) => {
  const [count, setCount] = useState(quantity);

  const handleIncrement = () => {
    const newCount = count + 1;
    setCount(newCount);
    onUpdateQuantity(newCount);
  };

  const handleDecrement = () => {
    if (count > 0) {
      const newCount = count - 1;
      setCount(newCount);
      onUpdateQuantity(newCount);
    }
  };

  return (
    <Box sx={{marginLeft:'auto',marginRight:'20px'}}>
      <Button variant="outlined" size="small" onClick={handleDecrement}
        sx={{
            padding:'0',
            minWidth:'2.5em',
        }}
      >-</Button>
      <span style={{ margin: '0 10px' }}>{count}</span>
      <Button variant="outlined" size="small" onClick={handleIncrement}
        sx={{
            padding:'0',
            minWidth:'2.5em',
        }}
      >+</Button>
    </Box>
  );
};

export default Counter;
