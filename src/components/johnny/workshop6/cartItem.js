import React, { useState, useEffect } from 'react';
import { Card, CardContent, CardMedia, Typography, Button, Box, Checkbox } from '@mui/material';

import Counter from './counter'

export default function cartItem({ product, checked }) {

  const { id, title, description, image, price, quantity,totalPrice } = product;


  const handleUpdateQuantity = (newQuantity) => {
    // Update local storage with the new quantity
    const cartItems = JSON.parse(localStorage.getItem('cartItems')) || [];
    const updatedCart = cartItems.map((item) =>
      item.id === id ? { ...item, quantity: newQuantity, totalPrice: item.price * newQuantity } : item
    );
    localStorage.setItem('cartItems', JSON.stringify(updatedCart));
  };

  return (


    <Card sx={{ display: 'flex', marginBottom: '10px', width: '100%', padding: '0' }}>
       {/* <Checkbox
        checked={checked}
        onChange={(e) => handleUpdateChecked(e.target.checked)}
        sx={{ margin: 'auto 0' }}
      /> */}
      <CardMedia
        component="img"
        image={image}
        alt={title}
        sx={{
          maxHeight: '100px',
          height: '100px',
          minWidth: '20%',
          maxWidth:'20%',
          objectFit: 'contain',
          margin: 'auto 0',
          marginLeft: '10px',
          padding: '10px'
        }}
      />
      <Box sx={{ minWidth:'77%',maxWidth:'77%', display: 'flex'}}>
        <Box sx={{ margin: 'auto 0',marginLeft:'10px',minWidth:'97%',maxWidth:'97%', }}>
          <Typography variant="h5" noWrap component="div" sx={{ marginBottom: '0' }}>
            {title}
          </Typography>
          <Typography sx={{ color: 'gray', fontSize: '.9em' }}>{price}$</Typography>
          <Box sx={{ display: 'flex' }}>
            <Typography variant="h6" color="text.primary" sx={{ margin: 'auto 0' }}>
              {totalPrice}$
            </Typography>
            <Counter quantity={quantity} onUpdateQuantity={handleUpdateQuantity} />
          </Box>
        </Box>
      </Box>
    </Card>
  )
}
