import React, { useState, useEffect } from 'react';
import {
    Container,
    Typography,
    CircularProgress,
    Button
} from '@mui/material';
import CLayout from '../cLayout';
import UserModal from '../userModal'

export default function UserDetail() {

    const [userData, setUserData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [openModal, setOpenModal] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(process.env.NEXT_PUBLIC_API_URL + "/users/1");
                const res = await response.json();
                setUserData(res.user);
                console.log("userData:", userData)
                setLoading(false);
            } catch (error) {
                console.error('Error fetching data:', error);
                setLoading(false);
            }
        };

        fetchData();
    }, [openModal]);

    const handleOpenModal = () => setOpenModal(true);
    const handleCloseModal = () => setOpenModal(false);

    return (
        <CLayout title="User Detail">
            {loading ? (
                <CircularProgress />
            ) : (
                <>
                    <UserModal userData={userData} open={openModal} handleClose={handleCloseModal} />
                    <Button onClick={handleOpenModal}
                        sx={{
                            backgroundColor:'black',
                            color:'white',
                            borderRadius:'10px',
                            '&:hover': {
                                backgroundColor: '#373737',
                            },
                        }}
                    >User Detail</Button>
                    {/* Display other user details here */}
                </>
            )}
        </CLayout>
    )
}
