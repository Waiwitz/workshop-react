import React, { useState, useEffect } from 'react';
import { Container, Typography, CircularProgress } from '@mui/material';
import CLayout from '../cLayout';
import UserTable from '../userTable';

export default function sort() {

    const [userData, setUserData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [column,setColumn] = useState("id")
    const [order,setOrder] = useState("desc") 
    
    useEffect(() => {
      const fetchData = async () => {
        try {
          const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/users?sort_column=${column}&sort_order=${order}`);
          const res = await response.json();
            
          console.log("sort:",res)

          setUserData(res);
          console.log("userData:",userData)
          setLoading(false);
        } catch (error) {
          console.error('Error fetching data:', error);
          setLoading(false);
        }
      };
  
      fetchData();
    }, []);

  return (
    <CLayout title='Sort'>
        {loading ? (
                <CircularProgress />
            ) : (
                <>
                    <UserTable userData={userData} />
                </>
            )}
    </CLayout>
  )
}
