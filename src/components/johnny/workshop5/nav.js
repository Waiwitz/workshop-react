import React from 'react';
import { AppBar, Toolbar, Typography, Button } from '@mui/material';

const Navbar = () => {
  return (
    <AppBar position="static" sx={{borderRadius:'0 0 12px 12px'}}>
      <Toolbar sx={{backgroundColor:"white",color:"black",borderRadius:'0 0 10px 10px'}}>
        <Typography component="div" sx={{ flexGrow: 1, fontWeight:'800', fontSize:'1.7em', }}>
          WorkShop 5
        </Typography>
        <Button color="inherit">Home</Button>
        <Button color="inherit">About</Button>
        <Button color="inherit">Contact</Button>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
