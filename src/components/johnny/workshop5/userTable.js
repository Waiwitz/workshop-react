import React from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Avatar } from '@mui/material';

const UserTable = ({ userData }) => {
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell>First Name</TableCell>
            <TableCell>Last Name</TableCell>
            <TableCell>Username</TableCell>
            <TableCell>Avatar</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {userData.length > 0 ? 
            userData.map((user)=>(
              <TableRow key={user.id}>
              <TableCell>{user.id}</TableCell>
              <TableCell>{user?.fname}</TableCell>
              <TableCell>{user?.lname}</TableCell>
              <TableCell>{user?.username}</TableCell>
              <TableCell>
                <Avatar alt={`Avatar-${user.id}`} src={user.avatar} />
              </TableCell>
            </TableRow>
            ))
            : <>no user</>
          }
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default UserTable;
