import React, { useState, useEffect } from 'react';
import { Container, Typography, CircularProgress } from '@mui/material';
import CLayout from '../cLayout';
import AttrTable from '../attrTable';

export default function sort() {

    const [attrData, setAttrData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [column,setColumn] = useState("id")
    const [order,setOrder] = useState("desc") 
    
    useEffect(() => {
      const fetchData = async () => {
        try {
          const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/attractions?sort_column=${column}&sort_order=${order}`);
          const res = await response.json();
            
          console.log("sort:",res)

          setAttrData(res);
          console.log("attrData:",attrData)
          setLoading(false);
        } catch (error) {
          console.error('Error fetching data:', error);
          setLoading(false);
        }
      };
  
      fetchData();
    }, []);

  return (
    <CLayout title='Sort'>
        {loading ? (
                <CircularProgress />
            ) : (
                <>
                   <AttrTable attrData={attrData} />
                </>
            )}
    </CLayout>
  )
}
