import React, { useState, useEffect } from 'react'
import { 
    CircularProgress,
    TextField,
} from '@mui/material'
//* import components
import CLayout from '../cLayout'
import AttrTable from '../attrTable';

export default function search() {

    const [attrData, setAttrData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [searchTerm, setSearchTerm] = useState('');

    useEffect(() => {
        // console.log(searchTerm)
        const fetchData = async () => {
            try {
                // const searchQuery = 'karn';
                const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/attractions?search=${searchTerm}`);
                const data = await response.json();
                setAttrData(data);
                setLoading(false);
            } catch (error) {
                console.error('Error fetching data:', error);
                setLoading(false);
            }
        };

        fetchData();
    }, [searchTerm]);

    // const handleSearchChange = (event) => {
    //     setSearchTerm(event.target.value);
    //   };

    return (
        <CLayout title="Search">
            <TextField
                label="Search"
                variant="outlined"
                fullWidth
                value={searchTerm}
                onChange={e=>setSearchTerm(e.target.value)}
                sx={{
                    marginBottom:'20px'
                }}
            />
            {loading ? (
                <CircularProgress />
            ) : (
                <AttrTable attrData={attrData} />
            )}
        </CLayout>
    )
}
