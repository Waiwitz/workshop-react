import React from 'react'
import {
    Container,
    Box,
    Typography,
    Button,
    Grid,
} from "@mui/material"


export default function campaign() {

    const card =[
        {
            title:`HOME DESIGN CONSULTATION`,
            detail:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
        },
        {
            title:'HOME DESIGN 3D 2D INTERIOR SERVICE',
            detail:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
        },
        {
            title:'HOME DESIGN CONSULTATION',
            detail:'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
        },
    ]

  return (
    <Container maxWidth='full' >
    <Grid  container
        sx={{
            // borderLeft:'3px solid black',
            padding:'0 40px',
            marginTop:'60px',
        }}
    >
        {card.map((card, index) => (
  <Grid
    item
    lg={4}
    sx={{
      borderRight: index == 2  ? 'none' : '3px solid black', // Apply border only to non-last cards
      padding: '1em',
    }}
    key={index}
  >
    <Typography
      sx={{
        fontSize: "1.5em",
        fontWeight: "900",
      }}
    >
      {card.title}
    </Typography>
    <Typography>{card.detail}</Typography>
  </Grid>
))}


        
    </Grid>
    </Container>
  )
}
