import React from 'react'

export default function footer() {
    return (

        <footer className="bg-white rounded-lg shadow-lg m-4 mt-20">
            <div className="w-full max-w-screen-xl mx-auto p-4 md:py-8">
                <div className="sm:flex sm:items-center sm:justify-between">
                    <a href="#" className="flex items-center mb-4 sm:mb-0 space-x-3 rtl:space-x-reverse">
                        <span className="self-center text-2xl font-semibold whitespace-nowrap ">NatpacanSri</span>
                    </a>
                    <ul className="flex flex-wrap items-center mb-6 text-sm font-medium text-gray-500 sm:mb-0 ">
                        <li>
                            <a href="https://www.facebook.com/profile.php?id=100013560722124" className="hover:underline me-4 md:me-6">Facebook</a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/jxhnn11nny/" className="hover:underline me-4 md:me-6">Instagram</a>
                        </li>
                        <li>
                            <a href="https://github.com/NatpacanSri" className="hover:underline me-4 md:me-6">Github</a>
                        </li>

                    </ul>
                </div>
                <hr className="border-gray-200 my-5" />
                <span className="block text-sm text-gray-500 sm:text-center">© 2023 <a href="#" className="hover:underline">NatpacanSri</a>. All Rights Reserved.</span>
            </div>
        </footer>


    )
}
