import React from "react";
import { Grid, Paper, Typography, Container, Box, Avatar } from "@mui/material";
import { styled } from "@mui/system";

const CustomContentGrid = () => {
  const data = [
    {
      title: "Title 1",
      content:
        "Content for box 1. Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
    },
    {
      title: "Title 2",
      content:
        "Content for box 2. Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
    },
    {
      title: "Title 3",
      content:
        "Content for box 3. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.",
    },
    {
      title: "Title 4",
      content:
        "Content for box 6. Lorem ipsum dolor sit amet, consectetur adipiscing elit  officia deserunt mollit anim id est laborum.",
    },
    {
      title: "Title 5",
      content:
        "Content for box 6. Lorem ipsum dolor sit amet, consectetur adipiscing elit  officia deserunt mollit anim id est laborum.",
    },
    {
      title: "Title 6",
      content:
        "Content for box 6. Lorem ipsum dolor sit amet, consectetur adipiscing elit  officia deserunt mollit anim id est laborum.",
    },
  ];

  return (
    <Container
      maxWidth="lg"
      style={{
        backgroundColor: "#00000",
        minHeight: "80em",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        paddingTop: "50px",
      }}
    >
      <Container maxWidth="lg">
        <Grid container spacing={10}>
          {data.map((item, index) => (
            <Grid item xs={12} sm={6} md={4} key={index}>
              <Box
                sx={{
                  width: "auto",
                  height: "auto",
                  backgroundColor: "#4db6ac",
                  textAlign: "center",
                  padding: 2,
                }}
              >
                {/* รูปภาพ */}
                <Avatar
                  alt="Profile Image"
                  src="/tcc.jpg"
                  sx={{
                    width: "100%",
                    height: "auto",
                    maxWidth: 200,
                    maxHeight: 200,
                    backgroundColor: "#000000",
                    margin: "auto",
                  }}
                />
              </Box>
              <Paper elevation={3} style={{ padding: 20, width: "100%" }}>
                {/* Title */}
                <Typography
                  variant="h6"
                  style={{
                    marginTop: 10,
                    padding: 20,
                    fontSize: "30px",
                    fontWeight: "bold",
                    color: "#000000",
                    letterSpacing: 1,
                  }}
                >
                  {item.title}
                </Typography>
                {/* Content */}
                <Typography variant="body2">{item.content}</Typography>
              </Paper>
            </Grid>
          ))}
        </Grid>
      </Container>
    </Container>
  );
};

export default CustomContentGrid;
