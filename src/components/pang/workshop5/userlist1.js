// import React, { useState } from "react";
// import Button from "@mui/material/Button";
// import Typography from "@mui/material/Typography";
// import Avatar from "@mui/material/Avatar";
// import Table from "@mui/material/Table";
// import TableBody from "@mui/material/TableBody";
// import TableCell from "@mui/material/TableCell";
// import TableContainer from "@mui/material/TableContainer";
// import TableHead from "@mui/material/TableHead";
// import TableRow from "@mui/material/TableRow";
// import Paper from "@mui/material/Paper";
// import TextField from "@mui/material/TextField";

// const Workshop5 = () => {
//   const [userList, setUserList] = useState([]);

//   const fetchUsers = () => {
//     fetch("https://www.melivecode.com/api/users")
//       .then((response) => response.json())
//       .then((data) => {
//         setUserList(data);
//         console.log("Users API Response เส้น 1:", data);
//       })
//       .catch((error) => console.error("Error fetching users:", error));
//   };

  

//   return (
//     <div>
//       <Typography variant="h4">Workshop 5 - เส้นที่ 1 </Typography>
//       <Button variant="contained" color="primary" onClick={fetchUsers}>
//         USER LIST
//       </Button>

//       {userList.length > 0 && (
//         <div>
//           <Typography variant="h5">User List</Typography>
//           <TableContainer component={Paper}>
//             <Table>
//               <TableHead>
//                 <TableRow>
//                   <TableCell>Avatar</TableCell>
//                   <TableCell>Name</TableCell>
//                   <TableCell>Username</TableCell>
//                 </TableRow>
//               </TableHead>
//               <TableBody>
//                 {userList.map((user) => (
//                   <TableRow key={user.id}>
//                     <TableCell>
//                       <Avatar alt={`${user.fname} ${user.lname}`} src={user.avatar} />
//                     </TableCell>
//                     <TableCell>{`${user.fname} ${user.lname}`}</TableCell>
//                     <TableCell>{user.username}</TableCell>
//                   </TableRow>
//                 ))}
//               </TableBody>
//             </Table>
//           </TableContainer>
//         </div>
//       )}
//     </div>
//   );
// };

// export default Workshop5;
