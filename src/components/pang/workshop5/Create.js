import React, { useState } from "react";
import { Button, TextField, Typography } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const Create = ({ onCreate }) => {
  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [email, setEmail] = useState("");
  const [avatar, setAvatar] = useState("");
  const [createLog] = useState("");

  const theme = createTheme({
    palette: {
      primary: {
        main: "#00bcd4",
        light: "#42a5f5",
        dark: "#1565c0",
        contrastText: "#fff",
      },
    },
  });

  const handleCreate = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/create`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          fname,
          lname,
          username,
          password,
          email,
          avatar,
        }),
      });

      const data = await response.json();

      if (response.ok) {
        console.log("ข้อมูลของเส้นที่ 7.", "Create", data);
        onCreate(data);
      } else {
        console.error("errorrr", "Create failed:", data);
      }
    } catch (error) {
      console.error("errorrr", "An error occurred during login:", error);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <div style={{ display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center"}}>
        <Typography variant="h5" gutterBottom>
          Create User
        </Typography>
        <TextField
          label="First Name"
          variant="outlined"
          width="50px"
          margin="normal"
          size="small"
          value={fname}
          onChange={(e) => setFname(e.target.value)}
        />
        <TextField
          label="Last Name"
          variant="outlined"
          width="50px"
          margin="normal"
          size="small"
          value={lname}
          onChange={(e) => setLname(e.target.value)}
        />
        <TextField
          label="Username"
          variant="outlined"
          width="50px"
          margin="normal"
          size="small"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <TextField
          label="Password"
          type="password"
          variant="outlined"
          width="50px"
          margin="normal"
          size="small"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <TextField
          label="Email"
          type="email"
          variant="outlined"
          width="50px"
          margin="normal"
          size="small"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
        />
        <TextField
          label="Avatar"
          type="link"
          variant="outlined"
          width="50px"
          margin="normal"
          size="small"
          value={avatar}
          onChange={(e) => setAvatar(e.target.value)}
        />
        <Button variant="contained" color="primary" size="small" onClick={handleCreate}>
          Create User
        </Button>
        {createLog && (
          <Typography variant="body2" color="error" gutterBottom>
            {createLog}
          </Typography>
        )}
      </div>
    </ThemeProvider>
  );
};

export default Create;
