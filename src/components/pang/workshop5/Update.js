import React, { useState } from "react";
import { Button, TextField, Typography } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const Update = ({ onUpdate }) => {
  const [userId, setUserId] = useState(""); // Add state for user ID
  const [lname, setLname] = useState("");
  const [updateLog] = useState("");

  const theme = createTheme({
    palette: {
      primary: {
        main: "#00bcd4",
        light: "#42a5f5",
        dark: "#1565c0",
        contrastText: '#fff',
      },
    },
  });

  const handleUpdate = async () => {
    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/update`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          id: userId,
          lname,
        }),
      });

      const data = await response.json();

      if (response.ok) {
        console.log("ข้อมูลของเส้นที่ 8", "Update", data);
        onUpdate(data);
      } else {
        console.error("error", "Update failed:", data);
      }
    } catch (error) {
      console.error("error", "An error occurred during update:", error);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <div style={{ display: "flex", flexDirection: "column", alignItems: "center", justifyContent: "center"}}>
        <Typography variant="h5" gutterBottom>
          Update User
        </Typography>
        <TextField
          label="User ID"
          variant="outlined"
          width="50px"
          margin="normal"
          size="small"
          value={userId}
          onChange={(e) => setUserId(e.target.value)}
        />
        <TextField
          label="Last Name"
          variant="outlined"
          width="50px"
          margin="normal"
          size="small"
          value={lname}
          onChange={(e) => setLname(e.target.value)}
        />
        <Button variant="contained" color="primary" width="25px" onClick={handleUpdate}>
          Update User
        </Button>
        {updateLog && (
          <Typography variant="body2" color="error" gutterBottom>
            {updateLog}
          </Typography>
        )}
      </div>
    </ThemeProvider>
  );
};

export default Update;
