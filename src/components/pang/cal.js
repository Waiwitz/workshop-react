import React, { useState } from "react";
import { Button, Grid, TextField, Paper } from "@mui/material";
import Link from "next/link";

// กันลืม สร้าง state input และ setInput โดยเริ่มต้นค่าที่ input เป็นข้อความว่าง
const Calculator = () => {
  const [input, setInput] = useState("");

  // ทำหน้าที่รับค่า value และเพิ่มค่านี้เข้าไปใน state input
  //โดยใช้ฟังก์ชั่น setInput และผ่านค่า prevInput (ค่าเดิมของ input) เพื่อเพิ่มค่าใหม่
  const handleButtonClick = (value) => {
    setInput((prevInput) => prevInput + value);
    console.log(value)
  };

  const handleClear = () => {
    setInput("");
    console.log(input)
  };

  // ใช้เพื่อคำนวณผลลัพธ์ของสมการที่ป้อนเข้ามา โดยใช้ eval ซึ่งเป็นฟังก์ชั่นที่ใช้สำหรับคำนวณผลลัพธ์จากสตริงที่มีรูปแบบ js
  const handleCalculate = () => {
    try {
      setInput(eval(input).toString());
      console.log(input)
    } catch (error) {
      setInput("Error");
    }
  };

  return (
    <Paper elevation={3} style={{ padding: "20px", maxWidth: "400px", margin: "auto" }}>
      <Grid container spacing={1} justifyContent="center">
        <Grid item xs={12} md={12}>
          <TextField
            fullWidth
            variant="outlined"
            value={input}
            onChange={(e) => setInput(e.target.value)}
            inputProps={{
              style: {fontSize: 25, textAlign: "center" } 
            }}
          />
        </Grid>
        {/* map เพื่อวนลูปผ่านอาร์เรย์ของสตริงที่แทนปุ่มบน cal */}
        {["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", ".", "/", "-", "*", "+"].map((value) => (
            <Grid item xs={8} md={4} key={value}>
              <Button
                fullWidth
                variant="outlined"
                onClick={() => handleButtonClick(value)}
                style={{ margin: "3px", fontSize: "1.5rem" }}
              >
                {value}
              </Button>
            </Grid>
          )
        )}
        <Grid item xs={6} md={3}>
          <Button
            fullWidth
            variant="contained"
            onClick={handleCalculate}
            style={{ margin: "4px", backgroundColor: "#FFFFF", color: "#fff", fontSize: "1.2rem" }}
          >
            =
          </Button>
        </Grid>
        <Grid item xs={6} md={3}>
          <Button
            fullWidth
            variant="contained"
            onClick={handleClear}
            style={{ margin: "4px", backgroundColor: "#FFFFF", color: "#fff", fontSize: "1.2rem" }}
          >
            C
          </Button>
        </Grid>
      </Grid>
    </Paper>
    
  );
};
Calculator.getLayout = (page) => <Layout>{page}</Layout>;
export default Calculator;
