import {
    Box,
    Button,
    IconButton,
  
    Typography,
  } from "@mui/material";
  import React, { useEffect, useState } from 'react';
  
  const UserInfo = ({ jwt }) => {
    const [userData, setUserData] = useState(null);
  
    useEffect(() => {
      const fetchUserData = async () => {
        try {
          const response = await fetch('https://www.melivecode.com/api/auth/user', {
            method: 'GET',
            headers: {
              Authorization: `Bearer ${jwt}`,
            },
          });
  
          // if (!password) {
          //   console.error("error", "Password is required na");
          //   return;
          // }
    
          // if (!username) {
          //   console.error("error", "Username is required na");
          //   return;
          // }
  
          const data = await response.json();
  
          if (response.ok) {
            console.log('ข้อมูล', 'สำเร็จ:', data);
            setUserData(data);
            localStorage.setItem('workshop4', JSON.stringify(data));
          } else {
            console.error('error', 'Failed to fetch user data:', data);
          }
        } catch (error) {
          console.error('error', 'An error occurred during user data fetch:', error);
        }
      };
  
      fetchUserData();
    }, [jwt]);
  
    return (
      <div>
         <Typography
          variant="h3"
          mt={5}
          mb={5}
          sx={{
              textAlign: "center"
          }}>WORKSHOP4</Typography>
        {userData ? (
          <div>
              <Typography
              mt={2} variant="h6" sx={{textAlign: "center"}}>Your profile: <img src={userData.user.avatar} alt="User Avatar" /></Typography>
              {/* <Typography
              mt={2} >ID: {userData.user.id}</Typography> */}
              <Typography  variant="h6" mt={2} >Username: {userData.user.username}</Typography>
              {/* <Typography>Password: {userData.user.password}</Typography> */}
              <Typography  variant="h6" mt={1}>First Name: {userData.user.fname}</Typography>
              <Typography  variant="h6" mt={1}>Last Name: {userData.user.lname}</Typography>
          </div>
        ) : (
          <p>Loading user data...</p>
        )}
      </div>
    );
  };
  
  export default UserInfo;
  