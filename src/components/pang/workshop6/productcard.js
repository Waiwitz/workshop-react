import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";

const ProductCard = ({ product, addToCart }) => {
  return (
    <Grid item xs={12} sm={6} md={4}>
      <Card>
        <CardMedia component="img" height="150" image={product.picture} alt={product.Name} />
        <CardContent>
          <Typography variant="h6">{product.Name}</Typography>
          <Typography variant="body2" color="text.secondary">
            {product.description}
          </Typography>
          <Typography variant="subtitle1">Price: {product.price}.-</Typography>
          <Typography variant="subtitle1">Discount: {product.discountPercentage}%</Typography>
          <Typography variant="subtitle1">Category: {product.category}</Typography>
          <div style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
            <Button
              style={{
                marginTop: "10px",
                backgroundColor: "#004d40",
                borderRadius: "18px",
                fontSize: "15px",
              }}
              onClick={() => addToCart(product)} // Use addToCart with the product parameter
            >
              <div style={{ color: "#FFFFFF" }}>ADD TO CART</div>
            </Button>
          </div>
        </CardContent>
      </Card>
    </Grid>
  );
};

export default ProductCard;
