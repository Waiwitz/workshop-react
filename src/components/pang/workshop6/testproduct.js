import React, { useState, useEffect } from "react";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Productfilter from "src/components/pang/workshop6/productfilter";
import ProductCard from "src/components/pang/workshop6/productcard";

const Testproduct = ({ setCartItems }) => {
  const [products, setProducts] = useState([
    {
      id: 1,
      Name: "Lychee Earl Grey Tea",
      description: "Earl Grey Tea, Lychee Juice & Chunks, Syrup and Butterfly Pea",
      price: 150,
      discountPercentage: 10,
      category: "Iced Tea",
      picture: "/pang/Lychee.jpg",
    },
    {
      id: 2,
      Name: "Iced Shaken Hibiscus Tea",
      description: "Hibiscus tea shake with iced",
      price: 115,
      discountPercentage: 0,
      category: "Iced Tea",
      picture: "/pang/Shaken.png",
    },
    {
      id: 3,
      Name: "Espresso Con Panna",
      description: "Espresso meets a dollop of whipped cream",
      price: 110,
      discountPercentage: 15,
      category: "Hot Coffee",
      picture: "/pang/Panna.png",
    },
    {
      id: 4,
      Name: "Cappuccino",
      description: "Espresso, steamed milk and layer of foam",
      price: 115,
      discountPercentage: 0,
      category: "Hot Coffee",
      picture: "/pang/Cappuccino.png",
    },
    {
      id: 5,
      Name: "Milk pudding",
      description: "Smooth creamy pudding made with real vanilla and fresh milk",
      price: 85,
      discountPercentage: 15,
      category: "Bakery",
      picture: "/pang/Puddingmilk.png",
    },
    {
      id: 6,
      Name: "Chocolate pudding",
      description: "Smooth creamy pudding made with chocolate and fresh milk",
      price: 85,
      discountPercentage: 0,
      category: "Bakery",
      picture: "/pang/Puddingcho.png",
    },
    {
      id: 7,
      Name: "Cheesecake",
      description: "The famous Basque-style burnt cheesecake",
      price: 140,
      discountPercentage: 15,
      category: "Cake",
      picture: "/pang/Cheesecake.png",
    },
    {
      id: 8,
      Name: "Strawberry Jelly Yogurt",
      description: "Vanilla chiffon cake layer with yoghurt top Strawberry Jelly",
      price: 165,
      discountPercentage: 0,
      category: "Cake",
      picture: "/pang/jelly.png",
    },
  ]);

  const [filteredProducts, setFilteredProducts] = useState(products);
  useEffect(() => {}, []);

  const handleFilter = (searchTerm) => {
    const filtered = products.filter((product) => {
      const nameMatch = product.Name.toLowerCase().includes(searchTerm.toLowerCase());
      const categoryMatch = product.category.toLowerCase().includes(searchTerm.toLowerCase());
      const priceMatch = product.price.toString().includes(searchTerm);
      return nameMatch || categoryMatch || priceMatch;
    });

    setFilteredProducts(filtered);
  };

  const addToCart = (product) => {
    setCartItems((prevCartItems) => [...prevCartItems, product]);
  };

  return (
    <div>
      <Productfilter onFilter={handleFilter} />

      <Grid container spacing={5} marginBottom={5}>
        {filteredProducts.map((product) => (
          <ProductCard key={product.id} product={product} addToCart={() => addToCart(product)} />
        ))}
      </Grid>
    </div>
  );
};

export default Testproduct;





// import React, { useState, useEffect } from "react";
// import Card from "@mui/material/Card";
// import CardContent from "@mui/material/CardContent";
// import CardMedia from "@mui/material/CardMedia";
// import Typography from "@mui/material/Typography";
// import Button from "@mui/material/Button";
// import Grid from "@mui/material/Grid";
// import Productfilter from "src/components/pang/workshop6/productfilter";

// const ProductCard = ({ product, addToCart }) => {
//   return (
//     <Grid item xs={12} sm={6} md={4}>
//       <Card>
//         <CardMedia component="img" height="150" image={product.picture} alt={product.Name} />
//         <CardContent>
//           <Typography variant="h6">{product.Name}</Typography>
//           <Typography variant="body2" color="text.secondary">
//             {product.description}
//           </Typography>
//           <Typography variant="subtitle1">Price: {product.price}.-</Typography>
//           <Typography variant="subtitle1">Discount: {product.discountPercentage}%</Typography>
//           <Typography variant="subtitle1">Category: {product.category}</Typography>
//           <div style={{ display: "flex", alignItems: "center", justifyContent: "center" }}>
//             <Button
//               style={{
//                 marginTop: "10px",
//                 backgroundColor: "#004d40",
//                 borderRadius: "18px",
//                 fontSize: "15px",
//               }}
//               onClick={() => addToCart(product)} // Use addToCart with the product parameter
//             >
//               <div style={{ color: "#FFFFFF" }}>ADD TO CART</div>
//             </Button>
//           </div>
//         </CardContent>
//       </Card>
//     </Grid>
//   );
// };

// const Testproduct = ({ setCartItems }) => {
//   const [products, setProducts] = useState([
//     {
//       id: 1,
//       Name: "Lychee Earl Grey Tea",
//       description: "Earl Grey Tea, Lychee Juice & Chunks, Syrup and Butterfly Pea",
//       price: 150,
//       discountPercentage: 10,
//       category: "Iced Tea",
//       picture: "/pang/Lychee.jpg",
//     },
//     {
//       id: 2,
//       Name: "Iced Shaken Hibiscus Tea",
//       description: "Hibiscus tea shake with iced",
//       price: 115,
//       discountPercentage: 15,
//       category: "Iced Tea",
//       picture: "/pang/Shaken.png",
//     },
//     {
//       id: 3,
//       Name: "Espresso Con Panna",
//       description: "Espresso meets a dollop of whipped cream",
//       price: 110,
//       discountPercentage: 15,
//       category: "Hot Coffee",
//       picture: "/pang/Panna.png",
//     },
//     {
//       id: 4,
//       Name: "Cappuccino",
//       description: "Espresso, steamed milk and layer of foam",
//       price: 115,
//       discountPercentage: 15,
//       category: "Hot Coffee",
//       picture: "/pang/Cappuccino.png",
//     },
//     {
//       id: 5,
//       Name: "Milk pudding",
//       description: "Smooth creamy pudding made with real vanilla and fresh milk",
//       price: 85,
//       discountPercentage: 15,
//       category: "Bakery",
//       picture: "/pang/Puddingmilk.png",
//     },
//     {
//       id: 6,
//       Name: "Chocolate pudding",
//       description: "Smooth creamy pudding made with chocolate and fresh milk",
//       price: 85,
//       discountPercentage: "",
//       category: "Bakery",
//       picture: "/pang/Puddingcho.png",
//     },
//     {
//       id: 7,
//       Name: "Cheesecake",
//       description: "The famous Basque-style burnt cheesecake",
//       price: 140,
//       discountPercentage: 15,
//       category: "Cake",
//       picture: "/pang/Cheesecake.png",
//     },
//     {
//       id: 8,
//       Name: "Strawberry Jelly Yogurt",
//       description: "Vanilla chiffon cake layer with yoghurt top Strawberry Jelly",
//       price: 165,
//       discountPercentage: "",
//       category: "Cake",
//       picture: "/pang/jelly.png",
//     },
//   ]);

//   const [filteredProducts, setFilteredProducts] = useState(products);
//   useEffect(() => {}, []);

//   const handleFilter = (searchTerm) => {
//     const filtered = products.filter((product) => {
//       const nameMatch = product.Name.toLowerCase().includes(searchTerm.toLowerCase());
//       const categoryMatch = product.category.toLowerCase().includes(searchTerm.toLowerCase());
//       const priceMatch = product.price.toString().includes(searchTerm);
//       return nameMatch || categoryMatch || priceMatch;
//     });

//     setFilteredProducts(filtered);
//   };

//   const addToCart = (product) => {
//     setCartItems((prevCartItems) => [...prevCartItems, product]);
//   };


//   return (
//     <div >
//       <Productfilter onFilter={handleFilter}/>

//       <Grid container spacing={5} marginBottom={5}>
//       {filteredProducts.map((product) => (
//         <ProductCard key={product.id} product={product} addToCart={() => addToCart(product)} />
//       ))}
//       </Grid>
//     </div>
//   );
// };

// export default Testproduct;



