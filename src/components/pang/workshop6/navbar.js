import React, { useState } from "react";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Avatar from "@mui/material/Avatar";
import Badge from "@mui/material/Badge";
import Button from "@mui/material/Button";
import CartPopup from "src/components/pang/workshop6/cartpopup";
import Link from "next/link"; 

const Nav = ({ cartItems }) => {
  const [cartOpen, setCartOpen] = useState(false);

  // const handleCartClick = () => {
  //   setCartOpen(!cartOpen);
  // };

  const toggleCart = () => {
    setCartOpen(!cartOpen);
  };

  return (
    <AppBar position="static" style={{ backgroundColor: "#004d40" }}>
      <Toolbar>
        <div style={{ display: "flex", alignItems: "center" }}>
          <Avatar
            alt="Starbuck logo"
            src="https://upload.wikimedia.org/wikipedia/en/thumb/d/d3/Starbucks_Corporation_Logo_2011.svg/800px-Starbucks_Corporation_Logo_2011.svg.png"
          /> 
         <Link href="/workShop/meldyjuju/workshop6"> <p style={{ color: "white", fontSize: "1.5rem" }}>Starbucks</p> </Link>
        </div>
        <div style={{ display: "none", marginLeft: "auto", spaceX: "4" }}>Coffee Drink</div>
        <div style={{ display: "flex", alignItems: "center", marginLeft: "auto" }}>
          <IconButton color="inherit" onClick={toggleCart}>
            <Badge badgeContent={cartItems.length} color="error">
              <ShoppingCartIcon />
            </Badge>
          </IconButton>
          {cartOpen && <CartPopup cartItems={cartItems} onClose={toggleCart} />}
          <div
            style={{
              position: "absolute",
              paddingTop: "2px",
              right: 0,
              zIndex: 10,
              borderRadius: "0 0 4px 4px",
            }}
          ></div>
          <div style={{ marginLeft: "1rem" }}></div>
          <div style={{ marginLeft: "1rem" }}>
            <Button style={{ color: "white" }}>Log In</Button>
            <Button style={{ color: "white" }}>Sign Up</Button>
          </div>
          <div style={{ marginLeft: "1rem" }}>
            <Button style={{ color: "white" }}>Logout</Button>
          </div>
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default Nav;
