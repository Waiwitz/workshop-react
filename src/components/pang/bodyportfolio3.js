import React from "react";
import { Box, Grid, Paper } from "@mui/material";

const CustomBox = () => {
  return (
    <div style={{
        fontSize: "40px",
        fontWeight: "bold",
        textAlign: "center",
        paddingTop: "40px",
        
      }}> PORTFOLIO
    <div style={{
        fontSize: "30px",
        fontWeight: "bold",
        textAlign: "center",
        paddingTop: "10px",
        
      }}> OUR RECENT WORK
    <Box display="flex" height="170vh" paddingTop="20px" >
      <Paper elevation={3} style={{ padding: "20px", width: "100%" , backgroundColor: "" }}>
        <Grid container spacing={2}>

          <Grid item xs={6}>
            <img src="/tcc.jpg" alt="Image 1" style={{ width: "100%", height: "350px", objectFit: "cover" }} />
          </Grid>
          <Grid item xs={6}>
            <img src="/tcc.jpg" alt="Image 2" style={{ width: "100%", height: "350px", objectFit: "cover", alignItems: "right"}} />
          </Grid>

 
          <Grid item xs={6}>
            <img src="/tcc.jpg" alt="Image 3" style={{ width: "100%", height: "350px", objectFit: "cover" }} />
          </Grid>
          <Grid item xs={2}>
            <img src="/tcc.jpg" alt="Image 4" style={{ width: "100%", height: "350px", objectFit: "cover" }} />
          </Grid>
          <Grid item xs={4}>
            <img src="/tcc.jpg" alt="Image 5" style={{ width: "100%", height: "350px", objectFit: "cover" }} />
          </Grid>


          <Grid item xs={3}>
            <img src="/tcc.jpg" alt="Image 3" style={{ width: "100%", height: "350px", objectFit: "cover" }} />
          </Grid>
          <Grid item xs={2}>
            <img src="/tcc.jpg" alt="Image 4" style={{ width: "100%", height: "350px", objectFit: "cover" }} />
          </Grid>
          <Grid item xs={7}>
            <img src="/tcc.jpg" alt="Image 5" style={{ width: "1000px", height: "350px", objectFit: "cover" }} />
          </Grid>

        </Grid>
      </Paper>
    </Box>
    </div>
    </div>
  );
};

export default CustomBox;
