import React from "react";
import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import Link from "@mui/material/Link";
import Avatar from "@mui/material/Avatar";
import ArrowForwardIosIcon from "@mui/icons-material/ArrowForwardIos";

const Footer = () => {
  return (
    <Box bgcolor="#000000" color="#FFFFFF" paddingY={5} position="relative" overflow="hidden">

        <Box
          bgcolor="#FFFFFF"
          display="flex"
          justifyContent="space-around"
          paddingY={4}
          paddingX={3}
          marginX="auto"
          position="relative"
          maxWidth="1200px"
          zIndex={2}
          marginTop="-50px"
        >
          <Avatar
            alt="Profile Image"
            src="/tcc.jpg"
            sx={{ width: 75, height: 75, margin: "auto", backgroundColor: "#000000" }}
          />
          <Avatar
            alt="Profile Image"
            src="/tcc.jpg"
            sx={{ width: 75, height: 75, margin: "auto", backgroundColor: "#000000" }}
          />
          <Avatar
            alt="Profile Image"
            src="/tcc.jpg"
            sx={{ width: 75, height: 75, margin: "auto", backgroundColor: "#000000" }}
          />
          <Avatar
            alt="Profile Image"
            src="/tcc.jpg"
            sx={{ width: 75, height: 75, margin: "auto", backgroundColor: "#000000" }}
          />
        </Box>

        <Box
          bgcolor="#424242"
          padding={3}
          marginBottom={3}
          marginX={5}
          maxWidth="auto"
          maxHeight="auto"
          zIndex={1}
          position="relative"
        >
          <div
            style={{
              fontSize: "50px",
              fontWeight: "bold",
              color: "#FFFFFF",
              textAlign: "left",
              letterSpacing: 2,
              marginLeft: "100px",
            }}
          >
            LETS CHANGE YOUR OWN HOME
          </div>
          <div
            style={{
              fontSize: "50px",
              fontWeight: "bold",
              color: "#FFFFFF",
              textAlign: "left",
              letterSpacing: 2,
              marginLeft: "100px",
            }}
          >
            INTERIOR DESIGN NOW
          </div>
          <Button
            style={{
              width: "250px",
              height: "65px",
              fontWeight: "bold",
              backgroundColor: "#000000",
              padding: "20px",
              borderRadius: "0px",
              boxSizing: "border-box",
              position: "relative",
              textAlign: "center",
              color: "#FFFFFF",
              marginLeft: "100px",
            }}
          >
            CONTACT US
          </Button>
        </Box>

      <Container maxWidth="">
        {/* Box 1: Four Avatars */}
        
        {/* Box 2: Text and Button */}
        

        {/* Box 3: Three Columns */}
        <Grid container spacing={3} marginX={5} zIndex={1} position="relative">
          <Grid item xs={6} md={6}>
            {/* Column 1 content */}
            <div
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                color: "#FFFFFF",
                textAlign: "left",
                letterSpacing: 5,
              }}
            >
              INFORMATION
            </div>

            <Typography
              style={{
                fontSize: "10px",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
              }}
            >
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s
            </Typography>
          </Grid>
          <Grid item xs={12} md={2}>
            {/* Column 2 content */}
            <div
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                color: "#FFFFFF",
                textAlign: "left",
                letterSpacing: 5,
              }}
            >
              NAVIGATION
            </div>

            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              <ArrowForwardIosIcon
                style={{
                  width: "13px",
                  height: "13px",
                }}
              />
              Homepage
            </div>
            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              <ArrowForwardIosIcon
                style={{
                  width: "13px",
                  height: "13px",
                }}
              />
              About us
            </div>
            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              <ArrowForwardIosIcon
                style={{
                  width: "13px",
                  height: "13px",
                }}
              />
              Services
            </div>
            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              <ArrowForwardIosIcon
                style={{
                  width: "13px",
                  height: "13px",
                }}
              />
              Project
            </div>
          </Grid>
          <Grid item xs={12} md={2}>
            {/* Column 3 content */}
            <div
              style={{
                fontSize: "20px",
                fontWeight: "bold",
                color: "#FFFFFF",
                textAlign: "left",
                letterSpacing: 5,
              }}
            >
              CONTACT US
            </div>
            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              Lumbung Hidup East Java
            </div>
            <div
              style={{
                fontSize: "10px",
                fontWeight: "semibold",
                color: "#FFFFFF",
                textAlign: "left",
                marginTop: 10,
                letterSpacing: 1,
              }}
            >
              Hello@homco.com
            </div>
          </Grid>
        </Grid>

        {/* Footer */}
        <Box marginTop={3} textAlign="center">
          <Typography variant="body2">© 2023 Your Company</Typography>
        </Box>
      </Container>
    </Box>
  );
};

export default Footer;
