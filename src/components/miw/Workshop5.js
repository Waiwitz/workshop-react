import {  useEffect, useState } from "react";
import { Button, Container, Typography, CircularProgress } from "@mui/material";
const Workshop5 = () => {
  const [loading, setLoading] = useState(false);

  const handleApiCall = async (apiUrl, method, body) => {
    try {
      setLoading(true);
      const response = await fetch(apiUrl, {
        method: method, 
        body: JSON.stringify(body),
        headers: {
          "Content-Type": "application/json",
          // Authorization: `Bearer ${localStorage.getItem ("jwt")}`,
        },
      });


     

      if (method === "GET"){
        console.log(`Request: ${method} `)
      }else{
        console.log(`Request:  ${method}`, body)
      }
      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(`${response.status} - ${errorData.message}`);
      }

      const data = await response.json();

      console.info("Info: API Call Successful");
      console.log("Response:", data);


    } catch (error) {
      console.error("Error: API Call Failed", error);
      if (error instanceof Error) {
        console.error("Error Message:", error.message);
      }
    } finally {
      setLoading(false);
    }
  };
  //////////USERS////////////////



  const UserList = () => {
    // Fetch users data
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users", "GET"); //User list 1
  };
  const UserSearch =  () => {
    // ดึงข้อมูลผู้ใช้ชุดใหม่
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users?search=karn"); //User List (SEARCH) 2
  };
  const UserPagination =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users?page=1&per_page=10", "GET"); //User (Pagination) 3
  };
  const UserSort =  () => {
     handleApiCall(
      process.env.NEXT_PUBLIC_API_URL + "/users?sort_column=id&sort_order=desc",
      "GET"
    ); //User (Sort) 3
  };
  const UserSearchPaginationSort =  () => {
     handleApiCall(
      process.env.NEXT_PUBLIC_API_URL +
        "/users?search=ka&page=1&per_page=10&sort_column=id&sort_order=desc",
      "GET"
    ); //User (Search+Pagination+Sort) 4
  };
  const UserDetail =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users/1", "GET"); //User (Detail) 5
  };

  const UserCreate =  () => {
    const body = {
      fname: "w",
      lname: "w",
      username: "w.chat@melivecode.com",
      password: "w",
      email: "w.chat@melivecode.com",
      avatar: "https://www.melivecode.com/users/cat.png",
    };
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users/create", "POST", body); //User (Create)
  };
  const UserUpdate =  () => {
    const body = {
      id: 12,
      lname: "Gato",
    };
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users/update", "PUT", body); // User (update)
  };

  const UserDelete =  () => {
    const body = {
      id: 11,
    };
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/users/delete", "DELETE", body); // User (DELETE)
  };

  //////////ATTRACTIONS////////////////

  const AttractionsList =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/attractions", "GET"); //ATTRACTIONS (list)
  };
  const AttractionsSearch =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/attractions?search=island", "GET"); //ATTRACTIONS (Search)
  };
  const AttractionPagination =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/attractions?page=1&per_page=10", "GET"); //ATTRACTIONS (Pagination)
  };
  const AttractionSort =  () => {
     handleApiCall(
      process.env.NEXT_PUBLIC_API_URL + "/attractions?sort_column=id&sort_order=desc",
      "GET"
    ); //ATTRACTIONS (Sort)
  };
  const AttractionSPS =  () => {
     handleApiCall(
      process.env.NEXT_PUBLIC_API_URL +
        "/attractions?search=island&page=1&per_page=10&sort_column=id&sort_order=desc",
      "GET"
    ); //ATTRACTIONS (Search+Pagination+Sort)
  };
  const AttractionLanguage =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/th/attractions", "GET"); //ATTRACTIONS (Language)
  };
  const AttractionDetail1 =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/attractions/1", "GET"); //ATTRACTIONS (Detail_1)
  };
  const AttractionDetail2 =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/th/attractions/1", "GET"); //ATTRACTIONS (Detail_2)
  };
  // const AttractionDelete = async () => {
  //   const body = {
  //     id: 13,
  //   };
  //   await handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/attractions/delete", "DELETE", body); // ATTRACTIONS (DELETE)
  // };
  const Attractionstatic_paths =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/attractions/static_paths", "GET"); //ATTRACTIONS (static_paths)
  };
  const PetSalesSummary =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/pets/7days/2023-01-01", "GET"); //PetSalesSummary (7 Day)
  };
  
  const PetSalesDaily =  () => {
     handleApiCall(process.env.NEXT_PUBLIC_API_URL + "/pets/2023-01-01", "GET"); //PetSales (DAILY)
  };
  
  return (
    <Container component="main" maxWidth="xl"  >
      <div>
        <Typography variant="h6" sx={{ textAlign: "center" }}>
          Workshop5:
        </Typography>
        <Typography sx={{ alignContent: 'flex-start'}} display="block" flexDirection="row" justifyContent="space-around">
        <Button variant="contained" color="primary" onClick={UserList} >
          User (List)
        </Button>
        <Button variant="contained" color="primary" onClick={UserSearch}>
          User (SEARCH )
        </Button>
        <Button variant="contained" color="primary" onClick={UserPagination}>
          User(Pagination)
        </Button>
        <Button variant="contained" color="primary" onClick={UserSort}>
          User(Sort)
        </Button>
        <Button variant="contained" color="primary" onClick={UserSearchPaginationSort}>
          User(Search+Pagination+Sort)
        </Button>
        <Button variant="contained" color="primary" onClick={UserDetail}>
          User(Detail)
        </Button>
        <Button variant="contained" color="primary" onClick={UserCreate}>
          User(Create)
        </Button>
        <Button variant="contained" color="primary" onClick={UserUpdate}>
          User (Update)
        </Button>
        <Button variant="contained" color="primary" onClick={UserDelete}>
          User (Delete)
        </Button>
        </Typography>
        <Typography sx={{ alignContent: 'flex-start' }} display="block" flexDirection="row" justifyContent="space-around">
        <Button variant="contained" color="secondary" onClick={AttractionsList}>
          Attractions (List)
        </Button>
        <Button variant="contained" color="secondary" onClick={AttractionsSearch}>
          Attractions (Search)
        </Button>
        <Button variant="contained" color="secondary" onClick={AttractionPagination}>
          Attractions (Pagination)
        </Button>
        <Button variant="contained" color="secondary" onClick={AttractionSort}>
          Attractions (Sort)
        </Button>
        <Button variant="contained" color="secondary" onClick={AttractionSPS}>
          Attractions (Search+Pagination+Sort)
        </Button>
        <Button variant="contained" color="secondary" onClick={AttractionLanguage}>
          Attractions (Language)
        </Button>
        <Button variant="contained" color="secondary" onClick={AttractionDetail1}>
          Attractions (Detail_1)
        </Button>
        <Button variant="contained" color="secondary" onClick={AttractionDetail2}>
          Attractions (Detail_2)
        </Button>
        {/* <Button variant="contained" color="secondary" onClick={AttractionDelete}>
          Attractions (Delete)
        </Button> */}
        <Button variant="contained" color="secondary" onClick={Attractionstatic_paths}>
          Attractions (static_paths)
        </Button>
        </Typography>
      
        <Button variant="contained" color="info" onClick={PetSalesSummary}>
        PetSalesSummary (7 Days)
        </Button>
        
        <Button variant="contained" color="info" onClick={PetSalesDaily}>
        PetSales(Daily)
        </Button>

       {loading  && <CircularProgress />}
        {/* {userData &&
          Array.isArray(userData) &&
          userData.map((user, index) => (
            <div key={index}>
              <Typography>Username: {user.username}</Typography>
              <Typography>Email: {user.email}</Typography>
              <Typography>First Name: {user.fname}</Typography>
              <Typography>Last Name: {user.lname}</Typography>

              {user.avatar && (
                <img
                  src={user.avatar}
                  alt={`User Avatar ${user.id}`}
                  style={{ width: "300px", height: "300px" }}
                />
              )}
            </div>
          ))} */}
      </div>
    </Container>
  );
};

export default Workshop5;
