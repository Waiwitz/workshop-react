import { Button, Grid, ListItem, ListItemButton, Typography } from "@mui/material";
import { Box, Container } from "@mui/system";
import styles from "src/styles/waiwitzStyles.module.css";
import HexagonIcon from '@mui/icons-material/Hexagon';

const Banner = ({ title, detail }) => {
    return (
        <>
            <Box sx={{
                width: '100%', height: 'fit-content',
                backgroundColor: "#989494", padding: '12em 2em 6em'
            }}>
                <Container maxWidth='xl'>
                    <Box>
                        <Typography className={styles.ws2TitleBanner} sx={{ fontSize: { sm: '40px' } }}>
                            {title}
                        </Typography>
                        <Typography variant="p" color={'white'}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.                        </Typography>
                    </Box>
                </Container>
            </Box>
        </>
    )
}

export default Banner;