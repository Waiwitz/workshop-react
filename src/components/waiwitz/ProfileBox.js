import { Box, Card, CardContent, Typography, Grid, Paper, Container } from "@mui/material"
import EmailIcon from '@mui/icons-material/Email';
import FacebookIcon from '@mui/icons-material/Facebook';
import styles from "src/styles/waiwitzStyles.module.css";

const profilePic = 'https://scontent.fbkk29-7.fna.fbcdn.net/v/t1.6435-9/81582715_795945204214765_5895909457866522624_n.jpg?_nc_cat=106&ccb=1-7&_nc_sid=7a1959&_nc_eui2=AeGpHhzHV3XkXghZjy-SnVw3EWgHUnXrinsRaAdSdeuKezYrGR-McohqwfmNVw3Mk5HPPUCGCN-N29Bf5-Y0WBei&_nc_ohc=3lMrMPd33lYAX9K_UUU&_nc_ht=scontent.fbkk29-7.fna&oh=00_AfBrz6Nes-9ZFa8WSJS7H9TyggRlpThGoN1K6OOdTwLajQ&oe=657CD5A7'
const ProfileBox = () => {
    return (
        <>
            <Container maxWidth='xl'>
                <Box>
                    <Grid container spacing={3} columns={{ xs: 4, sm: 8.5, md: 11.5 }} justifyContent="center" className={styles.spaceContainer}>
                        <Grid item xs={3}>
                            <Card style={{ height: '320px' }}>
                                <CardContent>
                                    <Paper className={styles.profilePic}>
                                        <img src={profilePic} alt="profilepic" />
                                    </Paper>
                                </CardContent>
                            </Card>
                        </Grid>

                        <Grid item xs={8}>
                            <CardContent>
                                <div>
                                    <Typography variant="h3" style={{ marginBottom: '1em' }}>Hi! I'm Waiwit Laoniyomthai 👀</Typography>
                                    <Typography margin='normal'>
                                        I am student currently studying 4th year of College of Computing in Khon kean University <br />
                                        I'm on a last year of university now. I am a quick learner and always got a feedback to improve myself. I am not a talkative person but sometime just talk too much for some reason and I love Country Music 🤠
                                    </Typography>
                                    <div className={styles.contact}>
                                        <EmailIcon /> <div>waiwit.laoniyomthai@gmail.com</div>
                                    </div>
                                    <div className={styles.contact}>
                                        <FacebookIcon /> <div>Waiwit Laoniyomthai</div>
                                    </div>
                                </div>
                            </CardContent>
                        </Grid>
                    </Grid>
                </Box>
            </Container>
        </>
    )
}

export default ProfileBox;