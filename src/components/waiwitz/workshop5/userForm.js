import { Box, Button, TextField } from "@mui/material";

const UserForm = ({ form, username, fname, lname, password, email, getApi, setUsername, setFname, setLname, setPassword, setEmail }) => {
    return (
        <Box textAlign={'center'} padding={'0 8em'}>
            {username}
            {/* <TextField label="Username" sx={{ m: 1, width: '25ch' }} onChange={e => setUsername(e.target.value)} />
                                    <TextField label="fname" sx={{ m: 1, width: '25ch' }} onChange={e => setFname(e.target.value)} />
                                    <TextField label="lname" sx={{ m: 1, width: '25ch' }} onChange={e => setLname(e.target.value)} />
                                    <TextField label="password" sx={{ m: 1, width: '25ch' }} onChange={e => setPassword(e.target.value)} />
                                    <TextField label="email" sx={{ m: 1, width: '25ch' }} onChange={e => setEmail(e.target.value)} />
                                    <Button onClick={() => callGetApi(api.url, api.apiName, api.method, userCreate)}>
                                        Save
                                    </Button> */}
            <TextField label="Username" sx={{ m: 1, width: '100%' }} defaultValue={username} onChange={e => setUsername(e.target.value)} />
            <TextField label="fname" sx={{ m: 1, width: '100%' }} defaultValue={fname} onChange={e => setFname(e.target.value)} />
            <TextField label="lname" sx={{ m: 1, width: '100%' }} defaultValue={lname} onChange={e => setLname(e.target.value)} />
            {form == 'createUser' ?
                <>
                    <TextField label="password" sx={{ m: 1, width: '100%' }} defaultValue={password} onChange={e => setPassword(e.target.value)} />
                    <TextField label="email" sx={{ m: 1, width: '100%' }} defaultValue={email} onChange={e => setEmail(e.target.value)} />
                </>
                :
                ''
            }
            <Button variant="outlined" sx={{ width: '100%' }} onClick={getApi}>
                Save
            </Button>
        </Box>
    )
}

export default UserForm;