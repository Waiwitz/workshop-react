import { MenuItem, Typography, Toolbar, AppBar} from '@mui/material';
import Link from 'next/link';
import styles from "src/styles/waiwitzStyles.module.css";

export default function NavBar() {
  return (
      <AppBar position="static" className={styles.navBar}>
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            <b className={styles.logo}>My Resume</b>
          </Typography>
          <Link href="resumeAboutMe" className={styles.navMenuLink}>
            <MenuItem>
              About ME!
            </MenuItem>
          </Link>
          <Link href="resumeProject" className={styles.navMenuLink}>
            <MenuItem>
              Project
            </MenuItem>
          </Link>
        </Toolbar>
      </AppBar>
  );
}