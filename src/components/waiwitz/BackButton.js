import { Button, Link } from "@mui/material";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';

const BackButton = () => {
    return (
        <Link sx={{ position: 'fixed', zIndex: '1200', left: {xs: '20px', md: '50px'}, top: {xs: '50px', md: '150px'} }} href='/workShop/waiwitz'>
            <Button sx={{ bgcolor: '#f5f5f5a2', backdropFilter: 'blur(4px)' }}>
                <ArrowBackIosIcon></ArrowBackIosIcon> หน้า workShop
            </Button>
        </Link>
    )
}

export default BackButton;