import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Avatar, Badge, Card, CardContent, CardMedia, InputBase, Menu, MenuItem, Tooltip, alpha, styled } from '@mui/material';
import Link from 'next/link';
import ShoppingCartTwoToneIcon from '@mui/icons-material/ShoppingCartTwoTone';
import styles from "src/styles/waiwitzStyles.module.css";
import { useEffect, useState } from 'react';
import DeleteIcon from '@mui/icons-material/Delete';

const drawerWidth = 240;

const NavBarEcom = ({ carts }) => {
    const [anchorElUser, setAnchorElUser] = useState(null);
    const [cartOpen, setCartOpen] = useState(false);
    const [cartsItem, setCartsItem] = useState(carts ? carts : JSON.parse(localStorage.getItem('carts')));

    const handleDrawerToggle = () => {
        setCartOpen((prevState) => !prevState);
    };

    const handleOpenUserMenu = (event) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    const handleDeleteCart = (index, title) => {
        const updatedCart = [...cartsItem];
        updatedCart.splice(index, 1);
        setCartsItem(updatedCart);
        localStorage.setItem('carts', JSON.stringify(updatedCart));
        alert(`ลบสินค้า ${title} ออกจากตะกร้าแล้ว`)
        console.log(`ลบสินค้า ${title} ออกจากตะกร้าแล้ว`);
    }

    const totalPrice = () => {
        return cartsItem.reduce((total, item) => total + item.price, 0);
    };
    useEffect(() => {
        setCartsItem(JSON.parse(localStorage.getItem('carts')) || []);
    }, [carts])

    const StyledBadge = styled(Badge)(({
        theme
    }) => ({
        '& .MuiBadge-badge': {
            right: -3,
            top: 13,
            border: `2px solid ${theme.palette.background.paper}`,
            padding: '0 4px',
        },
    }));
    const drawer = (
        <Box height={'100%'}>
            <Typography variant="h6" color={'black'} sx={{ my: 2, ml: 3 }}>
                Cart
            </Typography>
            <Divider />
            {
                cartsItem  ?
                    <>
                        <Box height={'80%'} overflow={'scroll'}>
                            <Box padding={2} position={'relative'}>
                                {cartsItem.map((item, index) => (

                                    <Card key={index} sx={{ my: 2 }}>
                                        <CardContent sx={{ display: 'flex' }}>
                                            <Box sx={{ width: 130, height: 110, overflow: 'hidden', mr: 2, border: 'solid 1px #ddd', borderRadius: 1, }}>
                                                <img component={'img'} src={item.image} width={60} style={{ margin: ' 1em auto' }} />
                                            </Box>
                                            <Box width={'100%'}>
                                                <Typography textAlign={'start'} gutterBottom>{item.title}</Typography>
                                                <Box display={'flex'} justifyContent={'space-between'} mt={4}>
                                                    <Typography alignSelf={'center'} fontWeight={'bold'}>${item.price}</Typography>
                                                    <Button variant='outlined' color='error' onClick={() => handleDeleteCart(index, item.title)}>
                                                        <DeleteIcon />
                                                    </Button>
                                                </Box>
                                            </Box>
                                        </CardContent>
                                    </Card>
                                ))
                                }
                            </Box>
                        </Box>
                        <Box width={'100%'} height={'10%'} position={'absolute'} bottom={0} boxShadow={3} display={'flex'} justifyContent={'space-between'}>
                            <Typography alignSelf={'center'} ml={2}>Total : ${totalPrice()}</Typography>
                            <Button variant='contained' sx={{ borderRadius: 0 }}>Checkout</Button>
                        </Box>
                    </>
                    :
                    <Typography variant='h5' textAlign={'center'} my={10}>ไม่มีสินค้าในตะกร้า</Typography>
            }
        </Box>
    );

    const Logout = () => {
        localStorage.removeItem('Token');
        window.location.href = '/workShop/waiwitz/workshopSix/logIn';
    }

    return (
        <Box sx={{ display: 'flex' }}>
            <CssBaseline />
            <AppBar component="nav" className={styles.navShop}>
                <Toolbar sx={{ justifyContent: 'space-between' }}>
                    {/* <IconButton
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{ mr: 2, display: { sm: 'none' }, color: 'white' }}
                    >
                        <MenuIcon />
                    </IconButton> */}
                    <Link href={'/workShop/waiwitz/workshopSix'} className={styles.clothesLogo}>
                        <Typography
                            variant="h6"
                            component="div" color={'white'}
                        >
                            Store
                        </Typography>
                    </Link>

                    <Box sx={{ flexGrow: 0, display: 'flex' }}>
                        <Box sx={{ mx: 5 }}>
                            <Link href={'/workShop/waiwitz/workshopSix'}>
                                <Button sx={{ color: '#eee' }}>
                                    Home
                                </Button>
                            </Link>
                            {/* <Link href={'/workShop/waiwitz/workshopSix/'}>
                                <Button sx={{ color: '#eee' }}>
                                    Product
                                </Button>
                            </Link> */}
                            <IconButton aria-label="cart" onClick={handleDrawerToggle}>
                                <StyledBadge badgeContent={cartsItem && localStorage.getItem('Token') ? cartsItem.length : 0} color="secondary">
                                    <ShoppingCartTwoToneIcon sx={{ color: 'white' }} />
                                </StyledBadge>
                            </IconButton>
                            {/* <Button sx={{ color: '#000' }}>
                                    <ShoppingCartTwoToneIcon />
                                </Button> */}
                        </Box>
                        {localStorage.getItem('Token')
                            ?
                            <>
                                <Tooltip title="Open settings">
                                    <IconButton onClick={handleOpenUserMenu} sx={{ p: 0 }}>
                                        <Avatar alt="-" src="/static/images/avatar/2.jpg" />
                                    </IconButton>
                                </Tooltip>
                                <Menu
                                    sx={{ mt: '45px' }}
                                    id="menu-appbar"
                                    anchorEl={anchorElUser}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={Boolean(anchorElUser)}
                                    onClose={handleCloseUserMenu}
                                >
                                    <MenuItem onClick={Logout}>
                                        <Typography textAlign="center">ออกจากระบบ</Typography>
                                    </MenuItem>
                                </Menu>
                            </>
                            :
                            <Link href={'/workShop/waiwitz/workshopSix/logIn'}>
                                <Button color='inherit' variant='outlined'>เข้าสู่ระบบ</Button>
                            </Link>
                        }
                    </Box>
                </Toolbar>
            </AppBar>
            <nav>
                <Drawer
                    anchor='right'
                    variant="temporary"
                    open={cartOpen}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        '& .MuiDrawer-paper': { boxSizing: 'border-box', width: { xs: '100%', md: 500 } },
                    }}
                >
                    {drawer}
                </Drawer>
            </nav>
        </Box>
    );
}


export default NavBarEcom;