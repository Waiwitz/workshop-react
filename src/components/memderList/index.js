/* eslint-disable react/jsx-max-props-per-line */
import { Card, Grid, Link, Typography } from "@mui/material";

export const MemberList = ({ memberList }) => {
  return (
    <Grid container spacing={3} p={5}>
      {memberList.map((list) => (
        <Grid item xs={12} md={6} lg={4} key={list.name}>
          <Card >
            <Link href={list.path} underline="none">
              <Typography gutterBottom variant="body1" px={3} py={2} color="initial">
                {list.name}
              </Typography>
            </Link>
          </Card>
        </Grid>
      ))}
    </Grid>
  );
};
