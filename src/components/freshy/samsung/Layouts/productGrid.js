import { Box, Button, CircularProgress, Container, Grid, Typography } from '@mui/material'
import React, { useEffect, useState } from 'react'
import { ProductCard } from '../products/productCard';
import samsungModels from '../JSON/samsung.json';

export const ProductGrid = () => {
    const [visibleItems, setVisibleItems] = useState(16);
    // Initial number of items to display

    const handleShowMore = () => {
        setVisibleItems(prevVisibleItems => prevVisibleItems + 16);
        // Increase the number of visible items
    };
    return (
        <Container maxWidth='lg' sx={{ fontFamily: 'Noto Sans Thai, sans-serif', mb: 10 }}>
            <Typography variant='h3' fontFamily='inherit' textAlign='center' my={6}>
                สินค้าทั้งหมด ({samsungModels.data.devices.length})
            </Typography>
            <Grid container spacing={4}>
                {samsungModels.data ?
                    samsungModels.data.devices.slice(0, visibleItems).map((model) => (
                        <ProductCard key={model.id} model={model} />
                    )) : (
                        <CircularProgress />
                    )
                }
            </Grid>
            {visibleItems < samsungModels.data.devices.length &&
                <Box sx={{ display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center', my: 6 }}>
                    <Button variant='contained' onClick={handleShowMore}
                        sx={{
                            my: 2, color: 'white', bgcolor: 'black',
                            display: 'block', borderRadius: 0, px: 4, fontSize: 18,
                            fontFamily: 'Noto Sans Thai, sans-serif', py: 1, minHeight: 0, border: '2px solid #ffffff00',
                            ":hover": { bgcolor: 'white', color: 'black' },
                            ":focus": { border: '2px dotted #000000', bgcolor: 'white', color: 'black' },
                        }}>
                        แสดงเพิ่มเติม
                    </Button>
                </Box>
            }
        </Container>
    )
}
