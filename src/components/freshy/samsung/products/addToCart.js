import { Button } from "@mui/material";
import React from "react";

const AddToCart = ({ model, color }) => {
    const addToCart = () => {
        // Retrieve existing cart from localStorage or initialize an empty array
        const existingCart = JSON.parse(localStorage.getItem("Cart")) || [];

        // Check if the item is already in the cart
        const existingItemIndex = existingCart.findIndex(
            (item) => item.id === model.id && item.color.hex === color.hex
        );

        if (existingItemIndex !== -1) {
            // If the item is already in the cart, increase the quantity
            existingCart[existingItemIndex].quantity += 1;
        } else {
            // If the item is not in the cart, add it with an initial quantity of 1
            existingCart.push({
                id: model.id,
                name: model.name,
                price: model.price,
                img: model.img,
                description: model.description,
                color: {
                    name: color.name,
                    hex: color.hex,
                },
                quantity: 1,
            });
        }

        // Update localStorage with the modified cart
        localStorage.setItem("Cart", JSON.stringify(existingCart));

        // You can optionally display a message or perform other actions after adding to the cart
        console.log("Item added to cart:", model.name, color.name);
    };
    return (
        <>
            <Button
                onClick={addToCart}
                sx={{
                    mt: 2,
                    color: "white",
                    bgcolor: "black",
                    display: "block",
                    borderRadius: 100,
                    fontFamily: "Noto Sans Thai, sans-serif",
                    py: 0.75,
                    minHeight: 0,
                    fontWeight: "medium",
                    fontSize: 14,
                    px: 1,
                    width: "100%",
                    border: "2px solid #ffffff00",
                    ":hover": { bgcolor: "inherit", color: "black", border: "2px dotted #000000" },
                    ":focus": { border: "2px dotted #ffffff", bgcolor: "black", color: "white" },
                }}
            >
                เพิ่มเข้าตะกร้า
            </Button>
        </>
    );
};

export default AddToCart;
