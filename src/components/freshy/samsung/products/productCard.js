import { CircleRounded } from "@mui/icons-material";
import { Box, Button, Grid, IconButton, Paper, Tooltip, Typography } from "@mui/material";
import Image from "next/image";
import React, { useMemo, useState } from "react";
import AddToCart from "./addToCart";

const isTab = (inputString) => {
  // Convert the input string to lowercase for case-insensitive matching
  const lowercasedInput = inputString.toLowerCase();

  // Check if the lowercased input string contains the word 'tab'
  return lowercasedInput.includes("tab");
};

export const colorList = [
  { name: "Mint", hex: "#c1d1a6" },
  { name: "Lavender", hex: "#dcced9" },
  { name: "White", hex: "#ffffff" },
  { name: "Black", hex: "#000000" },
  { name: "Indigo", hex: "#6b7d98" },
  { name: "Gray", hex: "#808080" },
  { name: "Silver", hex: "#cbc6c4" },
  { name: "Graphite", hex: "#53565a" },
  { name: "Tangerine", hex: "#ce8c4e" },
  { name: "Lime", hex: "#dce9be" },
  { name: "Purple", hex: "#514b79" },
  { name: "Cream", hex: "#f0eae0" },
  { name: "Red", hex: "#cc5953" },
];

const randomColor = () => {
  const randomIndex1 = Math.floor(Math.random() * colorList.length);
  let randomIndex2 = Math.floor(Math.random() * colorList.length);

  // Make sure the second color is different from the first one
  while (randomIndex2 === randomIndex1) {
    randomIndex2 = Math.floor(Math.random() * colorList.length);
  }

  return { color1: colorList[randomIndex1], color2: colorList[randomIndex2] };
};

export const getStorageValue = (description) => {
  const storageMatch = description.match(/\b(\d+)\s*GB\b/i);

  if (storageMatch) {
    const storageGB = parseInt(storageMatch[1], 10);

    if (storageGB >= 1024) {
      const storageTB = storageGB / 1024;
      return `${storageTB.toFixed(0)} TB`;
    } else {
      return `${storageGB} GB`;
    }
  } else {
    return "N/A";
  }
};

export const getRAMValue = (description) => {
  const ramMatch = description.match(/\b(\d+)\s*GB\s*RAM\b/i);

  if (ramMatch) {
    const ramGB = parseInt(ramMatch[1], 10);

    return `${ramGB} GB`;
  } else {
    return "N/A";
  }
};


export function formatCurrency(number) {
  return number.toFixed(0).replace(/\d(?=(\d{3})+$)/g, "$&,");
}

export const ProductCard = ({ model, key }) => {
  const memoizedColors = useMemo(() => randomColor(), []);
  const { color1, color2 } = memoizedColors;
  const [selectedColor, setSelectedColor] = useState(color1);

  return (
    <Grid key={key} item xs={6} sm={4} md={3} sx={{ fontFamily: "Noto Sans Thai, sans-serif" }}>
      <Paper
        sx={{
          bgcolor: "#f7f7f7",
          borderRadius: 3,
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          pt: 10,
          pb: 2.5,
          px: 2,
          gap: 4,
        }}
      >
        <Image
          src={model.img}
          style={{ mixBlendMode: "darken", transform: isTab(model.name) && "rotate(90deg)" }}
          width={120}
          height={120}
          alt={model.name}
        />
        <Typography variant="h6" fontSize={16} fontFamily="inherit" fontWeight="semibold">
          {model.name}
        </Typography>
        <Box sx={{ display: "flex", flexDirection: "column", gap: 1 }}>
          <Typography variant="overlive" fontSize={12} fontFamily="inherit" fontWeight="regular">
            สี :{color1.name}, {color2.name}
          </Typography>
          <Box
            sx={{
              display: "flex",
              gap: 0.5,
              width: "100%",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Tooltip title={color1.name}>
              <IconButton
                onClick={() => setSelectedColor(color1)}
                sx={{
                  color: color1.hex,
                  minWidth: 0,
                  minHeight: 0,
                  p: 0.2,
                  border: selectedColor === color1 ? '1px solid #000000' : '1px solid #00000000',
                  outline: selectedColor === color1 && '1px dotted #ffffff',
                  ":hover": { bgcolor: '#90909070' },
                  ":focus": { outline: '1px dotted #ffffff', border: '1px solid #000000' },
                }}
              >
                <CircleRounded color="inherit" />
              </IconButton>
            </Tooltip>
            <Tooltip title={color2.name}>
              <IconButton
                onClick={() => setSelectedColor(color2)}
                sx={{
                  color: color2.hex,
                  minWidth: 0,
                  minHeight: 0,
                  p: 0.2,
                  border: "1px solid #00000000",
                  border: selectedColor === color2 ? '1px solid #000000' : '1px solid #00000000',
                  outline: selectedColor === color2 && '1px dotted #ffffff',
                  ":hover": { bgcolor: "#90909070" },
                  ":focus": { outline: "1px dotted #ffffff", border: "1px solid #000000" },
                }}
              >
                <CircleRounded color="inherit" />
              </IconButton>
            </Tooltip>
          </Box>
          <Box
            sx={{
              display: "flex",
              flexDirection: "column",
              gap: 2,
              width: "100%",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Tooltip title={getStorageValue(model.description)} fontFamily="inherit">
              <Button
                variant="outlined"
                fontFamily="inherit"
                sx={{
                  my: 1,
                  color: "black",
                  fontSize: 12,
                  fontWeight: "regular",
                  display: "block",
                  borderRadius: 100,
                  minWidth: 0,
                  px: 1,
                  width: "fit-content",
                  fontFamily: "Noto Sans Thai, sans-serif",
                  py: 0.5,
                  minHeight: 0,
                  border: "1px solid black",
                  ":hover": { border: "1px solid black", color: "black" },
                  ":focus": { border: "2px dotted black", color: "black" },
                }}
              >
                {getStorageValue(model.description)}
              </Button>
            </Tooltip>
            <Typography variant="h6">฿{formatCurrency(model.price)}.00</Typography>
          </Box>
        </Box>
        <AddToCart model={model} color={selectedColor} />
      </Paper>
    </Grid>
  );
};
