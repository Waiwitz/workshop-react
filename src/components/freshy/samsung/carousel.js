import React, { useEffect, useState } from 'react';
import { Button, MobileStepper, Paper, Typography, Fade, Box, IconButton } from '@mui/material';
import { ArrowBackIosRounded, ArrowForwardIosRounded } from '@mui/icons-material';
import { fontFamily } from '@mui/system';


const carouselList = [
    {
        overline: 'ลูกค้า samsung.com',
        title: 'รับของขวัญผลิตภัณฑ์โคโลญจน์ Jo Malone London',
        subtitle: 'พิเศษ เมื่อซื้อตู้เย็นและเครื่องซักผ้า รุ่นที่ร่วมรายการ',
        color: 'black',
        imgPath: 'https://images.samsung.com/is/image/samsung/assets/th/offer/home-special-offer/2023/nov/cover-pc-1440x640.jpg?imwidth=1366',
        label: 'Step 1',
    },
    {
        overline: 'New!',
        title: 'Galaxy Z Fold5',
        subtitle: 'ลดทันที 3,000.- รับฟรี Case, BTS NTC, Griptok, Sticker, มูลค่ารวม 1,729.- ฟรี Samsung Care+ 1 ปี',
        color: 'black',
        imgPath: 'https://images.samsung.com/is/image/samsung/assets/th/2307/pfs/03-hd01-Q5-kv-pc-1440x640_default.jpg?imwidth=1366',
        label: 'Step 2',
    },
    {
        overline: 'New!',
        title: 'The Freestyle 2nd Gen',
        color: 'white',
        subtitle: 'รับโค้ดส่วนลด 3,000.- สำหรับซื้อ The Freestyle 2nd Gen เมื่อลงทะเบียน*',
        dueDate: 'ตั้งแต่วันที่ 24 พ.ย. - 3 ธ.ค. 2566',
        imgPath: 'https://images.samsung.com/is/image/samsung/assets/th/offer/home-special-offer/2023/nov/freestyle-cover-pc-1440x640.jpg?imwidth=1366',
        label: 'Step 3',
    },
    {
        overline: 'HOT!',
        title: 'BESPOKE Jet™ AI',
        subtitle: 'ประหยัดเงินสูงถึง 7690.- กับเครื่องดูดฝุ่นที่ทรงพลังที่สุด³² อย่าง BESPOKE Jet™ AI เมื่อคุณนำเครื่องเก่าไปรีไซเคิล ²⁴',
        color: 'white',
        imgPath: 'https://images.samsung.com/is/image/samsung/assets/uk/homepage/KVB_DT_1440x640_BespokeJet1.png?imwidth=1366',
        label: 'Step 4',
    },
];

const Carousel = () => {
    const [activeStep, setActiveStep] = useState(0);

    const handleNext = () => {
        setActiveStep((prevActiveStep) => (prevActiveStep + 1) % carouselList.length);
    };

    const handleBack = () => {
        setActiveStep((prevActiveStep) => (prevActiveStep - 1 + carouselList.length) % carouselList.length);
    };

    const maxSteps = carouselList.length;

    // Set up automatic interval to change active step every 5 seconds
    useEffect(() => {
        const intervalId = setInterval(() => {
            handleNext();
        }, 5000);

        return () => {
            // Cleanup the interval when the component unmounts
            clearInterval(intervalId);
        };
    }, [activeStep]);

    return (
        <Box sx={{ position: 'relative' }}>
            <Fade in={true} timeout={1000}>
                <Box>
                    <img
                        src={carouselList[activeStep].imgPath}
                        alt={carouselList[activeStep].label}
                        style={{ width: '100%', height: '620px', objectFit: 'cover', objectPosition: '68%' }}
                    />
                    <Box sx={{ position: 'absolute', top: { xs: 200, md: 100 }, left: { xs: 30, md: 100 }, maxWidth: { xs: '90%', sm: '80%', md: '45%' }, fontFamily: 'Noto Sans Thai, sans-serif' }}>
                        <Typography variant='subtitle1' fontFamily='inherit' fontWeight='bold' color={carouselList[activeStep].color}>
                            {carouselList[activeStep].overline}
                        </Typography>
                        <Typography variant='h2' fontFamily='inherit' my={2.5} fontWeight='600' color={carouselList[activeStep].color}>
                            {carouselList[activeStep].title}
                        </Typography>
                        <Typography variant='subtitle1' fontFamily='inherit' fontWeight='400' color={carouselList[activeStep].color}>
                            {carouselList[activeStep].subtitle}
                        </Typography>
                        {carouselList[activeStep].dueDate &&
                            <Typography variant='subtitle2' mt={2} fontFamily='inherit' fontWeight='400' color={carouselList[activeStep].color}>
                                {carouselList[activeStep].dueDate}
                            </Typography>
                        }
                        <Button variant='text' sx={{
                            fontFamily: 'inherit',
                            mr: 1.5,
                            display: 'inline-block', borderRadius: 0, textDecoration: 'underline 2px',
                            color: carouselList[activeStep].color, py: 1,
                            p: 0, minWidth: 0, ":hover": { textDecoration: 'underline 2px' },
                            ":focus": { border: '2px dotted #000000' }
                        }}>
                            รายละเอียด
                        </Button>
                        <Button variant='contained' sx={{
                            my: 2, bgcolor: 'black',
                            display: 'inline-block', borderRadius: 100,
                            fontFamily: 'inherit', px: 2, py: 1,
                            minHeight: 0, minWidth: 0, border: '2px solid #ffffff00',
                            ":hover": { bgcolor: 'white', color: 'black' },
                            ":focus": { border: '2px dotted #000000', bgcolor: 'white', color: 'black' }
                        }}>
                            ซื้อเลย
                        </Button>
                    </Box>
                </Box>
            </Fade>
            <MobileStepper
                variant='dots'
                steps={maxSteps}
                position="static"
                activeStep={activeStep}
                sx={{
                    justifyContent: 'center', padding: '10px 0', background: 'none', transform: 'translateY(-3.5em)',
                    '.MuiMobileStepper-dot': {
                        display: 'inline-block',
                        backgroundColor: '#00000040',
                        borderRadius: 0,
                        width: { xs: 60, sm: 100, md: 160 }, height: '1.5px', mx: 1,
                        boxShadow: '0 0 60px 4px #00000050'
                    },
                    '.MuiMobileStepper-dotActive': {
                        display: 'inline-block',
                        backgroundColor: 'white',
                        animation: 'fadeInCarousel',
                        animationDuration: '5s'
                    }
                }}
                nextButton={
                    <IconButton
                        size="small"
                        onClick={handleNext}
                        sx={{
                            position: 'absolute',
                            right: 16, top: '50%', transform: 'translateY(-700%)',
                            p: 1, bgcolor: '#00000070', color: 'white',
                            border: '1px solid #ffffff50',
                            ":hover": {
                                bgcolor: '#00000095'
                            }
                        }}>
                        <ArrowForwardIosRounded />
                    </IconButton>
                }
                backButton={
                    <IconButton
                        size="small"
                        onClick={handleBack}
                        sx={{
                            position: 'absolute',
                            left: 16, top: '50%', transform: 'translateY(-700%)',
                            p: 1, bgcolor: '#00000070', color: 'white',
                            border: '1px solid #ffffff50',
                            ":hover": {
                                bgcolor: '#00000095'
                            }
                        }}>
                        <ArrowBackIosRounded />
                    </IconButton>
                }
            />
        </Box>
    );
};

export default Carousel;