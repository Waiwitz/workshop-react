import { Autocomplete, TextField } from '@mui/material';
import CountryAutoComplete from './countrySelector';
import { Box, Container } from '@mui/system';
import { useEffect, useState } from 'react';
import StateAutoComplete from './stateSelector';
import CityAutoComplete from './citySelector';
import SearchButton from './searchButton';

export default function WeatherForm({handleLocation}) {
    const [country, setCountry] = useState(null);
    const [state, setState] = useState(null);
    const [city, setCity] = useState(null);
    const [isSearch, setIsSearch] = useState(false);
    const handleCountry = (countryName) => {
        setCountry(countryName);
        console.log(countryName);
    }
    const handleState = (stateName) => {
        setState(stateName);
        console.log(stateName);
    }
    const handleCity = (cityName) => {
        setCity(cityName);
        console.log(cityName);
    }

    useEffect(()=> {
        if(country || state || city && isSearch === true) handleLocation(country,state,city,isSearch);
        if(isSearch === true) {
            setTimeout(setIsSearch(false),5000);
        }
    },[isSearch]);
    return (
        <Container maxWidth='sm' className='flex flex-col items-center justify-center mt-10'>
            <CountryAutoComplete handleCountry={handleCountry} selectedCountry={country} />
            <StateAutoComplete country={country} handleState={handleState} selectedState={state} />
            <CityAutoComplete country={country} state={state} handleCity={handleCity} selectedCity={city}/>
            <SearchButton onClick={()=>setIsSearch(true)}/>
        </Container>
    );
}
