import React, { useState, useEffect } from "react";
import {
    Typography,
    TextField,
    Button,
    CircularProgress,
    Select,
    MenuItem,
    InputLabel,
    FormControl,
    Box,
    FormHelperText,
    Input,
} from "@mui/material";
import {
    Circle as CircleIcon,
    BorderColorRounded as BorderColorRoundedIcon,
    PublishRounded as PublishRoundedIcon,
} from "@mui/icons-material";
import defaultAvatar from "public/assets/freshy/avatars/defaultAvatar.svg";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
    PaperProps: {
        style: {
            maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
            width: 250,
        },
    },
};

const UserUpdate = () => {
    const [updating, setUpdating] = useState(false);
    const [response, setResponse] = useState(null);
    const [userData, setUserData] = useState([]);
    const [selectedUser, setSelectedUser] = useState({});
    const [avatar, setAvatar] = useState(null);
    const [avatarPreview, setAvatarPreview] = useState(null);
    const [fileSize, setFileSize] = useState(null);
    const [updated, setUpdated] = useState(false);

    useEffect(() => {
        const fetchUserData = async () => {
            try {
                const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/users`);
                if (!response.ok) {
                    throw new Error(`HTTP error! Status: ${response.status}`);
                }
                const result = await response.json();
                console.info('[INFO] Fetched user data');
                setUserData(result);
            } catch (error) {
                console.error("Error fetching user data:", error);
            }
        };

        fetchUserData();
    }, [userData]);

    const handleAvatarChange = (event) => {
        const file = event.target.files[0];
        if (file) {
            const sizeInKB = file.size / 1024;
            setFileSize(sizeInKB / 1024);
            setAvatarPreview(URL.createObjectURL(file));
            setAvatar(file);
        }
    };

    const handleUpdateUser = async () => {
        setUpdating(true);

        try {
            const userId = selectedUser ? selectedUser.id : null;
            if (!userId) {
                throw new Error("No user selected for update.");
            }

            let avatarBase64 = "";

            // Check if avatar is a File object
            if (avatar instanceof File) {
                // Convert avatar to base64 using FileReader
                avatarBase64 = await new Promise((resolve) => {
                    const reader = new FileReader();
                    reader.onload = (event) => {
                        resolve(event.target.result);
                    };
                    reader.readAsDataURL(avatar);
                    console.info('[INFO] Converted user avatar successfully');
                });
            }

            const requestBody = {
                id: userId,
                username: selectedUser?.username || "",
                email: selectedUser?.username || "",
                fname: selectedUser?.fname || "",
                lname: selectedUser?.lname || "",
                avatar: avatarBase64,
                password: selectedUser?.password || null,
                // Add other fields you want to update here
            };

            const response = await fetch("https://www.melivecode.com/api/users/update", {
                method: "PUT",
                headers: {
                    "Content-Type": "application/json",
                },
                body: JSON.stringify(requestBody),
            });

            const result = await response.json();

            if (!response.ok) {
                throw new Error(`HTTP error! Status: ${response.status}, Message: ${result.message}`);
            }

            setResponse(result);
        } catch (error) {
            setResponse({ status: "error", message: error.message });
        } finally {
            setUpdating(false);
            setUpdated(true);
        }
    };



    return (
        <>
            {!updated ?
                <form
                    style={{
                        display: "flex",
                        flexDirection: "column",
                        minWidth: "20vw",
                        overflowY: "scroll",
                    }}
                >
                    <FormControl variant="outlined" sx={{ my: 2 }}>
                        <InputLabel>User List</InputLabel>
                        <Select
                            name="user"
                            value={selectedUser}
                            MenuProps={MenuProps}
                            renderValue={(selected) => (
                                selected?.fname !== undefined ? (
                                    <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
                                        <img src={selected.avatar} style={{ width: 32, height: 32, borderRadius: '50%' }} alt={selected.fname} />
                                        <Typography variant="button">
                                            {`${selected.fname} ${selected.lname}`}
                                        </Typography>
                                    </Box>
                                ) : (
                                    <Box sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: 2 }}>
                                        <img src={defaultAvatar.src} style={{ width: 32, height: 32, borderRadius: '50%' }} alt='default' />
                                        <Typography variant="button">
                                            Choose a user!
                                        </Typography>
                                    </Box>
                                )
                            )}
                            onChange={(event) => {
                                setSelectedUser(event.target.value);
                                setAvatarPreview(null); // Clear avatar preview when changing user
                            }}
                        >
                            {userData.map((user) => (
                                <MenuItem key={user.id} value={user} sx={{ display: 'flex', flexDirection: 'row', gap: 2 }}>
                                    <img src={user.avatar} style={{ width: 32, height: 32, borderRadius: '50%' }} alt={user.fname} />
                                    <Typography variant="button">
                                        {`${user.fname} ${user.lname}`}
                                    </Typography>
                                </MenuItem>
                            ))}
                        </Select>
                    </FormControl>

                    <FormControl sx={{ my: 2 }}>
                        <Box sx={{ width: '100%', display: 'flex', justifyContent: 'center', position: 'relative' }}>
                            <label htmlFor="avatar">
                                <Input
                                    id="avatar"
                                    name="avatar"
                                    type="file"
                                    accept="image/*"
                                    onChange={handleAvatarChange}
                                    style={{ display: 'none' }}
                                />
                                <img
                                    src={avatarPreview || (selectedUser && selectedUser.avatar) || defaultAvatar.src}
                                    style={{
                                        borderRadius: '50%',
                                        boxShadow: '0 1px 10px 2px rgba(0, 0, 0, 0.1)',
                                        width: 160, height: 160, objectFit: 'cover'
                                    }}
                                    alt="Avatar Preview"
                                />
                                <Box sx={{
                                    bgcolor: '#6366f1',
                                    position: 'absolute',
                                    bottom: 0, right: '28%',
                                    borderRadius: 2, p: 1,
                                    boxShadow: '0 1px 10px 2px rgba(0, 0, 0, 0.1)',
                                    cursor: 'pointer',
                                    transition: 'ease-out .25s',
                                    color: 'white',
                                    ":hover": {
                                        bgcolor: '#4338ca',
                                        transform: 'translateY(-0.25em)'
                                    }
                                }}>
                                    {avatarPreview ? <BorderColorRoundedIcon /> : <PublishRoundedIcon />}
                                </Box>
                            </label>
                        </Box>
                    </FormControl>
                    {fileSize &&
                        <Typography variant='caption' sx={{ textAlign: 'center', mb: 2 }}>
                            Size: {fileSize?.toFixed(2)} MB
                        </Typography>
                    }
                    <FormControl sx={{ my: 1 }}>
                        <TextField
                            name="firstName"
                            label="First Name"
                            variant="outlined"
                            value={selectedUser?.fname || ""}
                            required
                            onChange={(e) => setSelectedUser({ ...selectedUser, fname: e.target.value })}
                        />
                        <FormHelperText error>
                            {selectedUser?.fname === "" && "First Name is required"}
                        </FormHelperText>
                    </FormControl>

                    <FormControl sx={{ my: 1 }}>
                        <TextField
                            name="lastName"
                            label="Last Name"
                            variant="outlined"
                            value={selectedUser?.lname || ""}
                            required
                            onChange={(e) => setSelectedUser({ ...selectedUser, lname: e.target.value })}
                        />
                        <FormHelperText error>
                            {selectedUser?.lname === "" && "Last Name is required"}
                        </FormHelperText>
                    </FormControl>
                    <FormControl sx={{ my: 1 }}>
                        <TextField
                            name="email"
                            label="Email"
                            variant="outlined"
                            value={selectedUser?.username || ""}
                            required
                            onChange={(e) => setSelectedUser({ ...selectedUser, username: e.target.value })}
                        />
                        <FormHelperText error>
                            {selectedUser?.username === "" && "Email is required"}
                        </FormHelperText>
                    </FormControl>

                    <FormControl sx={{ my: 1 }}>
                        <TextField
                            name="password"
                            label="Password"
                            variant="outlined"
                            type="password"
                            value={selectedUser?.password || ""}
                            onChange={(e) => setSelectedUser({ ...selectedUser, password: e.target.value })}
                        />
                        <FormHelperText error>
                            {selectedUser?.password === "" && "Password is required"}
                        </FormHelperText>
                    </FormControl>

                    <Button
                        sx={{ my: 1 }}
                        variant="contained"
                        color="primary"
                        onClick={handleUpdateUser}
                        disabled={updating}
                    >
                        Update User
                    </Button>
                </form>
                :
                <>
                    {updating && <CircularProgress style={{ marginTop: 20 }} />}

                    {response && (
                        <>
                            <Typography
                                variant="button"
                                color={response.status === "ok" ? "primary" : "error"} mt={4}>
                                {response.message}
                            </Typography>
                            {response.status === "ok" && (
                                <>
                                    <Box sx={{ display: 'flex', gap: 4, mt: 4 }}>
                                        <Box sx={{ position: 'relative' }}>
                                            <img src={response.user?.avatar} alt='Avatar' style={{
                                                width: '120px', height: '120px',
                                                borderRadius: '50%', outline: '1px solid #62626240',
                                                outlineOffset: '5px'
                                            }} />
                                            <CircleIcon sx={{ position: 'absolute', right: 0, bottom: 6, color: '#23c369', border: '1px solid #62626240', outline: '3px solid white', borderRadius: '50%', outlineOffset: -4, fontSize: '1.75em' }} />
                                        </Box>
                                        <Box>
                                            <Typography variant="button">{`ID: ${response.user?.id}`}</Typography>
                                            <Typography variant="h6" mb={1}>Name: {response.user?.fname} {response.user?.lname}</Typography>
                                            <Typography variant="body1">Username: {response.user?.username}</Typography>
                                            <Typography variant="body1">Email: {response.user?.email}</Typography>
                                        </Box>
                                    </Box>
                                    <Button
                                        fullWidth
                                        size="large"
                                        sx={{ mt: 3 }}
                                        onClick={() => {
                                            setAvatar(null);
                                            setSelectedUser(null);
                                            setAvatarPreview(null);
                                            setFileSize(null);
                                            setUpdated(false);
                                        }}
                                        variant="contained"
                                        disabled={!updated}
                                    >
                                        {updating ? <CircularProgress size={24} /> : 'Back to Update!'}
                                    </Button>
                                </>
                            )}
                        </>
                    )}
                </>
            }
        </>
    );
};

export default UserUpdate;
