import { Box, Button, Paper, Typography } from "@mui/material";
import QuestionAnswerRoundedIcon from '@mui/icons-material/QuestionAnswerRounded';
import HouseRoundedIcon from '@mui/icons-material/HouseRounded';
import DesignServicesRoundedIcon from '@mui/icons-material/DesignServicesRounded';

export default function Procedure() {
    return (
        <Paper
            elevation={0}
            sx={{
                display: "flex",
                justifyContent: "center",
                fontFamily: "Inter, sans-serif",
                flexDirection: 'column',
                my: { xs: 12, md: 16 },
                gap: 4,
            }}
        >
            <Box flex={1}>
                <Typography variant="overline" color="initial" sx={{ my: 2 }}>
                    HOW WE WORK
                </Typography>
                <Typography
                    variant="h1"
                    color="initial"
                    sx={{
                        mb: 2,
                        fontSize: { xs: '2em', sm: '3em', md: '3.5em' },
                    }}
                >
                    OUR WORK PROCEDURE
                </Typography>
            </Box>
            <Box
                flex={1}
                sx={{
                    display: 'flex',
                    flexDirection: { xs: 'column', md: 'row' },
                    gap: 4
                }}
            >
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        gap: 2,
                        bgcolor: '#4ba4f2',
                        p: 4,
                        width: '100%',
                        maxHeight: '24em',
                        minHeight: '18em',
                        boxShadow: '10px 10px #06223B'
                    }}>
                    <QuestionAnswerRoundedIcon sx={{
                        color: 'white',
                        mb: 1,
                        fontSize: { xs: '2em', sm: '3em', md: '3.5em' },
                    }} />
                    <Typography
                        color='white'
                        variant="h5"
                        fontWeight="semibold">
                        CLIENT DESIGN CONSULTATION
                    </Typography>
                    <Typography
                        variant="body1"
                        color='white'>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                    </Typography>
                </Box>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        gap: 2,
                        bgcolor: '#4BB2F2',
                        p: 4,
                        width: '100%',
                        maxHeight: '24em',
                        minHeight: '18em',
                        boxShadow: '10px 10px #06223B'
                    }}>
                    <DesignServicesRoundedIcon sx={{
                        color: 'white',
                        mb: 1,
                        fontSize: { xs: '2em', sm: '3em', md: '3.5em' },
                    }} />
                    <Typography
                        color='white'
                        variant="h5"
                        fontWeight="semibold">
                        PROTOTYPING HOME DESIGN
                    </Typography>
                    <Typography
                        variant="body1"
                        color='white'>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                    </Typography>
                </Box>
                <Box
                    sx={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        gap: 2,
                        bgcolor: '#3e88ec',
                        p: 4,
                        width: '100%',
                        maxHeight: '24em',
                        minHeight: '18em',
                        boxShadow: '10px 10px #06223B'
                    }}>
                    <HouseRoundedIcon sx={{
                        color: 'white',
                        mb: 1,
                        fontSize: { xs: '2em', sm: '3em', md: '3.5em' },
                    }} />
                    <Typography
                        color='white'
                        variant="h5" f
                        ontWeight="semibold">
                        PROCESSING TO DESIGN HOME
                    </Typography>
                    <Typography
                        variant="body1"
                        color='white'>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                    </Typography>
                </Box>
            </Box>
        </Paper>
    );
}
