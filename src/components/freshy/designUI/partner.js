import React from 'react';
import { Paper, Typography, Button, Box } from '@mui/material';
import Image from 'next/image';
import p870 from 'public/assets/freshy/designUI/870.png';
import p880 from 'public/assets/freshy/designUI/880.png';
import p1609 from 'public/assets/freshy/designUI/1609.png';

const PartnerSection = () => {
    return (
        <Box elevation={0}
            sx={{
                display: 'flex',
                flexDirection: { xs: 'column-reverse', md: 'row' },
                justifyContent: { xs: 'center', md: 'space-between' },
                alignItems: 'center',
                fontFamily: 'Inter, sans-serif',
                my: { xs: 6, md: 8 }
            }}>
            <Box
                flex={1}
                sx={{
                    textAlign: { xs: 'center', md: 'left' }
                }}>
                <Typography
                    variant="overline"
                >
                    PERFECT PARTNER
                </Typography>
                <Typography
                    variant="h1"
                    sx={{
                        fontSize: { xs: '2em', sm: '3em', md: '3.5em' },
                        my: 2
                    }}>
                    WE HAVE PRIORITY FOR CAN CREATE DREAM HOME DESIGN
                </Typography>
                <Typography
                    variant="subtitle1"
                    sx={{ my: 2 }}>
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                </Typography>
                <Button
                    variant="contained"
                    color="inherit"
                    sx={{
                        my: 3,
                        bgcolor: '#3e99ec',
                        color: 'white',
                        py: 2,
                        px: 4,
                        "&:hover": {
                            bgcolor: '#3077b9'
                        }
                    }}>
                    Portfolio
                </Button>
            </Box>
            <Box flex={1} sx={{ position: 'relative' }}>
                <Box sx={{
                    position: 'absolute',
                    transform: {
                        xs: 'skew(-10deg, -2deg) translateY(2%)',
                        md: 'skew(-10deg, -2deg) translateY(20%)'
                    },
                    zIndex: 20,
                    width: 220,
                    transition: 'ease-out 1.5s',
                    ":hover": {
                        transform: {
                            xs: 'skew(18deg, -36deg) translate(-18%, -20%)',
                            md: 'skew(-27deg, 17deg) translate(24%, -16%)'
                        },
                        width: 280
                    }
                }}>
                    <Image src={p1609} alt='Interior' />
                </Box>
                <Box sx={{
                    position: 'absolute',
                    right: '-6%', top: '68%',
                    transform: 'translate(6%)',
                    zIndex: 20,
                    width: 260,
                    transition: 'ease-out 1.5s',
                    ":hover": {
                        transform: 'skew(-17deg, 28deg) translate(12%, -32%)',
                        width: 320
                    }
                }}>
                    <Image src={p1609} alt='Interior' />
                </Box>
                <Box sx={{ position: 'relative', zIndex: 10,transition: 'ease-out 3s', ":hover": {
                        transform: 'skew(-2deg, -4deg)',
                    } }}>
                    <Image src={p870} alt='Interior' />
                </Box>
                {/* <Box sx={{position: 'absolute'}}>
                    <Image src={InteriorPhoto} alt='Interior' />
                </Box> */}
            </Box>
        </Box>
    );
};

export default PartnerSection;
