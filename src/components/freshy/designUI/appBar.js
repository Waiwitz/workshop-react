import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import Menu from '@mui/material/Menu';
import MenuIcon from '@mui/icons-material/Menu';
import Container from '@mui/material/Container';
import Button from '@mui/material/Button';
import MenuItem from '@mui/material/MenuItem';
import WidgetsRoundedIcon from '@mui/icons-material/WidgetsRounded';
import { Close, ExpandMoreRounded, HomeRounded, GroupRounded, WindowRounded, WorkRounded, PersonRounded, DashboardRounded } from '@mui/icons-material';
import { Drawer, Link, List, ListItem, ListItemText, Paper } from '@mui/material';

const baseUrl = '/workShop/teerawut/designUI';
const pages = [
    { name: 'Home', href: `${baseUrl}/`, startIcon: <HomeRounded /> },
    { name: 'About us', href: `${baseUrl}/about`, startIcon: <GroupRounded /> },
    { name: 'Our Services', href: `${baseUrl}services`, startIcon: <WindowRounded /> },
    {
        name: 'Our Projects',
        startIcon: <WorkRounded />,
        children: [
            { name: 'Project 1', href: `${baseUrl}/projects/project-1` },
            { name: 'Project 2', href: `${baseUrl}/projects/project-2` },
            { name: 'Project 3', href: `${baseUrl}/projects/project-3` },
        ]
    },
    { name: 'Portfolio', href: `${baseUrl}/portfolio`, startIcon: <PersonRounded /> },
    {
        name: 'Pages',
        startIcon: <DashboardRounded />,
        children: [
            { name: 'FAQ', href: `${baseUrl}/faq` },
            { name: 'Blog', href: `${baseUrl}/blog` },
            { name: 'Single Post', href: `${baseUrl}/single-post` },
            { name: '404', href: `${baseUrl}/404` },
        ],
    },
];

function ResponsiveAppBar({ currentPage }) {
    const [anchorElSubMenu, setAnchorElSubMenu] = React.useState(null);
    const [anchorElSubMenuMobile, setAnchorElSubMenuMobile] = React.useState(null);
    const [activePage, setActivePage] = React.useState(currentPage);
    const [openDrawer, setOpenDrawer] = React.useState(false);

    const handleOpenSubMenu = (event, page) => {
        setAnchorElSubMenu(event.currentTarget);
        setActivePage(page);
    };

    const handleCloseSubMenu = () => {
        setAnchorElSubMenu(null);
        setActivePage(currentPage);
    };

    const handleOpenSubMenuMobile = (event, page) => {
        if (activePage === page) {
            handleCloseSubMenuMobile();
        } else {
            setAnchorElSubMenuMobile(event.currentTarget);
            setActivePage(page);
        }

    };

    const handleCloseSubMenuMobile = () => {
        setAnchorElSubMenuMobile(null);
        setActivePage(currentPage);
    };

    React.useEffect(() => {
        console.log(activePage);
    }, [activePage]);

    return (
        <AppBar position="sticky" elevation={1} color='inherit' sx={{ py: 2 }}>
            <Container maxWidth="xl">
                <Toolbar disableGutters sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: '100%' }}>
                    <Box sx={{ display: 'flex' }}>
                        <WidgetsRoundedIcon fontSize='large' sx={{ display: 'flex', mr: 1.5, fontSize: '3.5em', color: '#3E99EC' }} />
                        <Typography
                            variant="h5"
                            noWrap
                            component="a"
                            href={`${baseUrl}/`}
                            sx={{
                                mr: 2,
                                display: 'block',
                                fontFamily: 'Inter, sans-serif',
                                fontWeight: 500,
                                color: 'inherit',
                                textDecoration: 'none',
                                color: '#242424'
                            }}
                        >
                            HOMCO.
                            <Typography
                                variant='body1'
                                sx={{
                                    fontFamily: 'Inter, sans-serif',
                                    fontWeight: 200,
                                    color: 'inherit',
                                    textDecoration: 'none',
                                    color: '#242424'
                                }}>
                                INTERIOR CENTRE
                            </Typography>
                        </Typography>
                    </Box>
                    {/* Mobile Navbar toggler */}
                    <Box sx={{ display: { xs: 'flex', md: 'none' } }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={() => setOpenDrawer(true)}
                            color="inherit"
                        >
                            <MenuIcon />
                        </IconButton>
                    </Box>
                    {/* Desktop Navbar */}
                    <Box sx={{ display: { xs: 'none', md: 'flex' }, gap: 1 }}>
                        {pages.map((page, index) => (
                            <React.Fragment key={index}>
                                {page.children ? (
                                    <>
                                        <Button
                                            onClick={(event) => handleOpenSubMenu(event, page.name)}
                                            endIcon={<ExpandMoreRounded />}
                                            sx={{
                                                color: 'inherit',
                                                display: 'flex',
                                                fontFamily: 'Inter, sans-serif',
                                                textTransform: 'uppercase',
                                                color: activePage === page.name ? '#3e99ec' : '#424242',
                                                bgcolor: activePage === page.name ? '#f9f9fe' : 'inherit',
                                                fontWeight: activePage === page.name ? 'bold' : 'medium',
                                                ":hover": {
                                                    color: '#3e99ec',
                                                    fontWeight: 'bold',
                                                }
                                            }}
                                        >
                                            {page.name}
                                        </Button>
                                        <Menu
                                            anchorEl={anchorElSubMenu}
                                            open={Boolean(anchorElSubMenu && activePage === page.name)}
                                            onClose={handleCloseSubMenu}
                                            anchorOrigin={{
                                                vertical: 'bottom',
                                                horizontal: 'left',
                                            }}
                                            transformOrigin={{
                                                vertical: 'top',
                                                horizontal: 'left',
                                            }}
                                            sx={
                                                {
                                                    color: '#424242',
                                                    display: { xs: 'none', md: 'inline-block' },
                                                }
                                            }
                                            getContentAnchorEl={null}
                                        >
                                            {page.children.map((child, childIndex) => (
                                                <MenuItem
                                                    key={childIndex}
                                                    onClick={handleCloseSubMenu}
                                                    sx={{
                                                        color: '#424242',
                                                        ":hover": {
                                                            color: '#3e99ec',
                                                            fontWeight: 'bold',
                                                        }
                                                    }}>
                                                    <Link
                                                        href={child.href}
                                                        variant="button"
                                                        color="inherit"
                                                        fullWidth
                                                        sx={{
                                                            my: 1,
                                                            textDecoration: 'none',
                                                        }}>
                                                        {child.name}
                                                    </Link>
                                                </MenuItem>
                                            ))}
                                        </Menu>
                                    </>
                                ) : (
                                    <Button
                                        key={index}
                                        href={page.href}
                                        sx={{
                                            color: 'inherit',
                                            display: 'block',
                                            fontFamily: 'Inter, sans-serif',
                                            textTransform: 'uppercase',
                                            color: activePage === page.name ? '#3e99ec' : '#424242',
                                            bgcolor: activePage === page.name ? '#f9f9fe' : 'inherit',
                                            fontWeight: activePage === page.name ? 'bold' : 'medium',
                                            ":hover": {
                                                color: '#3e99ec',
                                                fontWeight: 'bold',
                                            }
                                        }}
                                    >
                                        {page.name}
                                    </Button>
                                )}
                            </React.Fragment>
                        ))}
                    </Box>
                </Toolbar>
            </Container>
            {/* Drawer Mobile Navbar */}
            <Drawer anchor="right" open={openDrawer} onClose={() => setOpenDrawer(false)} transitionDuration={500} sx={{ display: { xs: 'block', md: 'none' } }}>
                <List>
                    <Paper square sx={{ display: { xs: 'flex', md: 'none' }, justifyContent: 'end', mb: 2 }}>
                        <IconButton
                            size="large"
                            aria-label="account of current user"
                            aria-controls="menu-appbar"
                            aria-haspopup="true"
                            onClick={() => setOpenDrawer(false)}
                            color="inherit"
                            sx={{ m: { xs: 2, sm: 2.5 } }}
                        >
                            <Close />
                        </IconButton>
                    </Paper>
                    {pages.map((page, index) => (
                        <React.Fragment key={index}>
                            {page.children ? (
                                <>
                                    <ListItem onClick={(event) => handleOpenSubMenuMobile(event, page.name)} sx={{ cursor: 'pointer' }}>
                                        <ListItemText>
                                            <Link
                                                variant="h6"
                                                fontWeight={activePage === page.name ? 'bold' : 'medium'}
                                                sx={{
                                                    display: 'flex',
                                                    alignItems: 'center',
                                                    gap: 1,
                                                    px: 4,
                                                    py: 1,
                                                    width: '75vw',
                                                    textAlign: 'left',
                                                    textDecoration: 'none',
                                                    color: activePage === page.name ? '#3e99ec' : '#424242',
                                                    fontFamily: 'Inter, sans-serif',
                                                    borderRadius: '8px',
                                                    "&:hover": {
                                                        color: '#3e99ec',   
                                                        fontWeight: 'bold',
                                                        bgcolor: '#f9f9fe'
                                                    }
                                                }}
                                            >
                                                {page.startIcon}{page.name}<ExpandMoreRounded />
                                            </Link>
                                        </ListItemText>
                                    </ListItem>
                                    {anchorElSubMenuMobile && activePage === page.name && (
                                        <List>
                                            {page.children.map((child, childIndex) => (
                                                <ListItem key={childIndex} onClick={() => setOpenDrawer(false)}>
                                                    <ListItemText>
                                                        <Link
                                                            href={child.href}
                                                            variant="subtitle1"
                                                            sx={{
                                                                display: 'flex',
                                                                alignItems: 'center',
                                                                px: 8,
                                                                py: .5,
                                                                textAlign: 'left',
                                                                textDecoration: 'none',
                                                                color: '#424242',
                                                                fontFamily: 'Inter, sans-serif',
                                                                "&:hover": {
                                                                    color: '#3e99ec',   
                                                                    fontWeight: 'bold',
                                                                    bgcolor: '#f9f9fe'
                                                                }
                                                            }}
                                                        >
                                                            {child.name}
                                                        </Link>
                                                    </ListItemText>
                                                </ListItem>
                                            ))}
                                        </List>
                                    )}
                                </>
                            ) : (
                                <ListItem onClick={() => setOpenDrawer(false)}>
                                    <ListItemText>
                                        <Link
                                            href={page.href}
                                            variant="h6"
                                            fontWeight={activePage === page.name ? 'bold' : 'medium'}
                                            sx={{
                                                display: 'flex',
                                                alignItems: 'center',
                                                gap: 1,
                                                px: 4,
                                                py: 1,
                                                width: '75vw',
                                                textAlign: 'left',
                                                textDecoration: 'none',
                                                color: activePage === page.name ? '#3e99ec' : '#424242',
                                                fontFamily: 'Inter, sans-serif',
                                                borderRadius: '8px',
                                                "&:hover": {
                                                    color: '#3e99ec',   
                                                    fontWeight: 'bold',
                                                    bgcolor: '#f9f9fe'
                                                }
                                            }}
                                        >
                                            {page.startIcon}{page.name}
                                        </Link>
                                    </ListItemText>
                                </ListItem>
                            )}
                        </React.Fragment>
                    ))}
                </List>
            </Drawer>
        </AppBar>
    );
}
export default ResponsiveAppBar;