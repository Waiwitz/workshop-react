import { Avatar, Box, Button, Paper, Typography } from "@mui/material";
import StarRateRoundedIcon from '@mui/icons-material/StarRateRounded';
import StarHalfRoundedIcon from '@mui/icons-material/StarHalfRounded';
import UserAvatar from 'public/assets/avatars/avatar-carson-darrin.png';

export default function Feedback() {
    return (
        <Paper
            elevation={0}
            sx={{
                display: "flex",
                justifyContent: "center",
                fontFamily: "Inter, sans-serif",
                flexDirection: { xs: 'column', md: 'row' },
                mt: { xs: 12, md: 16 },
                mb: { xs: 42, md: 32 },
                gap: 4,
            }}>
            <Box flex={1}>
                <Typography variant="overline" color="initial" sx={{ my: 2 }}>
                    Clients Feedback
                </Typography>
                <Typography
                    variant="h1"
                    color="initial"
                    sx={{
                        my: 2,
                        fontSize: { xs: '2em', sm: '3em', md: '3.5em' },
                    }}>
                    OUR TESTIMONIAL FROM BEST CLIENTS
                </Typography>
                <Typography
                    variant="subtitle1"
                    sx={{ my: 2 }}>
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                </Typography>
            </Box>
            <Box
                flex={1}
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 2,
                    bgcolor: "white",
                    p: 4,
                    width: '100%',
                    maxHeight: '24em',
                    minHeight: '18em',
                    borderRadius: 1,
                    border: 'solid 1px #61616140'
                }}>
                <Box sx={{ display: 'flex', mb: 1, color: '#EC9C3E' }}>
                    <StarRateRoundedIcon sx={{ fontSize: '2em' }} />
                    <StarRateRoundedIcon sx={{ fontSize: '2em' }} />
                    <StarRateRoundedIcon sx={{ fontSize: '2em' }} />
                    <StarRateRoundedIcon sx={{ fontSize: '2em' }} />
                    <StarHalfRoundedIcon sx={{ fontSize: '2em' }} />
                </Box>
                <Typography variant="body1">
                    Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                </Typography>
                <Box sx={{ display: 'flex', alignItems: 'center', gap: 2 }}>
                    <Avatar alt="John De" src={UserAvatar.src} sx={{ width: 80, height: 80 }} />
                    <Typography variant="h6">
                        JOHN DE
                        <Typography variant="body1">
                            Art Director
                        </Typography>
                    </Typography>
                </Box>
            </Box>
        </Paper>
    );
}
