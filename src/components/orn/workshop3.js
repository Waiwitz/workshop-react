import * as React from 'react';
import { TextField, Button } from '@mui/material';
import swal from 'sweetalert';
import validator from 'validator';
import Navbar from './navbar';
import Grid from '@mui/material/Grid';
import { useState } from 'react';
import { Crud } from './Fetch';



const Workshop3Compo = () => {

  const [username, setUserName] = useState();
  const [password, setPassword] = useState();

  const handleSubmit = async e => {
    e.preventDefault();

    // เรียกใช้ Crud() 
    const response = await Crud(`${process.env.NEXT_PUBLIC_AUTH_URL}`,{
      username,
      password
    },'POST');
   
    try {
    //  ถ้ามี accessToken ใน response ให้ setItem ลงใน localStorage
      if ('accessToken' in response) {
        swal("Success", response.message, "success", {
          buttons: false,
          timer: 2000,
        })
          .then((value) => {
            localStorage.setItem('accessToken', response['accessToken']);
            console.log('info :', response.message, response);
            window.location.href = "/workShop/onrain/workshop4";
          });
      }
      else {
        // validate ช่องกรอก
        if (username.trim() === '' || !validator.isEmail(username)) {
          swal("Failed", "email ไม่ถูกต้อง", "error")
        }
        else if (password.trim() === '') {
          swal("Failed", "password ไม่ถูกต้อง", "error")
        }
        else {
          swal("Failed", response.message + '\n' + 'บัญชีผู้ใช้ไม่ถูกต้อง', "error");
        }
        console.log("error : "+ response.message)
      }
    } catch (error) {
      console.log(error)
    }
   
  }

  return (
    <>
      <Navbar />
      <Grid container spacing={3} mt={5}>
        <Grid item xs>

        </Grid>
        <Grid item xs={6}>
          <form onSubmit={handleSubmit}
            noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              name="email"
              label="Email Address"
              type='email'
              onChange={e => setUserName(e.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="password"
              name="password"
              label="Password"
              type="password"
              onChange={e => setPassword(e.target.value)}
            />
            <Button
              type="submit"
              fullWidth
              sx={{backgroundColor:'#363062 !important',color:'#FFF'}}
            >
              Sign In
            </Button>
          </form>

        </Grid>
        <Grid item xs>

        </Grid>
      </Grid>
    
    </>
  )

}

export default Workshop3Compo;