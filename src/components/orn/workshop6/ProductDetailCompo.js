import NavbarWorkshop6 from "./navbarWorkshop6";
import Workshop6Compo from './workshop6'
import { useRouter } from 'next/router';
import { GetData } from '../Fetch';
import * as React from 'react';
import { styled } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Paper from '@mui/material/Paper';
import Grid from '@mui/material/Grid';
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Typography from '@mui/material/Typography';
import Link from '@mui/material/Link';
import ImageList from '@mui/material/ImageList';
import ImageListItem from '@mui/material/ImageListItem';
import { TextField, FormControl, InputAdornment } from "@mui/material";
import AddCircleSharpIcon from '@mui/icons-material/AddCircleSharp';
import RemoveCircleSharpIcon from '@mui/icons-material/RemoveCircleSharp';
import swal from "sweetalert";



const Item = styled(Paper)(({ theme }) => ({
    backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
    ...theme.typography.body2,
    padding: theme.spacing(3)
}));


function srcset(image, size, rows = 1, cols = 1) {
    return {
        src: `${image}?w=${size * cols}&h=${size * rows}&fit=crop&auto=format`,
        srcSet: `${image}?w=${size * cols}&h=${size * rows
            }&fit=crop&auto=format&dpr=2 2x`,
    };
}


const ProductDetailCompo = () => {
    const router = useRouter();


    const [oneProduct, setOneProduct] = React.useState();
    const [pickedItem, setPickedItem] = React.useState(1);
    const [cart, setCart] = React.useState([]);

    React.useEffect(() => {
        const { id } = router.query;
        if (id) {
            GetData(`${process.env.NEXT_PUBLIC_PRODUCT}/${id}`)
                .then(data => {
                    setOneProduct(data)
                    console.log(data)
                })
                .catch(error => {
                    console.error('Error fetching product data:', error);
                });
        }
    }, [router.query.id]);

    const breadcrumbs = [
        <Link
            underline="hover"
            key="2"
            color="inherit"
            href="/workShop/onrain/workshop6"

        >
            Products
        </Link>,
        <Typography component='div' key={oneProduct != null && oneProduct.id || 3}>
            {oneProduct != null && oneProduct.title}
        </Typography>

    ];
    var itemData = []
    if (oneProduct != null && Array.isArray(oneProduct.images)) {
        for (let i = 0; i < oneProduct.images.length; i++) {
            itemData.push({
                img: oneProduct.images[i],
                title: oneProduct.title,
                rows: 2,
                cols: 2,
            });
        }
        console.log(itemData);
    }

    const addmoreItem = () => {
        setPickedItem(pickedItem + 1)
    }
    const removeItem = () => {
        setPickedItem(Math.max(1, pickedItem - 1));
    }



    const SubmitAddToCart = (e) => {
        e.preventDefault();
        const productData = {
            id: oneProduct.id,
            title: oneProduct.title,
            Amount: pickedItem,
            Thumbnail: oneProduct.thumbnail,
            Price: oneProduct.price
        };
        const cartArray = Array.from(cart); 
        const updatedProducts = [...cartArray, productData];
        const checkProduct = cart.some(item => item.id === productData.id)
        if(checkProduct){
            const updatedCart = cart.map(item => {
                if (item.id === productData.id) {
                    return {
                        ...item,
                        Amount: item.Amount + pickedItem // Update the quantity
                    };
                }
                return item;
            });

            swal("Success", 'เพิ่มข้อมูลลงตะกร้าแล้ว', "success", {
                buttons: false,
                timer: 2000,
            })
                .then((value) => {
                    localStorage.setItem('products', JSON.stringify(updatedCart));
                    setCart(updatedCart);
                });
    
        }else{
            swal("Success", 'เพิ่มข้อมูลลงตะกร้าแล้ว', "success", {
                buttons: false,
                timer: 2000,
            })
                .then((value) => {
                    localStorage.setItem('products', JSON.stringify(updatedProducts));
                    setCart(updatedProducts);
                });
        }

        
    }

    React.useEffect(() => {
        const storedProduct = localStorage.getItem('products');
        if (storedProduct) {
            setCart(JSON.parse(storedProduct));
        }
    }, []);



    return (
        <>
            <NavbarWorkshop6 />

            <Box sx={{ flexGrow: 1, m: 4 }}>
                <Breadcrumbs
                    separator={<NavigateNextIcon fontSize="small" />}
                    aria-label="breadcrumb"
                    m={2}
                >
                    {breadcrumbs}
                </Breadcrumbs>
                <Grid container spacing={2} columns={16}>
                    <Grid item xs={8}>
                        <Item sx={{ height: '70vh' }}>
                            {/* {oneProduct != null && (oneProduct.images.map((item) => (
                                <img key={item} src={item}></img>
                            ))) } */}
                            <ImageList
                                variant="quilted"
                                cols={4}
                                rowHeight={121}
                                sx={{ height: '62vh' }}
                            >
                                {itemData.map((item) => (
                                    <ImageListItem key={item.img} cols={item.cols || 1} rows={item.rows || 1}>
                                        <img
                                            {...srcset(item.img, 121, item.rows, item.cols)}
                                            alt={item.title}
                                            loading="lazy"
                                        />
                                    </ImageListItem>
                                ))}
                            </ImageList>

                        </Item>
                    </Grid>
                    <Grid item xs={8}>
                        <Item sx={{ height: '70vh' }}>
                            <Typography variant="h6" component='div'>product: {oneProduct != null && oneProduct.title}</Typography>
                            <Typography component='div'>brand: {oneProduct != null && oneProduct.brand}</Typography>
                            <Typography component='div'>category: {oneProduct != null && oneProduct.category}</Typography>
                            <Typography component='div'>description: {oneProduct != null && oneProduct.description}</Typography>
                            <Typography component='div'>discountPercentage: {oneProduct != null && oneProduct.discountPercentage}</Typography>
                            <Typography component='div'>price: {oneProduct != null && oneProduct.price}</Typography>
                            <Typography component='div'>rating: {oneProduct != null && oneProduct.rating}</Typography>
                            <Typography component='div'>stock: {oneProduct != null && oneProduct.stock}</Typography>
                            <Box
                                sx={{ m: 1 }}
                            >
                                <form onSubmit={SubmitAddToCart}>
                                    <FormControl variant="standard">
                                        <Box sx={{ display: 'flex', alignItems: 'flex-end' }}>
                                            <RemoveCircleSharpIcon sx={{ fontSize: '30px', cursor: 'pointer' }} onClick={removeItem} />
                                            <TextField id="input-with-sx" variant="standard"
                                                sx={{
                                                    width: '40px',
                                                    '& input': {
                                                        textAlign: 'center',
                                                    },
                                                }}
                                                value={pickedItem} />
                                            <AddCircleSharpIcon sx={{ fontSize: '30px', cursor: 'pointer' }} onClick={addmoreItem} />
                                        </Box>
                                        <Button sx={{ mt: 2 }} variant="outlined" type="submit">add to cart</Button>

                                    </FormControl>
                                </form>
                            </Box>

                        </Item>
                    </Grid>
                </Grid>
            </Box>
        </>
    )
}
export default ProductDetailCompo;