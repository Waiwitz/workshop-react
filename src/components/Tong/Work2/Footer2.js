import * as React from "react";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Grid from "@mui/material/Grid";
import { Button } from "@mui/material";
import TextField from "@mui/material/TextField";
import FacebookIcon from "@mui/icons-material/Facebook";
import TwitterIcon from "@mui/icons-material/Twitter";
import InstagramIcon from "@mui/icons-material/Instagram";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import KeyboardArrowRightIcon from "@mui/icons-material/KeyboardArrowRight";

function Footer2() {
  return (
    <Container
      maxWidth="xl"
      sx={{ backgroundColor: "primary.main", color: "primary.contrastText" }}
    >
      <Box>
        <Toolbar disableGutters sx={{ m: 10 }}>
          <Grid container spacing={2}>
            <Grid item xs={4}>
              <Typography
                variant="h6"
                component="div"
                sx={{ display: { xs: "none", sm: "block" } }}
              >
                INFORMATION
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography
                variant="h6"
                component="div"
                sx={{ display: { xs: "none", sm: "block" } }}
              >
                NAVIGATION
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography
                variant="h6"
                component="div"
                sx={{ display: { xs: "none", sm: "block" } }}
              >
                CONTACT US
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec
                ullamcorper mattis, pulvinar dapibus leo.
              </Typography>
              <br />
              <FacebookIcon />
              &nbsp;
              <TwitterIcon />
              &nbsp;
              <InstagramIcon />
              &nbsp;
              <LinkedInIcon />
            </Grid>
            <Grid item xs={4}>
              <Typography>
                <ul>
                  <li>
                    {" "}
                    <KeyboardArrowRightIcon /> Homepage
                  </li>
                  <br />
                  <li>
                    {" "}
                    <KeyboardArrowRightIcon /> About us
                  </li>
                  <br />
                  <li>
                    {" "}
                    <KeyboardArrowRightIcon />
                    Services
                  </li>
                  <br />
                  <li>
                    {" "}
                    <KeyboardArrowRightIcon /> Project
                  </li>
                </ul>
              </Typography>
            </Grid>
            <Grid item xs={4}>
              <Typography sx={{}}>Lumbung Hidup East Java</Typography>
              <Typography sx={{ mt: 1 }}>Hello@Homco.com</Typography>

              <TextField
                label="Username"
                color="primary"
                variant="outlined"
                fullWidth
                margin="normal"
                sx={{
                  color: "primary.main",
                  bgcolor: "white",
                  borderRadius: 0,
                  "& .MuiInputLabel-root": {
                    color: "primary.main", // กำหนดสีของ label เป็นสีม่วง
                  },
                  "& .MuiInputLabel-root.Mui-focused": {
                    color: "primary", // กำหนดสีของ label เมื่อได้รับ focus เป็นสีม่วง
                  },
                  "& .MuiOutlinedInput-root": {
                    borderRadius: 0, // กำหนด borderRadius ของ TextField เป็นสี่เหลี่ยม
                    "& fieldset": {
                      borderColor: "primary.main",
                    },
                    "&:hover fieldset": {
                      borderColor: "primary.main",
                    },
                    "&.Mui-focused fieldset": {
                      borderColor: "primary.main",
                    },
                  },
                  "&:hover": {
                    bgcolor: "white",
                  },
                }}
              />
              <Button
                variant=""
                sx={{
                  color: "primary.main",
                  bgcolor: "white",
                  borderRadius: 0, // กำหนดให้มุมเป็นสี่เหลี่ยม
                  "&:hover": {
                    bgcolor: "white",
                  },
                }}
              >
                SUBSCRIBE
              </Button>
            </Grid>
          </Grid>
        </Toolbar>
      </Box>
    </Container>
  );
}

export default Footer2;
