  import * as React from "react";
  import CssBaseline from "@mui/material/CssBaseline";
  import Box from "@mui/material/Box";
  import Container from "@mui/material/Container";
  import Typography from "@mui/material/Typography";
  import Grid from "@mui/material/Grid";
  import { Button } from "@mui/material";
  import Card from "@mui/material/Card";
  import CardActions from "@mui/material/CardActions";
  import CardContent from "@mui/material/CardContent";

  export default function Content2() {
    return (
      <React.Fragment>
        <CssBaseline />
        <Container
          maxWidth="fixed"
          sx={{ backgroundColor: "primary.main", color: "primary.contrastText" }}
        >
          <br />
          <Box sx={{ bgcolor: "primary.main", height: "40vh", ml: 5 }}>
            <Grid container spacing={2} sx={{ mt: 10 }}>
              <Grid item xs={8}>
                <Typography
                  variant="h2"
                  component="div"
                  sx={{ display: { xs: "none", sm: "block" } }}
                >
                  SINGLE POST
                </Typography>
                <br />
              </Grid>
              <Grid item xs={4}></Grid>
              <Grid item xs={6}>
                <Typography>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec
                  ullamcorper mattis, pulvinar dapibus leo.
                </Typography>
              </Grid>
              <Grid item xs={4}></Grid>
            </Grid>
          </Box>
        </Container>

        <Container maxWidth="fixed" sx={{ backgroundColor: "", color: "" }}>
          <br />
          <Box sx={{ bgcolor: "", height: "50vh", ml: 5 }}>
            <Grid container spacing={2}>
              <Box></Box>
              <Grid item xs={4}>
                <Card sx={{ backgroundColor: "#757575", borderRadius: 0 }}>
                  <CardContent>
                    <Typography sx={{ fontSize: 14 }} color="white" gutterBottom>
                      {/* Word of the Day */}
                    </Typography>
                    <Typography variant="h5" component="div"></Typography>
                    <Typography sx={{ mb: 1.5 }} color="white">
                      {/* adjective */}
                    </Typography>
                  </CardContent>
                </Card>
                <br />
              </Grid>

              <Grid item xs={6}>
                <Typography variant="h3" sx={{ fontWeight: "bold", mb: 2 }}>
                  TIPS CHOICE BEST AGENCY FOR HOUSE DECORATION
                </Typography>

                <Typography sx={{ fontSize: 16 }} color="" gutterBottom>
                  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                  veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                  voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
                  magni dolores eos qui ratione voluptatem sequi nesciunt.
                </Typography>
              </Grid>
              <Grid item xs={12}>
                <Typography sx={{ fontSize: 16 }} color="" gutterBottom>
                  Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
                  doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore
                  veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam
                  voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur
                  magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est,
                  qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non
                  numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat
                  voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis
                  suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum
                  iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur,
                  vel illum qui dolorem eum fugiat quo voluptas nulla pariatur? But I must explain to
                  you how all this mistaken idea of denouncing pleasure and praising pain was born and
                  I will give you a complete account of the system, and expound the actual teachings
                  of the great explorer of the truth, the master-builder of human happiness. No one
                  rejects, dislikes, or avoids pleasure itself, because it is pleasure, but because
                  those who do not know how to pursue pleasure rationally encounter consequences that
                  are extremely painful. Nor again is there anyone who loves or pursues or desires to
                  obtain pain of itself, because it is pain, but because occasionally circumstances
                  occur in which toil and pain can procure him some great pleasure.
                </Typography>
              </Grid>
            </Grid>
          </Box>
        </Container>
      </React.Fragment>
    );
  }
