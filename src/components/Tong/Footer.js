import * as React from "react";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";

function Footer() {
  return (
    <Container maxWidth="xl">
      <Toolbar disableGutters>
        <Typography
          variant="body1"
          color="text.secondary"
          sx={{
            flexGrow: 1,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
          }}
        >
        Creact By Natthapon Nammasen
        
        </Typography>
        
      </Toolbar>
    </Container>
  );
}

export default Footer;
