import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Container from "@mui/material/Container";
import Dialog from "./Dialog";

function Content() {
  return (
    <>
      <Container fixed>
        <Card sx={{ justifyContent: "center" }}>
          <CardContent>
            {/* <CardActionArea> */}
            <CardContent>
              <br />
              <Typography gutterBottom variant="h5" component="div">
                About Me
              </Typography>
              <Typography variant="body2" color="text.secondary">
                I am a programmer with multiple competition experiences, such as the Digital
                Thailand Big Bang 2023 where I secured the 6th runner-up position. I have also
                worked with startup companies in Khon Kaen. I have a keen interest in UX/UI, Front
                End Development, and Graphic Design. I am continually passionate about personal
                development to keep up with the evolving technology.
              </Typography>
              <br />
              <Typography gutterBottom variant="h5" component="div">
                My Hobby
              </Typography>
              <Typography variant="body2" color="text.secondary">
                I like playing games and guitar.
              </Typography>
              <br />
              <Typography gutterBottom variant="h5" component="div">
                Contact Us
              </Typography>
              <Typography variant="body1" color="text.secondary">
                <li> Phone 0952250533 </li>
              </Typography>
              <Typography variant="body1" color="text.secondary">
                <li> Github TongNPZ</li>
              </Typography>
              <Typography variant="body1" color="text.secondary">
                <li> Email tong.naja101@gmail.com</li>
              </Typography>
              <br />
              <Dialog />
              {/* 
              <Link href="/Calculator">
              Calculator
              </Link> */}
              <br/>
            </CardContent>
            {/* </CardActionArea> */}
          </CardContent>
        </Card>
      </Container>
    </>
  );
}

export default Content;
